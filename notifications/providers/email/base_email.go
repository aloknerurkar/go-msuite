package email

import (
	"bytes"
	"fmt"
	"gitlab.com/go-msuite/common/objects/utils"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	Notifications "gitlab.com/go-msuite/notifications/pb"
	htpl "html/template"
	"log"
	"net/smtp"
	ttpl "text/template"
)

type EmailConf struct {
	*Configurator.BaseEmailerConfig
	Auth     smtp.Auth
	Template *ttpl.Template
}

type BaseEmailer struct {
	conf *EmailConf
}

var emailScript = `From: {{.From}}
To: {{.To}}
Subject: {{.Subject}}
MIME-version: 1.0
Content-Type: text/html; charset="UTF-8"
<html><body>{{.Message}}</body></html>`

func NewBaseEmailer(conf *Configurator.BaseEmailerConfig) (*BaseEmailer, error) {
	eConf := &EmailConf{
		BaseEmailerConfig: conf,
		Auth:              smtp.PlainAuth("", conf.Username, conf.Password, conf.SmtpAddr),
		Template:          ttpl.Must(ttpl.New("emailTpl").Parse(emailScript)),
	}
	bEmailer := new(BaseEmailer)
	bEmailer.conf = eConf
	return bEmailer, nil
}

func (e *BaseEmailer) SupportedModes() []Notifications.NotificationType {
	return []Notifications.NotificationType{
		Notifications.NotificationType_EMAIL,
	}
}

func (e *BaseEmailer) Send(req *Notifications.SendReq) (*Notifications.Notification, error) {
	var msg bytes.Buffer
	e.conf.Template.Execute(&msg, struct {
		From    string
		To      string
		Subject string
		Message htpl.HTML
	}{From: req.GetEmailMsg().From, To: req.GetEmailMsg().To, Subject: req.GetEmailMsg().Title,
		Message: htpl.HTML(req.GetEmailMsg().Msg)})

	err := utils.WithRetry(func() error {
		err := smtp.SendMail(fmt.Sprintf("%s:%s", e.conf.SmtpAddr, e.conf.SmtpPort), e.conf.Auth,
			req.GetEmailMsg().From, []string{req.GetEmailMsg().To}, msg.Bytes())
		if err != nil {
			log.Printf("Intermediate error while sending email: %s", err.Error())
		}
		return err
	}, 3)

	if err != nil {
		log.Printf("Failed sending email Err:%s", err.Error())
		return nil, err
	}

	return &Notifications.Notification{
		UserId: req.GetUserId(),
		Type:   Notifications.NotificationType_EMAIL,
		Msg:    &Notifications.Notification_Email{Email: req.GetEmailMsg()},
	}, nil
}
