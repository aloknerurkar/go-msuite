package providers

import (
	"github.com/pkg/errors"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	Notifications "gitlab.com/go-msuite/notifications/pb"
	"gitlab.com/go-msuite/notifications/providers/email"
	"sync"
)

type Provider interface {
	SupportedModes() []Notifications.NotificationType
	Send(*Notifications.SendReq) (*Notifications.Notification, error)
}

type Providers struct {
	providers map[Notifications.NotificationType]Provider
	mtx       sync.Mutex
}

func (p *Providers) GetProvider(id Notifications.NotificationType) Provider {
	p.mtx.Lock()
	defer p.mtx.Unlock()
	if v, ok := p.providers[id]; ok {
		return v
	}
	panic("Provider not found")
	return nil
}

func NewProviders(conf []*Configurator.NotifProviderConfig) (*Providers, error) {
	pvdrs := new(Providers)
	pvdrs.providers = make(map[Notifications.NotificationType]Provider, len(conf))
	for i := range conf {
		var (
			p   Provider
			err error
		)
		switch conf[i].Type {
		case Notifications.NotificationType_EMAIL:
			switch conf[i].GetEmail().Src {
			case Configurator.EmailProvider_NATIVE:
				p, err = email.NewBaseEmailer(conf[i].GetEmail().GetEmailer())
			default:
				err = errors.New("Unimplemented emailer.")
			}
		default:
			err = errors.New("Unimplemented provider type.")
		}
		if err != nil {
			return nil, err
		}
		pvdrs.mtx.Lock()
		pvdrs.providers[conf[i].Type] = p
		pvdrs.mtx.Unlock()
	}
	return pvdrs, nil
}

func NewTestProviders(p Provider) (*Providers, error) {
	pvdrs := new(Providers)
	pvdrs.providers = make(map[Notifications.NotificationType]Provider, 5)
	pvdrs.providers[Notifications.NotificationType_EMAIL] = p
	pvdrs.providers[Notifications.NotificationType_SMS] = p
	pvdrs.providers[Notifications.NotificationType_ANDROID] = p
	pvdrs.providers[Notifications.NotificationType_IOS] = p
	pvdrs.providers[Notifications.NotificationType_WEB] = p
	return pvdrs, nil
}
