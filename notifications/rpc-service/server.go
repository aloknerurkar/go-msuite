package rpc_service

import (
	"errors"
	lg "github.com/aloknerurkar/log-post/client"
	"gitlab.com/go-msuite/app-errors"
	"gitlab.com/go-msuite/common/go-common"
	"gitlab.com/go-msuite/common/objects"
	Messages "gitlab.com/go-msuite/common/pb"
	"gitlab.com/go-msuite/configurator/config_service/grpc_server"
	"gitlab.com/go-msuite/locker"
	Notifications "gitlab.com/go-msuite/notifications/pb"
	"gitlab.com/go-msuite/notifications/providers"
	"gitlab.com/go-msuite/store"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"sync"
	"sync/atomic"
)

var (
	req_id int64 = 1
)

type subscriber struct {
	*Notifications.SubscribeReq
}

func (l *subscriber) GetNamespace() string { return go_common.NOTIFICATIONS_SVC + "_" + "subscriber" }

func (l *subscriber) GetId() string { return l.UserId }

type notifObj struct {
	*Notifications.Notification
}

func (l *notifObj) GetNamespace() string { return go_common.NOTIFICATIONS_SVC + "_" + "notification" }

func (l *notifObj) SetId(i string) { l.Id = i }

func (l *notifObj) SetCreated(i int64) { l.Created = i }

func (l *notifObj) SetUpdated(i int64) { l.Updated = i }

type notifications struct {
	conf       *objects.GrpcServerConf
	logr       *lg.LogPostClient
	dbP        store.Store
	lckr       locker.Locker
	notif_pvdr *providers.Providers
}

var InitFn grpc_server.RPCInitFn = Init

func Init(srv_conf *objects.GrpcServerConf, srv *grpc.Server) error {

	lgr := lg.InitLogger(srv_conf.SvcName, "", srv_conf.LogLevel, false)

	dbConf := srv_conf.GetDBConfig("redis")
	if dbConf == nil {
		return lgr.Error(errors.New("DB Config absent"), "Database config not provided")
	}

	dbp, err := store.NewStore(dbConf)
	if err != nil {
		return lgr.Error(err, "Failed initializing notifications db")
	}

	lockerConf := srv_conf.GetLockerConfig("zookeeper")
	if lockerConf == nil {
		return lgr.Error(errors.New("Locker Config absent"), "Locker config not provided")
	}

	lockr, err := locker.New(lockerConf)
	if err != nil {
		return lgr.Error(err, "Failed initializing notifications locker")
	}

	pvdrs_config := srv_conf.GetNotifProviders()
	pvdrs, err := providers.NewProviders(pvdrs_config)
	if err != nil {
		return lgr.Error(err, "Failed initializing notifications providers")
	}

	Notifications.RegisterNotificationsServer(srv, &notifications{
		conf:       srv_conf,
		dbP:        dbp,
		lckr:       lockr,
		logr:       lgr,
		notif_pvdr: pvdrs,
	})
	return nil
}

func TestInit(srv_conf *objects.GrpcServerConf, srv *grpc.Server, test_provider providers.Provider) error {

	lgr := lg.InitLogger(srv_conf.SvcName, "", srv_conf.LogLevel, false)

	dbConf := srv_conf.GetDBConfig("redis")
	if dbConf == nil {
		return lgr.Error(errors.New("DB Config absent"), "Database config not provided")
	}

	dbp, err := store.NewStore(dbConf)
	if err != nil {
		return lgr.Error(err, "Failed initializing notifications db")
	}

	lockerConf := srv_conf.GetLockerConfig("zookeeper")
	if lockerConf == nil {
		return lgr.Error(errors.New("Locker Config absent"), "Locker config not provided")
	}

	lockr, err := locker.New(lockerConf)
	if err != nil {
		return lgr.Error(err, "Failed initializing notifications locker")
	}

	pvdrs, _ := providers.NewTestProviders(test_provider)

	Notifications.RegisterNotificationsServer(srv, &notifications{
		conf:       srv_conf,
		dbP:        dbp,
		lckr:       lockr,
		logr:       lgr,
		notif_pvdr: pvdrs,
	})
	return nil
}

func (s *notifications) Subscribe(c context.Context,
	req *Notifications.SubscribeReq) (resp *Messages.ReqUUID, ret_err error) {
	_req_id := atomic.AddInt64(&req_id, 1)
	s.logr.TraceStart("ReqID: %d", _req_id)
	defer s.logr.TraceEnd("ReqID: %d", _req_id)

	sub := &subscriber{SubscribeReq: req}

	unlock, err := s.lckr.TryLock(sub, locker.DefaultTimeout)
	if err != nil {
		ret_err = s.logr.Error(app_errors.ErrInternal("Unable to create new subscriber"),
			"Failed to get lock on User %s SecErr:%s", req.UserId, err.Error())
		return
	}
	defer unlock()

	err = s.dbP.Create(sub)
	if err != nil {
		ret_err = s.logr.Error(app_errors.ErrInternal("Unable to store newly created subscriber"),
			"Failed to get store User %v SecErr:%s", req, err.Error())
		return
	}

	resp = &Messages.ReqUUID{Uuid: req.UserId}
	s.logr.Info("Created new subscriber %v", req)
	return
}

func (s *notifications) Send(c context.Context, req *Notifications.SendReq) (resp *Notifications.Notification, ret_err error) {
	_req_id := atomic.AddInt64(&req_id, 1)
	s.logr.TraceStart("ReqID: %d", _req_id)
	defer s.logr.TraceEnd("ReqID: %d", _req_id)

	pvdr := s.notif_pvdr.GetProvider(req.Type)
	supported := false
	for _, v := range pvdr.SupportedModes() {
		if v == req.Type {
			supported = true
		}
	}

	if !supported {
		ret_err = s.logr.Error(app_errors.ErrUnimplemented("Unsupported request"),
			"Notification provider unsupported Req:%v Supported:%v", req,
			pvdr.SupportedModes())
		return
	}

	req.GetEmailMsg().From = "no-reply@kuber.com"
	if len(req.GetUserId()) > 0 {
		sub := &subscriber{SubscribeReq: &Notifications.SubscribeReq{UserId: req.GetUserId()}}
		err := s.dbP.Read(sub)
		if err != nil {
			ret_err = s.logr.Error(app_errors.ErrInvalidArg("User ID does not exist"),
				"Failed getting subscriber UUID:%s SecErr:%s", req.GetUserId(),
				err.Error())
			return
		}
		for i := range sub.GetSubscriptions() {
			if sub.GetSubscriptions()[i].GetMode() == Notifications.NotificationType_EMAIL {
				req.GetEmailMsg().To = sub.GetSubscriptions()[i].GetEmail()
			}
		}
	}
	if len(req.GetEmail()) > 0 {
		req.GetEmailMsg().To = req.GetEmail()
	}
	if len(req.GetEmailMsg().To) == 0 {
		ret_err = s.logr.Error(app_errors.ErrInvalidArg("Email not provided"),
			"Failed getting subscriber Email:%s", req.GetUserId())
		return
	}

	resp, ret_err = pvdr.Send(req)
	if ret_err != nil {
		ret_err = s.logr.Error(app_errors.ErrInternal("Failed sending notification"),
			"Provider failure SecErr:%s", ret_err.Error())
		return
	}

	obj := &notifObj{Notification: resp}

	ret_err = s.dbP.Create(obj)
	if ret_err != nil {
		ret_err = s.logr.Error(app_errors.ErrInternal("Failed storing notification"),
			"Failed storing notification %v SecErr:%s", resp, ret_err.Error())
	}

	return
}

func (s *notifications) Get(c context.Context,
	req *Messages.ReqUUID) (resp *Notifications.Notification, ret_err error) {
	_req_id := atomic.AddInt64(&req_id, 1)
	s.logr.TraceStart("ReqID: %d", _req_id)
	defer s.logr.TraceEnd("ReqID: %d", _req_id)

	msg := &notifObj{
		&Notifications.Notification{
			Id: req.Uuid,
		},
	}

	unlock, err := s.lckr.TryLock(msg, locker.DefaultTimeout)
	if err != nil {
		ret_err = s.logr.Error(app_errors.ErrInternal("Unable to fetch notification"),
			"Failed getting lock SecErr:%s", err.Error())
		return
	}
	defer unlock()

	err = s.dbP.Read(msg)
	if err != nil {
		ret_err = s.logr.Error(app_errors.ErrInternal("Unable to fetch notification"),
			"Unable to get notification from DB SecErr:%s", err.Error())
	}

	resp = msg.Notification

	return
}

func (s *notifications) List(c context.Context,
	req *Messages.ReqUUIDList) (resp *Notifications.NotificationList, ret_err error) {
	_req_id := atomic.AddInt64(&req_id, 1)
	s.logr.TraceStart("ReqID: %d", _req_id)
	defer s.logr.TraceEnd("ReqID: %d", _req_id)

	wg := sync.WaitGroup{}
	mtx := sync.Mutex{}
	errc := make([]error, 0)

	resp = &Notifications.NotificationList{Items: make([]*Notifications.Notification, len(req.Uuids))}

	for i := range req.Uuids {
		wg.Add(1)
		resp.Items[i] = new(Notifications.Notification)
		it := &notifObj{Notification: resp.Items[i]}
		it.Id = req.Uuids[i].Uuid
		go func(_wg *sync.WaitGroup, _mtx *sync.Mutex, item *notifObj) {
			defer _wg.Done()
			err := s.dbP.Read(item)
			if err != nil {
				_mtx.Lock()
				errc = append(errc, err)
				_mtx.Unlock()
			}
		}(&wg, &mtx, it)
	}
	wg.Wait()

	if len(errc) > 0 {
		ret_err = s.logr.Error(app_errors.ErrInternal("One of the get queries returned error."),
			"Sending error along with response SecErr[0]:%s", errc[0])
	}

	return
}
