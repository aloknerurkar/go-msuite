package locker

import (
	configurator "gitlab.com/go-msuite/configurator/pb"
	"gitlab.com/go-msuite/locker/handlers/memlock"
	"gitlab.com/go-msuite/locker/handlers/redis"
	"gitlab.com/go-msuite/locker/handlers/zookeeper"
	storeItem "gitlab.com/go-msuite/store/item"
	"time"
)

const (
	zookeeperHandler handlerName = "zookeeper"
	redisHandler     handlerName = "redis"
	// lock acquire timeout
	// DefaultTimeout lock acquire timeout
	DefaultTimeout = time.Millisecond * 10000
)

type (
	handlerName string
	// Locker Interface is the base functionality that any locker handler
	// should implement in order to become valid handler
	Locker interface {
		Close() error
		Lock(doc storeItem.Item) (func() error, error)
		TryLock(doc storeItem.Item, t time.Duration) (func() error, error)
	}
)

func New(lockerConfig *configurator.LockerConfig) (handler Locker, err error) {

	switch handlerName(lockerConfig.Handler) {
	case zookeeperHandler:
		handler, err = zookeeper.NewLocker(lockerConfig)
	case redisHandler:
		handler, err = redis.NewLocker(lockerConfig)
	default:
		handler = memlock.NewLocker()
	}
	return handler, err
}
