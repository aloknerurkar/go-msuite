package http_service

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/pkg/errors"
	gw "gitlab.com/go-msuite/auth/pb"
	"gitlab.com/go-msuite/common/go-common"
	"gitlab.com/go-msuite/common/objects"
	"google.golang.org/grpc"
)

// Any additional options etc can be supplied here.
func Init(ctx context.Context, mux *runtime.ServeMux, conf *objects.ProxyConf, opts []grpc.DialOption) error {
	c := conf.GetProxyEp(go_common.AUTH_SVC)
	if c == nil {
		return errors.New("Auth endpoint absent.")
	}
	return gw.RegisterAuthHandlerFromEndpoint(ctx, mux, c.Hostname, opts)
}
