package rpc_service

import (
	"bytes"
	"errors"
	"fmt"
	lg "github.com/aloknerurkar/log-post/client"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/go-msuite/app-errors"
	spec "gitlab.com/go-msuite/auth/pb"
	"gitlab.com/go-msuite/common/go-common"
	"gitlab.com/go-msuite/common/objects"
	"gitlab.com/go-msuite/common/objects/utils"
	msgs "gitlab.com/go-msuite/common/pb"
	"gitlab.com/go-msuite/configurator/config_service/grpc_server"
	"gitlab.com/go-msuite/locker"
	Notifications "gitlab.com/go-msuite/notifications/pb"
	"gitlab.com/go-msuite/store"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"sync/atomic"
	"time"
)

var (
	req_id int64 = 1
)

const (
	VERSION = "v1.0.0"
)

type unverifiedUser struct {
	*spec.UnverifiedUser
}

func (u *unverifiedUser) GetNamespace() string { return go_common.AUTH_SVC + "_" + "UnverifiedUser" }

func (u *unverifiedUser) GetId() string { return u.GetType().String() + "_" + u.GetUsername() }

func (u *unverifiedUser) SetCreated(i int64) { u.Created = i }

func (u *unverifiedUser) SetUpdated(i int64) { u.Updated = i }

type verifiedUser struct {
	*spec.VerifiedUser
}

func (u *verifiedUser) GetNamespace() string { return go_common.AUTH_SVC + "_" + "VerifiedUser" }

func (u *verifiedUser) GetId() string { return u.GetType().String() + "_" + u.GetUsername() }

func (u *verifiedUser) SetId(id string) { u.UserId = id }

func (u *verifiedUser) SetCreated(i int64) { u.Created = i }

func (u *verifiedUser) SetUpdated(i int64) { u.Updated = i }

type authServer struct {
	conf   *objects.GrpcServerConf
	dbP    store.Store
	lckr   locker.Locker
	lgr    *lg.LogPostClient
	cpools *objects.ClientPools
}

var InitFn grpc_server.RPCInitFn = Init

func Init(srv_conf *objects.GrpcServerConf, srv *grpc.Server) error {

	lgr := lg.InitLogger(srv_conf.SvcName, VERSION, srv_conf.LogLevel, false)

	db_conf := srv_conf.GetDBConfig("redis")
	if db_conf == nil {
		return lgr.Error(errors.New("DB Config absent"), "Database config not provided")
	}

	dbp, err := store.NewStore(db_conf)
	if err != nil {
		return lgr.Error(err, "Failed creating DB")
	}

	lockerConf := srv_conf.GetLockerConfig("zookeeper")
	if lockerConf == nil {
		return lgr.Error(errors.New("Locker Config absent"), "Locker config not provided")
	}

	lockr, err := locker.New(lockerConf)
	if err != nil {
		return lgr.Error(err, "Failed initializing wallet locker")
	}

	pools := objects.NewClientPools(srv_conf)

	err = srv_conf.ParseJWTKeys()
	if err != nil {
		return lgr.Error(err, "Failed parsing JWT keys.")
	}

	spec.RegisterAuthServer(srv, &authServer{
		dbP:    dbp,
		lckr:   lockr,
		lgr:    lgr,
		conf:   srv_conf,
		cpools: pools,
	})
	return nil
}

func (s *authServer) verify_fn() func(token *jwt.Token) (interface{}, error) {
	return func(token *jwt.Token) (interface{}, error) {
		return s.conf.PubKey, nil
	}
}

/*
 * First step in User Registration. The client will start user registration process using this API call.
 * We will send the confirmation information on the supplied email/passcode on mobile. We will create an unverified
 * user entry with the credentials and the code that we have sent. Once the user gets the confirmation, he will
 * use verify to complete the verification. During verification he will use this passcode that we generate to complete
 * the registration.
 */
func (s *authServer) Register(c context.Context,
	creds *spec.AuthCredentials) (ret_emp *msgs.EmptyMessage, ret_err error) {

	_req_id := atomic.AddInt64(&req_id, 1)
	s.lgr.TraceStart("ReqID: %d", _req_id)
	defer s.lgr.TraceEnd("ReqID: %d", _req_id)

	usrObj := &unverifiedUser{
		UnverifiedUser: &spec.UnverifiedUser{
			Type:     creds.Type,
			Username: creds.Username,
		},
	}

	unlock, err := s.lckr.TryLock(usrObj, locker.DefaultTimeout)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Internal server error."),
			"ReqID:%d Failed to get lock on user %s. SecErr:%s", _req_id,
			usrObj.GetId(), err.Error())
		return
	}
	defer unlock()

	alreadyStarted := false
	err = s.dbP.Read(usrObj)
	if err == nil && usrObj.Verified {
		ret_err = s.lgr.Error(app_errors.ErrPermissionDenied("Username already exists. Please login."),
			"ReqID:%d Username %s already exists.", _req_id, usrObj.GetId())
		return
	} else if err == nil {
		alreadyStarted = true
	}

	hashedPass, err := bcrypt.GenerateFromPassword([]byte(creds.Password), bcrypt.DefaultCost)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Internal server error."),
			"ReqID:%d Failed to generate password hash. SecErr:%s", _req_id, err.Error())
		return
	}

	// Random string. Will be a 20 char string in case of email and 4 digit OTP in case of mobile.
	var ran_str string

	switch creds.Type {
	case spec.LoginType_EMAIL:
		ran_str = utils.RandStringBytes(20)
	case spec.LoginType_MOBILE:
		fallthrough
	case spec.LoginType_OAUTH_PROVIDER:
		fallthrough
	default:
		ret_err = s.lgr.Error(app_errors.ErrUnimplemented("Unsupported registration type."),
			"ReqID:%d Failed to register user. SecErr:%s", _req_id, err.Error())
		return
	}

	usrObj.Code = ran_str
	usrObj.Password = hashedPass

	if alreadyStarted {
		err = s.dbP.Update(usrObj)
	} else {
		err = s.dbP.Create(usrObj)
	}
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Failed creating user entry."),
			"ReqID:%d Failed to register user SecErr:%s", _req_id, err.Error())
		return
	}

	notifications, done, err := s.cpools.Notifications()
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Something went wrong."),
			"ReqID:%d Failed to connect to Notification Svc SecErr:%s", _req_id, err.Error())
		return
	}
	defer done()

	send_req := &Notifications.SendReq{}
	if creds.Type == spec.LoginType_EMAIL {
		send_req.Recipient = &Notifications.SendReq_Email{Email: creds.Username}
		send_req.Type = Notifications.NotificationType_EMAIL
	} else if creds.Type == spec.LoginType_MOBILE {
		send_req.Recipient = &Notifications.SendReq_Mobile{Mobile: creds.Username}
		send_req.Type = Notifications.NotificationType_SMS
	}
	send_req.Msg = &Notifications.SendReq_EmailMsg{
		EmailMsg: &Notifications.EmailMsg{
			Title: "Welcome to Kuber!",
			Msg:   fmt.Sprintf(user_registration_email, ran_str),
		},
	}

	resp, err := notifications.Send(context.Background(), send_req)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Failed sending registration notification."),
			"ReqID:%d Failed to send registration SecErr:%s", _req_id, err.Error())
		return
	}
	s.lgr.Info("Send notification %v to user", resp)

	ret_emp = &msgs.EmptyMessage{}
	return
}

// Used to complete verification.
func (s *authServer) Verify(c context.Context, verify *spec.VerifyReq) (ret_emp *msgs.EmptyMessage, ret_err error) {

	_req_id := atomic.AddInt64(&req_id, 1)
	s.lgr.TraceStart("ReqID: %d VerifyReq:%s", _req_id, verify.String())
	defer s.lgr.TraceEnd("ReqID: %d", _req_id)

	usrObj := &unverifiedUser{
		UnverifiedUser: &spec.UnverifiedUser{
			Type:     verify.Creds.Type,
			Username: verify.Creds.Username,
		},
	}

	err := s.dbP.Read(usrObj)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Failed verifying user entry."),
			"ReqID:%d Failed to verify user SecErr:%s", _req_id, err.Error())
		return
	}

	if usrObj.Verified {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Already verified user."),
			"ReqID:%d Duplicate verification req: %v", _req_id, verify)
		return
	}

	verifiedUsr := &verifiedUser{
		&spec.VerifiedUser{
			Username: usrObj.Username,
			Type:     usrObj.Type,
			Password: usrObj.Password,
		},
	}

	// Step 1: Update user as verified
	usrObj.Verified = true
	err = s.dbP.Update(usrObj)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Failed creating user entry."),
			"ReqID:%d Failed to create user SecErr:%s", _req_id, err.Error())
		return
	}

	// Step 2: Create verified user entry
	err = s.dbP.Create(verifiedUsr)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Failed creating user entry."),
			"ReqID:%d Failed to create user SecErr:%s", _req_id, err.Error())
		return
	}

	// Step 3: Subscribe user to notifications
	notifications, done, err := s.cpools.Notifications()
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Something went wrong."),
			"ReqID:%d Failed to connect to Notification Svc SecErr:%s", _req_id, err.Error())
		return
	}
	defer done()

	subs := make([]*Notifications.SubscriberInfo, 0)

	switch verifiedUsr.Type {
	case spec.LoginType_EMAIL:
		subs = append(subs, &Notifications.SubscriberInfo{
			Mode: Notifications.NotificationType_SMS,
			Type: &Notifications.SubscriberInfo_MobileNo{
				MobileNo: verifiedUsr.Username,
			},
		})
	case spec.LoginType_MOBILE:
		subs = append(subs, &Notifications.SubscriberInfo{
			Mode: Notifications.NotificationType_EMAIL,
			Type: &Notifications.SubscriberInfo_Email{
				Email: verifiedUsr.Username,
			},
		})
	}

	_, err = notifications.Subscribe(context.Background(), &Notifications.SubscribeReq{
		UserId:        verifiedUsr.UserId,
		Subscriptions: subs,
	})
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Failed subscribing user after verification."),
			"ReqID:%d Failed to subsribe user to Notification Svc SecErr:%s", _req_id, err.Error())
		return
	}

	ret_emp = &msgs.EmptyMessage{}
	return
}

func (s *authServer) create_tokens(user_id, username, loginType string) (accessToken string, refreshToken string, err error) {
	new_token := jwt.New(jwt.GetSigningMethod("RS256"))

	new_token.Claims = &utils.AuthClaims{
		&jwt.StandardClaims{ExpiresAt: time.Now().Add(time.Hour * 24).Unix()},
		"access_token", user_id, username, loginType,
	}

	accessToken, err = new_token.SignedString(s.conf.PrivKey)
	if err != nil {
		return
	}

	new_token.Claims = &utils.AuthClaims{
		&jwt.StandardClaims{ExpiresAt: time.Now().Add(time.Hour * 24 * 30).Unix()},
		"refresh_token", user_id, username, loginType,
	}

	refreshToken, err = new_token.SignedString(s.conf.PrivKey)
	if err != nil {
		accessToken = ""
		return
	}

	s.lgr.Info("Generated New tokens: %s %s", accessToken, refreshToken)
	return
}

func (s *authServer) Authenticate(c context.Context,
	creds *spec.AuthCredentials) (result *spec.AuthResult, ret_err error) {

	_req_id := atomic.AddInt64(&req_id, 1)
	s.lgr.TraceStart("ReqID: %d", _req_id)
	defer s.lgr.TraceEnd("ReqID: %d", _req_id)

	usrObj := &verifiedUser{
		&spec.VerifiedUser{
			Username: creds.Username,
			Type:     creds.Type,
		},
	}

	unlock, err := s.lckr.TryLock(usrObj, locker.DefaultTimeout)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Internal server error."),
			"ReqID:%d Failed to get lock on user %s. SecErr:%s", _req_id,
			usrObj.GetId(), err.Error())
		return
	}
	defer unlock()

	err = s.dbP.Read(usrObj)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrPermissionDenied("Username does not exist. Please signup first."),
			"ReqID:%d Username %s does not exist. SecErr:%s", _req_id, usrObj.GetId(),
			err.Error())
		return
	}

	if usrObj.UseTempPwd {
		if err = bcrypt.CompareHashAndPassword(usrObj.TempPwd, []byte(creds.Password)); err != nil {
			ret_err = s.lgr.Error(app_errors.ErrPermissionDenied("Temporary Password incorrect."),
				"ReqID:%d Temporary password doesnt match. SecErr:%v", _req_id, err)
			return
		}
	} else {
		if err = bcrypt.CompareHashAndPassword(usrObj.Password, []byte(creds.Password)); err != nil {
			ret_err = s.lgr.Error(app_errors.ErrPermissionDenied("Password incorrect."),
				"ReqID:%d Password doesnt match. SecErr:%v", _req_id, err)
			return
		}
	}

	accTok, refTok, err := s.create_tokens(usrObj.UserId, usrObj.Username, usrObj.Type.String())
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Internal server error."),
			"ReqID:%d Failed creating tokens. SecErr:%s", _req_id, err.Error())
		return
	}

	result = &spec.AuthResult{
		UserId:       usrObj.UserId,
		AccessToken:  accTok,
		RefreshToken: refTok,
	}

	return
}

func (s *authServer) RefreshToken(c context.Context,
	curr_token *spec.AuthResult) (result *spec.AuthResult, ret_err error) {

	_req_id := atomic.AddInt64(&req_id, 1)
	s.lgr.TraceStart("ReqID: %d", _req_id)
	defer s.lgr.TraceEnd("ReqID: %d", _req_id)

	if len(curr_token.RefreshToken) == 0 {
		ret_err = s.lgr.Error(app_errors.ErrInvalidArg("Token not present."), "ReqID:%d", _req_id)
		return
	}

	token, err := jwt.ParseWithClaims(curr_token.RefreshToken, &utils.AuthClaims{}, s.verify_fn())
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrUnauthenticated("Invalid token."),
			"ReqID:%d Error while parsing claims SecErr:%s", _req_id, err.Error())
		return
	}

	claims := token.Claims.(*utils.AuthClaims)
	accTok, refTok, err := s.create_tokens(claims.UserUuid, claims.Username, claims.LoginType)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Internal server error."),
			"ReqID:%d Failed creating tokens. SecErr:%s", _req_id, err.Error())
		return
	}

	result = &spec.AuthResult{
		UserId:       claims.UserUuid,
		AccessToken:  accTok,
		RefreshToken: refTok,
	}

	return
}

func (s *authServer) ResetPassword(c context.Context,
	upd_creds *spec.UpdateCredentials) (ret_emp *msgs.EmptyMessage, ret_err error) {

	_req_id := atomic.AddInt64(&req_id, 1)
	s.lgr.TraceStart("ReqID: %d", _req_id)
	defer s.lgr.TraceEnd("ReqID: %d", _req_id)

	if len(upd_creds.AccessToken) == 0 {
		ret_err = s.lgr.Error(app_errors.ErrInvalidArg("Token not present."), "ReqID:%d", _req_id)
		return
	}

	// Parse returns error for expired tokens.
	token, err := jwt.ParseWithClaims(upd_creds.AccessToken, &utils.AuthClaims{}, s.verify_fn())
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrUnauthenticated("Invalid token."),
			"ReqID:%d Error while parsing claims ERR:%s", _req_id, err.Error())
		return
	}

	authClaims, ok := token.Claims.(*utils.AuthClaims)
	if !ok || !token.Valid || (upd_creds.UserId != authClaims.UserUuid) {
		ret_err = s.lgr.Error(app_errors.ErrUnauthenticated("Token invalid for request."),
			"ReqID:%d User ID mismatch in token and req. Token UID:%s ReqUID:%s",
			_req_id, upd_creds.UserId, authClaims.UserUuid)
		return
	}

	hashedPass, err := bcrypt.GenerateFromPassword([]byte(upd_creds.OldPassword), bcrypt.DefaultCost)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Internal server error."),
			"ReqID:%d Failed to generate password hash. SecErr:%s", _req_id, err.Error())
		return
	}

	usr := &verifiedUser{
		&spec.VerifiedUser{
			Username: authClaims.Username,
			Type:     spec.LoginType(spec.LoginType_value[authClaims.LoginType]),
		},
	}

	err = s.dbP.Read(usr)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInvalidArg("Username does not exist."),
			"ReqID:%d User ID %s does not exist.", _req_id, upd_creds.UserId)
		return
	}

	if usr.UseTempPwd {
		if bytes.Compare(usr.TempPwd, hashedPass) != 0 {
			ret_err = s.lgr.Error(app_errors.ErrPermissionDenied("Temporary Password incorrect."),
				"ReqID:%d Temporary password doesnt match.", _req_id)
			return
		}
	} else {
		if bytes.Compare(usr.Password, hashedPass) != 0 {
			ret_err = s.lgr.Error(app_errors.ErrPermissionDenied("Password incorrect."),
				"ReqID:%d Password doesnt match.", _req_id)
			return
		}
	}

	hashedPass, err = bcrypt.GenerateFromPassword([]byte(upd_creds.NewPassword), bcrypt.DefaultCost)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Internal server error."),
			"ReqID:%d Failed to generate password hash. SecErr:%s", _req_id, err.Error())
		return
	}

	usr.Password = hashedPass
	usr.UseTempPwd = false

	err = s.dbP.Update(usr)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Failed updating password entry"),
			"ReqID:%d Failed updating password in DB SecErr:%s", _req_id, err.Error())
	}
	ret_emp = &msgs.EmptyMessage{}
	return
}

func (s *authServer) ForgotPassword(c context.Context,
	creds *spec.AuthCredentials) (ret_emp *msgs.EmptyMessage, ret_err error) {

	_req_id := atomic.AddInt64(&req_id, 1)
	s.lgr.TraceStart("ReqID: %d", _req_id)
	defer s.lgr.TraceEnd("ReqID: %d", _req_id)

	usr := &verifiedUser{
		&spec.VerifiedUser{
			Username: creds.Username,
			Type:     creds.Type,
		},
	}

	unlock, err := s.lckr.TryLock(usr, locker.DefaultTimeout)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Internal server error."),
			"ReqID:%d Failed to get lock on user %s. SecErr:%s", _req_id,
			usr.GetId(), err.Error())
		return
	}
	defer unlock()

	err = s.dbP.Read(usr)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrPermissionDenied("Username does not exist."),
			"ReqID:%d Username %s does not exists. SecErr:%s", _req_id, usr.GetId(), err.Error())
		return
	}

	temp_pwd := utils.RandStringBytes(10)
	hashedPass, err := bcrypt.GenerateFromPassword([]byte(temp_pwd), bcrypt.DefaultCost)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Internal server error."),
			"ReqID:%d Failed to generate password hash. SecErr:%s", _req_id, err.Error())
		return
	}

	usr.TempPwd = hashedPass
	usr.UseTempPwd = true

	err = s.dbP.Update(usr)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Internal server error."),
			"ReqID:%d Failed to update user. SecErr:%s", _req_id, err.Error())
		return
	}

	notifications, done, err := s.cpools.Notifications()
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Something went wrong."),
			"ReqID:%d Failed to connect to Notification Svc SecErr:%s", _req_id, err.Error())
		return
	}
	defer done()

	send_req := &Notifications.SendReq{}
	if creds.Type == spec.LoginType_EMAIL {
		send_req.Recipient = &Notifications.SendReq_Email{Email: creds.Username}
	} else if creds.Type == spec.LoginType_MOBILE {
		send_req.Recipient = &Notifications.SendReq_Mobile{Mobile: creds.Username}
	}
	send_req.Msg = &Notifications.SendReq_EmailMsg{
		EmailMsg: &Notifications.EmailMsg{
			Title: "Kuber password change request",
			Msg:   fmt.Sprintf(forgot_pwd_email, creds.Username, temp_pwd, creds.Username, temp_pwd),
		},
	}

	resp, err := notifications.Send(context.Background(), send_req)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Failed sending forgot password notification."),
			"ReqID:%d Failed to send forgot password notif SecErr:%s", _req_id, err.Error())
		return
	}
	s.lgr.Info("Send notification %v to user", resp)

	ret_emp = &msgs.EmptyMessage{}
	return
}

func (s *authServer) ReportUnauthorizedPwdChange(c context.Context,
	creds *spec.AuthCredentials) (ret_emp *msgs.EmptyMessage,
	ret_err error) {
	_req_id := atomic.AddInt64(&req_id, 1)
	s.lgr.TraceStart("ReqID: %d", _req_id)
	defer s.lgr.TraceEnd("ReqID: %d", _req_id)

	usr := &verifiedUser{
		&spec.VerifiedUser{
			Username: creds.Username,
			Type:     creds.Type,
		},
	}

	unlock, err := s.lckr.TryLock(usr, locker.DefaultTimeout)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Internal server error."),
			"ReqID:%d Failed to get lock on user %s. SecErr:%s", _req_id,
			usr.GetId(), err.Error())
		return
	}
	defer unlock()

	err = s.dbP.Read(usr)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Username does not exist."),
			"ReqID:%d Username %s already exists. SecErr:%s", _req_id, usr.GetId(), err.Error())
		return
	}

	usr.UseTempPwd = false

	err = s.dbP.Update(usr)
	if err != nil {
		ret_err = s.lgr.Error(app_errors.ErrInternal("Failed removing temporary password."),
			"ReqID:%d Failed removing temp password from DB SecErr:%s", _req_id, err.Error())
	}
	ret_emp = &msgs.EmptyMessage{}
	return
}

var user_registration_email = `<style>
	.btn-link{
	  border:none;
	  outline:none;
	  background:none;
	  cursor:pointer;
	  color:#0000EE;
	  padding:0;
	  text-decoration:underline;
	  font-family:inherit;
	  font-size:inherit;
	}
	</style>
	<p>Hi!</p>
	<p>Thanks for registering with Kuber!</p>
	<p>As a final step in the registration, please click on the following link to verify
	your email address and you are all set!</p>
	<p><form action="http://api.pocketbarter.com/v1/verify/%s" method="post">
	  <button type="submit" name="verify_btn" class="btn-link">Verify Email Address</button>
	</form></p>
	<p>Thanks,</p>
	<p>Kuber Team</p>`

var forgot_pwd_email = `<style>
	.btn-link{
	  border:none;
	  outline:none;
	  background:none;
	  cursor:pointer;
	  color:#0000EE;
	  padding:0;
	  text-decoration:underline;
	  font-family:inherit;
	  font-size:inherit;
	}
	</style>
	<p>Hi,</p>
	<p>Seems like you have forgotten your credentials. We have created a temporary
	password for you. Please login using the following credentials:</p>
	<p>Username: %s</p>
	<p>Temporary Password: %s</p>
	<p>If you did not request to reset the password, please click on the following link:</p>
	<p><form action="http://api.pocketbarter.com/v1/report_pwd_change/%s?type=1&password=%s" method="post">
	<button type="submit" name="verify_btn" class="btn-link">Report unauthorized password change</button>
	</form></p>
	<p>The temporary password will be valid for a period of 24 hours. It is recommended that
	you reset the password immediately after logging in. If you fail to change within 24 hours, you
	will need to re-submit the password change request. This is required for your account's safety!</p>
	<p>Thanks,</p>
	<p>Kuber Team</p>`
