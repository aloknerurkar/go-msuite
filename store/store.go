package store

import (
	"errors"
	configurator "gitlab.com/go-msuite/configurator/pb"
	"gitlab.com/go-msuite/store/handlers/os_filestore"
	"gitlab.com/go-msuite/store/handlers/redis"
	"gitlab.com/go-msuite/store/item"
)

const (
	POSTGRES     Handler = "postgres"
	REDIS        Handler = "redis"
	DUMB_DB      Handler = "dumb_db"
	OS_FILESTORE Handler = "os_filestore"
)

type (
	Handler string

	Store interface {
		Create(item.Item) error
		Update(item.Item) error
		Delete(item.Item) error
		Read(item.Item) error
		List(item.Items, item.ListOpt) (int, error)
	}
)

func NewStore(conf *configurator.DBConfig) (Store, error) {
	switch Handler(conf.Handler) {
	case REDIS:
		return redis.NewRedisStore(conf)
	case OS_FILESTORE:
		return os_filestore.NewOSFileStore(conf)
	case POSTGRES:
		fallthrough
	case DUMB_DB:
		fallthrough
	default:
		break
	}

	return nil, errors.New("Invalid handler")
}
