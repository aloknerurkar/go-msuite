package redis

import (
	"fmt"
	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/proto"
	"github.com/satori/go.uuid"
	configurator "gitlab.com/go-msuite/configurator/pb"
	"gitlab.com/go-msuite/store/item"
	"strconv"
	"sync"
	"time"
)

type redisHandler struct {
	conf *configurator.DBConfig
	pool *redis.Pool
}

func NewRedisStore(conf *configurator.DBConfig) (*redisHandler, error) {
	store := new(redisHandler)
	store.conf = conf
	store.pool = &redis.Pool{
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,

		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", store.conf.Hostname+":"+strconv.Itoa(int(store.conf.Port)))
			if err != nil {
				return nil, err
			}
			return c, err
		},

		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
	return store, nil
}

func (r *redisHandler) storeKey(i item.Item) string {
	if len(r.conf.DbName) > 0 {
		return r.conf.DbName + "_" + i.GetNamespace() + "_" + i.GetId()
	}
	return i.GetNamespace() + "_" + i.GetId()
}

func (r *redisHandler) Create(i item.Item) error {
	_conn := r.pool.Get()
	defer _conn.Close()

	if v, ok := i.(item.IdSetter); ok {
		v.SetId(uuid.NewV4().String())
	}

	if v, ok := i.(item.TimeTracker); ok {
		v.SetCreated(time.Now().Unix())
		v.SetUpdated(time.Now().Unix())
	}

	if msg, ok := i.(proto.Message); ok {
		val, err := proto.Marshal(msg)
		if err != nil {
			return fmt.Errorf("error marshalling key %s: %v", r.storeKey(i), err)
		}

		_, err = _conn.Do("SET", r.storeKey(i), val)
		if err != nil {
			v := string(val)
			if len(v) > 15 {
				v = v[0:12] + "..."
			}
			return fmt.Errorf("error setting key %s to %s: %v", r.storeKey(i), v, err)
		}
		return nil
	}

	return fmt.Errorf("unsupported object type: %v", i)
}

func (r *redisHandler) Update(i item.Item) error {
	_conn := r.pool.Get()
	defer _conn.Close()

	if v, ok := i.(item.TimeTracker); ok {
		v.SetUpdated(time.Now().Unix())
	}

	if msg, ok := i.(proto.Message); ok {
		val, err := proto.Marshal(msg)
		if err != nil {
			return fmt.Errorf("error marshalling key %s: %v", r.storeKey(i), err)
		}

		_, err = _conn.Do("SET", r.storeKey(i), val)
		if err != nil {
			v := string(val)
			if len(v) > 15 {
				v = v[0:12] + "..."
			}
			return fmt.Errorf("error setting key %s to %s: %v", r.storeKey(i), v, err)
		}
		return nil
	}

	return fmt.Errorf("unsupported object type: %v", i)
}

func (r *redisHandler) Delete(i item.Item) error {
	_conn := r.pool.Get()
	defer _conn.Close()

	_, err := _conn.Do("DEL", r.storeKey(i))
	if err != nil {
		return fmt.Errorf("error deleting key %s: %v", r.storeKey(i), err)
	}
	return err
}

func (r *redisHandler) Read(i item.Item) error {
	_conn := r.pool.Get()
	defer _conn.Close()

	val, err := redis.Bytes(_conn.Do("GET", r.storeKey(i)))
	if err != nil {
		return fmt.Errorf("error getting key %s: %v", r.storeKey(i), err)
	}

	if msg, ok := i.(proto.Message); ok {
		err = proto.Unmarshal(val, msg)
		if err != nil {
			v := string(val)
			if len(v) > 15 {
				v = v[0:12] + "..."
			}
			return fmt.Errorf("error unmarshalling data %s to %s: %v", v, r.storeKey(i), err)
		}
		return nil
	}

	return fmt.Errorf("unsupported object type: %v", i)
}

func (r *redisHandler) List(l item.Items, o item.ListOpt) (int, error) {
	_conn := r.pool.Get()
	defer _conn.Close()

	if int64(len(l)) < o.Limit {
		return 0, fmt.Errorf("error insufficient items in array to unmarshal required %d got %d",
			o.Limit, len(l))
	}

	comm := make(chan string, o.Limit)
	stop := make(chan bool, 1)
	errc := make([]error, 0)

	wg := sync.WaitGroup{}
	mtx := sync.Mutex{}

	wg.Add(1)
	go func(_wg *sync.WaitGroup, cm chan string, ex_cm chan bool) {
		defer _wg.Done()
		pattern := r.conf.DbName + "_" + l[0].GetNamespace()
		iter := 0
		skip := o.Page * o.Limit
		for {
			arr, err := redis.Values(_conn.Do("SCAN", iter, "MATCH", pattern))
			if err != nil {
				mtx.Lock()
				errc = append(errc, fmt.Errorf("error retrieving '%s' keys", pattern))
				mtx.Unlock()
				break
			}

			iter, _ = redis.Int(arr[0], nil)
			k, _ := redis.Strings(arr[1], nil)
			if int64(len(k)) <= skip {
				skip -= int64(len(k))
				continue
			} else {
				for _, v := range k[skip:] {
					comm <- v
				}
				skip = 0
			}

			if iter == 0 {
				break
			}
		}
		ex_cm <- true
		return
	}(&wg, comm, stop)

	idx := 0
	wg.Add(1)
	go func(_wg *sync.WaitGroup, cm chan string, ex_cm chan bool, its item.Items) {
		for {
			select {
			case _key := <-cm:
				val, err := redis.Bytes(_conn.Do("GET", _key))
				if err == nil {
					if msg, ok := its[idx].(proto.Message); ok {
						err = proto.Unmarshal(val, msg)
					} else {
						err = fmt.Errorf("unsupported object type: %v", its[idx])
					}
				}
				if err != nil {
					mtx.Lock()
					errc = append(errc, fmt.Errorf("error getting key %s: %v", _key, err))
					mtx.Unlock()
					return
				}
				idx++
			case <-ex_cm:
				return
			}
		}
	}(&wg, comm, stop, l)

	wg.Wait()

	if len(errc) > 0 {
		return 0, errc[0]
	}

	return (idx + 1), nil
}
