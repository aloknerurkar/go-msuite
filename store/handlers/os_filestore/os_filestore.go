package os_filestore

import (
	"errors"
	"fmt"
	"github.com/satori/go.uuid"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	"gitlab.com/go-msuite/store/item"
	"io/ioutil"
	"os"
)

type osFileStore struct {
	root string
	conf *Configurator.DBConfig
}

func NewOSFileStore(conf *Configurator.DBConfig) (*osFileStore, error) {
	store := new(osFileStore)
	store.conf = conf
	store.root = conf.DbPath + "/" + conf.DbName
	err := os.Mkdir(store.root, os.ModePerm)
	if err != nil && !os.IsExist(err) {
		return nil, err
	}
	return store, nil
}

func (s *osFileStore) getFullPath(i item.Item) string {
	return s.root + "/" + i.GetNamespace() + "/" + i.GetId()
}

func (s *osFileStore) getParentPath(i item.Item) string {
	return s.root + "/" + i.GetNamespace()
}

func (s *osFileStore) Create(i item.Item) error {

	if v, ok := i.(item.IdSetter); ok {
		v.SetId(uuid.NewV4().String())
	}

	if fi, ok := i.(item.FileItemSetter); ok {
		if _, err := os.Stat(s.getParentPath(i)); os.IsNotExist(err) {
			os.Mkdir(s.getParentPath(i), os.ModePerm)
		}
		fp, err := os.Create(s.getFullPath(i))
		if err != nil {
			return fmt.Errorf("failed creating file %v", err)
		}
		fi.SetFp(fp)
		return nil
	}

	return errors.New("Invalid item type")
}

func (s *osFileStore) Update(i item.Item) error {
	if fi, ok := i.(item.FileItemSetter); ok {
		fp, err := os.OpenFile(s.getFullPath(i), os.O_WRONLY, os.ModePerm)
		if err != nil {
			return fmt.Errorf("failed opening file for edit %v", err)
		}
		fi.SetFp(fp)
		return nil
	}

	return errors.New("Invalid item type")
}

func (s *osFileStore) Delete(i item.Item) error {
	return os.Remove(s.getFullPath(i))
}

func (s *osFileStore) Read(i item.Item) error {
	if fi, ok := i.(item.FileItemSetter); ok {
		fp, err := os.Open(s.getFullPath(i))
		if err != nil {
			return fmt.Errorf("failed opening file for read %v", err)
		}
		fi.SetFp(fp)
		return nil
	}

	return errors.New("Invalid item type")
}

func (s *osFileStore) List(l item.Items, o item.ListOpt) (int, error) {
	files, err := ioutil.ReadDir(s.root)
	if err != nil {
		return 0, fmt.Errorf("error opening root dir %s Err:%v", s.root, err)
	}

	if o.Page*o.Limit > int64(len(files)) {
		return 0, errors.New("No more files")
	}

	skip := o.Page * o.Limit
	for i := skip; i < skip+o.Limit+1; i++ {
		var fp *os.File
		if fi, ok := l[i].(item.FileItemSetter); ok {
			fp, err = os.Open(files[i].Name())
			if err == nil {
				fi.SetFp(fp)
			}
		} else {
			err = errors.New("File not provided.")
		}
		if err != nil {
			err = fmt.Errorf("Failed iterating over files Err:%v", err)
			break
		}
	}
	return len(l), err
}
