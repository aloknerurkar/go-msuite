FROM golang:1.12-stretch
MAINTAINER Alok Nerurkar <aloknerurkar@gmail.com>

# Install protobuf related stuff
ENV PROTOBUF_VERSION=3.5.1 \
    PROTOC_GEN_DOC_VERSION=1.0.0-rc \
    SRC_DIR=/go-msuite \
    PROTO_PATH=/protobuf

RUN mkdir -p $PROTO_PATH && \
    curl -L https://github.com/google/protobuf/archive/v${PROTOBUF_VERSION}.tar.gz \
    | tar xvz --strip-components=1 -C $PROTO_PATH

RUN apt-get update && apt-get install -y autoconf libtool bash vim && \
    cd $PROTO_PATH && \
    autoreconf -f -i -Wall,no-obsolete && \
    ./configure --prefix=/usr --enable-static=no && \
    make -j2 && make install

RUN mkdir -p $PROTO_PATH/google/protobuf && \
        for f in any duration descriptor empty struct timestamp wrappers; do \
            curl -L -o $PROTO_PATH/google/protobuf/${f}.proto \
            https://raw.githubusercontent.com/google/protobuf/master/src/google/protobuf/${f}.proto; \
        done && \
    mkdir -p $PROTO_PATH/google/api && \
        for f in annotations http; do \
            curl -L -o $PROTO_PATH/google/api/${f}.proto \
            https://raw.githubusercontent.com/grpc-ecosystem/grpc-gateway/master/third_party/googleapis/google/api/${f}.proto; \
        done && \
    mkdir -p $PROTO_PATH/github.com/gogo/protobuf/gogoproto && \
        curl -L -o $PROTO_PATH/github.com/gogo/protobuf/gogoproto/gogo.proto \
        https://raw.githubusercontent.com/gogo/protobuf/master/gogoproto/gogo.proto

RUN go get -u -v -ldflags '-w -s' \
	github.com/golang/protobuf/protoc-gen-go \
   github.com/gogo/protobuf/protoc-gen-gogo \
	github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger \
	github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway \
	&& install -c ${GOPATH}/bin/protoc-gen* /usr/bin/


# Download packages first so they can be cached.
COPY go.mod go.sum $SRC_DIR/
RUN cd $SRC_DIR \
  && go mod download

COPY . $SRC_DIR

ENV PROTO_PATH=--proto_path=/protobuf:$SRC_DIR:. \
    GOOGLE_APIS=/protobuf \
    GATEWAY_PLUGIN=--grpc-gateway_out=logtostderr=true,paths=source_relative:.

ONBUILD ARG SERVICE

# Build the thing.
ONBUILD RUN cd $SRC_DIR \
            && make -e all SVC_DIRS=${SERVICE}
