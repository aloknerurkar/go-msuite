package tests

import (
	Auth "gitlab.com/go-msuite/auth/pb"
	"gitlab.com/go-msuite/common/go-common"
	"gitlab.com/go-msuite/common/objects"
	Configurator "gitlab.com/go-msuite/configurator/pb"
)

var authClientConf = &objects.GrpcClientConf{
	GrpcClientConfig: &Configurator.GrpcClientConfig{
		SvcName:    go_common.AUTH_SVC,
		ServerAddr: []string{"localhost:10001"},
		UseJwt:     false,
		UseTls:     false,
	},
}

var creds = []Auth.AuthCredentials{
	{
		Username: "alok@email.com",
		Password: "password",
		Type:     Auth.LoginType_EMAIL,
	},
	{
		Username: "harry@email.com",
		Password: "password",
		Type:     Auth.LoginType_EMAIL,
	},
	{
		Username: "pranav@email.com",
		Password: "password",
		Type:     Auth.LoginType_EMAIL,
	},
}
