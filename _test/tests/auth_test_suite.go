package tests

import (
	"fmt"
	Auth "gitlab.com/go-msuite/auth/pb"
	"gitlab.com/go-msuite/common/objects"
	Messages "gitlab.com/go-msuite/common/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"regexp"
	"sync"
	"testing"
	"time"
)

var EmailChan chan string

var TestPrintDetails = func(test, format string, args ...interface{}) {
	fmt.Printf("Starting test: %s %s", test, fmt.Sprintf(format, args))
}

type AuthTests struct {
	Test *testing.T
}

func (t *AuthTests) RunAuthTests() {
	err := authClientConf.CreatePool(2, func(conn *grpc.ClientConn) error {
		_, err := Messages.NewHeartBeatClient(conn).GetHeartBeat(context.Background(),
			&Messages.EmptyMessage{})
		return err
	})

	if err != nil {
		t.Test.Errorf("Failed to create connection pool to Auth service. Err:%s", err.Error())
		t.Test.FailNow()
		return
	}

	t.TestRegister(authClientConf)
	t.TestLogin(authClientConf)
}

func (t *AuthTests) TestRegister(authCli *objects.GrpcClientConf) {
	wg := sync.WaitGroup{}
	for i := range creds {
		wg.Add(1)
		go func(wg_ *sync.WaitGroup, idx int) {
			defer wg_.Done()
			(&RegisterTest{
				authClient: authCli,
				Creds:      &creds[idx],
				T:          t.Test,
			}).Execute()
		}(&wg, i)
	}
	wg.Wait()
}

func (t *AuthTests) TestLogin(authCli *objects.GrpcClientConf) {
	wg := sync.WaitGroup{}
	for i := range creds {
		wg.Add(1)
		go func(wg_ *sync.WaitGroup, idx int) {
			defer wg_.Done()
			(&LoginTest{
				authClient: authCli,
				Creds:      &creds[idx],
				T:          t.Test,
			}).Execute()
		}(&wg, i)
	}
	wg.Wait()
}

// 1. Auth.Register
// 2. Auth.Verify
// 3. Auth.Authenticate
type RegisterTest struct {
	authClient *objects.GrpcClientConf
	Creds      *Auth.AuthCredentials
	T          *testing.T
}

func (t *RegisterTest) Execute() {

	TestPrintDetails("RegisterTest", "Conf: %+v Creds: %+v\n", t.authClient, t.Creds)

	conn, done, err := t.authClient.GetPooledConn()
	if err != nil {
		t.T.Fatalf("Failed to get new connection to Auth service. Err:%s", err.Error())
	}
	defer done()

	_, err = Auth.NewAuthClient(conn).Register(context.Background(), t.Creds)
	if err != nil {
		t.T.Fatalf("Failed to register user. Err:%s", err.Error())
	}

	msg := <-EmailChan

	r, _ := regexp.Compile("http://api.pocketbarter.com/v1/verify/*")
	lt := r.FindStringIndex(msg)
	code := msg[lt[1] : lt[1]+20]

	_, err = Auth.NewAuthClient(conn).Verify(context.Background(), &Auth.VerifyReq{Code: code, Creds: t.Creds})
	if err != nil {
		t.T.Fatalf("Failed to verify user. Err:%s", err.Error())
	}

	time.Sleep(time.Second)

	_, err = Auth.NewAuthClient(conn).Authenticate(context.Background(), t.Creds)
	if err != nil {
		t.T.Fatalf("Failed to Authenticate user after registration. Err:%s", err.Error())
	}
}

// 1. Auth.Authenticate -> Failure case invalid password
// 2. Auth.Authenticate
type LoginTest struct {
	authClient *objects.GrpcClientConf
	Creds      *Auth.AuthCredentials
	T          *testing.T
}

func (t *LoginTest) Execute() {

	TestPrintDetails("LoginTest", "Conf: %+v Creds: %+v\n", t.authClient, t.Creds)

	conn, done, err := t.authClient.GetPooledConn()
	if err != nil {
		t.T.Fatalf("Failed to get new connection to Auth service. Err:%s", err.Error())
	}
	defer done()

	invalid_creds := &Auth.AuthCredentials{
		Type:     t.Creds.Type,
		Username: t.Creds.Username,
		Password: t.Creds.Password + "bullshit",
	}

	_, err = Auth.NewAuthClient(conn).Authenticate(context.Background(), invalid_creds)
	if err == nil {
		t.T.Fatalf("Invalid credentials did not return error.")
	}

	_, err = Auth.NewAuthClient(conn).Authenticate(context.Background(), t.Creds)
	if err != nil {
		t.T.Fatalf("Failed to authenticate user %v Err:%s.", t.Creds, err.Error())
	}
}
