package test

import (
	"errors"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"gitlab.com/go-msuite/_test/tests"
	auth_svc "gitlab.com/go-msuite/auth/rpc-service"
	cdn_svc "gitlab.com/go-msuite/cdn/rpc-service"
	"gitlab.com/go-msuite/common/go-common"
	"gitlab.com/go-msuite/common/objects"
	"gitlab.com/go-msuite/configurator/client"
	"gitlab.com/go-msuite/configurator/config_service"
	"gitlab.com/go-msuite/configurator/config_service/grpc_server"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	conf_svc "gitlab.com/go-msuite/configurator/rpc-service"
	Notifications "gitlab.com/go-msuite/notifications/pb"
	notif_svc "gitlab.com/go-msuite/notifications/rpc-service"
	payments_svc "gitlab.com/go-msuite/payments/rpc-service"
	wallet_svc "gitlab.com/go-msuite/wallet/rpc-service"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"os"
	"strconv"
	"sync"
	"time"
)

var notifID uint32

type TestNotifProvider struct{}

func (t *TestNotifProvider) SupportedModes() []Notifications.NotificationType {
	return []Notifications.NotificationType{
		Notifications.NotificationType_EMAIL,
	}
}

func (t *TestNotifProvider) Send(r *Notifications.SendReq) (*Notifications.Notification, error) {
	switch r.Type {
	case Notifications.NotificationType_EMAIL:
		tests.EmailChan <- r.GetEmailMsg().Msg
	default:
		panic("Unsupported type")
	}
	notifID++
	return &Notifications.Notification{
		Id:      fmt.Sprintf("test_id_%d", notifID),
		Created: time.Now().Unix(),
		Msg: &Notifications.Notification_Email{
			Email: r.GetEmailMsg(),
		},
	}, nil
}

var NotifInitFn grpc_server.RPCInitFn = NotificationTestInit

func NotificationTestInit(srv_conf *objects.GrpcServerConf, srv *grpc.Server) error {
	test_pvdr := &TestNotifProvider{}
	return notif_svc.TestInit(srv_conf, srv, test_pvdr)
}

func StartServices(ctx context.Context) <-chan error {

	services := []config_service.Service{
		config_service.NewService(&Configurator.Request{
			SvcName: go_common.AUTH_TEST_SVC,
			Version: "1.0.0",
			Configs: []*Configurator.ConfigReq{
				{
					ConfType: Configurator.ConfigType_GRPC_SERVER_CONFIG,
					Handler:  go_common.AUTH_TEST_SVC,
					SubConfs: []*Configurator.ConfigReq{
						{
							ConfType: Configurator.ConfigType_DB_CONFIG,
							Handler:  "redis",
						},
						{
							ConfType: Configurator.ConfigType_LOCKER_CONFIG,
							Handler:  "zookeeper",
						},
						{
							ConfType: Configurator.ConfigType_GRPC_CLIENT_CONFIG,
							Handler:  go_common.NOTIFICATIONS_SVC,
						},
					},
				},
			},
		}, auth_svc.InitFn),
		config_service.NewService(&Configurator.Request{
			SvcName: go_common.CDN_SVC,
			Version: "1.0.0",
			Configs: []*Configurator.ConfigReq{
				{
					ConfType: Configurator.ConfigType_GRPC_SERVER_CONFIG,
					Handler:  go_common.CDN_SVC,
					SubConfs: []*Configurator.ConfigReq{
						{
							ConfType: Configurator.ConfigType_DB_CONFIG,
							Handler:  "os_filestore_test",
						},
						{
							ConfType: Configurator.ConfigType_DB_CONFIG,
							Handler:  "redis",
						},
						{
							ConfType: Configurator.ConfigType_LOCKER_CONFIG,
							Handler:  "zookeeper",
						},
					},
				},
			},
		}, cdn_svc.InitFn),
		config_service.NewService(&Configurator.Request{
			SvcName: go_common.PAYMENTS_SVC,
			Version: "1.0.0",
			Configs: []*Configurator.ConfigReq{
				{
					ConfType: Configurator.ConfigType_GRPC_SERVER_CONFIG,
					Handler:  go_common.PAYMENTS_SVC,
					SubConfs: []*Configurator.ConfigReq{
						{
							ConfType: Configurator.ConfigType_DB_CONFIG,
							Handler:  "redis",
						},
						{
							ConfType: Configurator.ConfigType_GRPC_CLIENT_CONFIG,
							Handler:  go_common.WALLET_SVC,
						},
						{
							ConfType: Configurator.ConfigType_LOCKER_CONFIG,
							Handler:  "zookeeper",
						},
					},
				},
			},
		}, payments_svc.InitFn),
		config_service.NewService(&Configurator.Request{
			SvcName: go_common.WALLET_SVC,
			Version: "1.0.0",
			Configs: []*Configurator.ConfigReq{
				{
					ConfType: Configurator.ConfigType_GRPC_SERVER_CONFIG,
					Handler:  go_common.WALLET_SVC,
					SubConfs: []*Configurator.ConfigReq{
						{
							ConfType: Configurator.ConfigType_DB_CONFIG,
							Handler:  "redis",
						},
						{
							ConfType: Configurator.ConfigType_LOCKER_CONFIG,
							Handler:  "zookeeper",
						},
					},
				},
			},
		}, wallet_svc.InitFn),
		config_service.NewService(&Configurator.Request{
			SvcName: go_common.NOTIFICATIONS_SVC,
			Version: "1.0.0",
			Configs: []*Configurator.ConfigReq{
				{
					ConfType: Configurator.ConfigType_GRPC_SERVER_CONFIG,
					Handler:  go_common.NOTIFICATIONS_SVC,
					SubConfs: []*Configurator.ConfigReq{
						{
							ConfType: Configurator.ConfigType_DB_CONFIG,
							Handler:  "redis",
						},
						{
							ConfType: Configurator.ConfigType_LOCKER_CONFIG,
							Handler:  "zookeeper",
						},
					},
				},
			},
		}, NotifInitFn),
	}

	err_chan := make(chan error, len(services)+1)

	confSvc := config_service.NewService(nil, conf_svc.InitFn)

	conf_srv_conf := &objects.GrpcServerConf{
		GrpcServerConfig: conf_svc.ConfiguratorDefaultConf.ConfigValues[0].GetServer()}
	conf_srv_conf.LogLevel = 12

	if len(os.Getenv("ZOOKEEPER")) > 0 {
		if conf_srv_conf.GetLockerConfig("zookeeper") != nil {
			conf_srv_conf.GetLockerConfig("zookeeper").Hostname = os.Getenv("ZOOKEEPER")
		}
	}

	if len(os.Getenv("REDIS")) > 0 {
		if conf_srv_conf.GetLockerConfig("redis") != nil {
			conf_srv_conf.GetLockerConfig("redis").Hostname = os.Getenv("REDIS")
		}
		if conf_srv_conf.GetDBConfig("redis") != nil {
			conf_srv_conf.GetDBConfig("redis").Hostname = os.Getenv("REDIS")
		}
	}

	confSvc.UpdateConfig(conf_svc.ConfiguratorDefaultConf)
	go func(ctx context.Context, errc chan error, svc_ config_service.Service) {
		started := false
		for {
			select {
			case <-ctx.Done():
				return
			default:
				if !started {
					go func() {
						err := svc_.Start(ctx)
						if err != nil {
							log.Println("START ERR:" + err.Error())
						}
						errc <- err
					}()
					started = true
				}
			}
		}
	}(ctx, err_chan, confSvc)

	wg := sync.WaitGroup{}

	for idx := range services {
		cli := client.NewConfigClient("localhost:10010", services[idx])
		go func(errc chan error, cli_ *client.ConfigClient) {
			started := false
			for {
				select {
				case <-ctx.Done():
					log.Println("Received stop signal. Stopping client...")
					_ = cli_.Stop()
					return
				default:
					if !started {
						cli_.Start()
						started = true
					}
					if cli_.Status() == client.STOPPED {
						log.Println("Client has stopped. Return error to parent.")
						errc <- errors.New("Client has stopped.")
						return
					}
				}
			}
		}(err_chan, cli)
		wg.Add(1)
		start := time.Now()
		go func(cli_ *client.ConfigClient, wg_ *sync.WaitGroup, start_time time.Time) {
			defer wg.Done()
			for {
				if cli_.Status() == client.RUNNING {
					return
				}
				time.Sleep(time.Second)
				if time.Since(start_time) > time.Minute {
					// Return to allow error processing. Start is waiting to return because of
					// this.
					log.Println("Client hasnt moved to running for too long.")
					return
				}
			}
		}(cli, &wg, start)
	}

	wg.Wait()

	return err_chan
}

var (
	postgres_hostname = func() string {
		if len(os.Getenv("POSTGRES")) > 0 {
			return os.Getenv("POSTGRES")
		}
		log.Println("Defaulting to localhost")
		return "localhost"
	}

	redis_hostname = func() string {
		if len(os.Getenv("REDIS")) > 0 {
			return os.Getenv("REDIS")
		}
		log.Println("Defaulting to localhost")
		return "localhost"
	}
)

func Cleanup() {
	c, err := redis.Dial("tcp", redis_hostname()+":"+strconv.Itoa(6379))
	if err != nil {
		log.Printf("Failed opening conn to Redis Err:%s", err.Error())
	} else {
		_, err = c.Do("FLUSHALL")
		if err != nil {
			log.Printf("Failed cleaning up Redis Err:%s", err.Error())
		}
	}

	err = os.RemoveAll("resources/output")
	if err != nil {
		log.Printf("Failed removing output artifacts Err:%s", err.Error())
	}

	err = os.Mkdir("resources/output", 0777)
	if err != nil {
		log.Printf("Failed removing output artifacts Err:%s", err.Error())
	}
}
