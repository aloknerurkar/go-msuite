package test

import (
	"gitlab.com/go-msuite/_test/tests"
	"golang.org/x/net/context"
	"log"
	"os"
	"testing"
	"time"
)

func TestMain(t *testing.M) {
	ctx, cancel := context.WithCancel(context.Background())
	// In case of developer running tests this maybe helpful.
	Cleanup()

	var errc <-chan error
	errc = StartServices(ctx)
	start := time.Now()
	start_test := false

	tests.EmailChan = make(chan string, 3)

	for {
		select {
		case err := <-errc:
			log.Printf("Start services returned error %s.", err.Error())
			cancel()
			os.Exit(-1)
		default:
			if time.Since(start) > (time.Second * 15) {
				start_test = true
				log.Println("Waited 15 secs for services to return error. Starting test.")
				break
			}
			time.Sleep(time.Second * 3)
		}
		if start_test {
			break
		}
	}
	ret := t.Run()
	cancel()

	Cleanup()

	os.Exit(ret)
}

func TestRunner(t *testing.T) {
	t.Run("phase=1", func(t *testing.T) {
		at := &tests.AuthTests{Test: t}
		at.RunAuthTests()
	})
}
