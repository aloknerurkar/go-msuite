#!/usr/bin/env bash

if [[ -n $POSTGRES ]]
then
    echo "CI Mode..."
    echo "POSTGRES IP:"$POSTGRES
    echo "REDIS IP:"$REDIS
    echo "ZOOKEEPER IP:"$ZOOKEEPER
    exit 0
fi

POSTGRES=postgresql@9.6
REDIS=redis-server
ZOOKEEPER=zookeeper

echo "===== Checking for postgresql server 9.6 ====="
if [[ -z $(ps -ef |grep ${POSTGRES}) ]]
then
    echo "===== Postgres not present ====="
    exit -1
fi

echo "===== Checking for redis-server ====="
if [[ -z $(ps -ef |grep ${REDIS}) ]]
then
    echo "===== Redis not present ====="
    exit -1
fi

echo "===== Checking for zookeeper ====="
if [[ -z $(ps -ef |grep ${ZOOKEEPER}) ]]
then
    echo "===== Zookeeper not present ====="
    exit -1
fi