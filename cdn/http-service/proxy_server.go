package http_service

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/grpc-ecosystem/grpc-gateway/utilities"
	CDN "gitlab.com/go-msuite/cdn/pb"
	"gitlab.com/go-msuite/common/go-common"
	"gitlab.com/go-msuite/common/objects"
	"google.golang.org/grpc"
	"io"
	"log"
	"net/http"
	"strings"
	"time"
)

const (
	IMAGE_FOLDER_PATH  = "./osFileStore/"
	DEFAULT_BLOCK_SIZE = 1024
)

var global_endpoint string
var global_opts []grpc.DialOption

func getCDNClient() (CDN.CDNClient, func(), error) {
	conn, err := grpc.Dial(global_endpoint, global_opts...)
	if err != nil {
		return nil, nil, err
	}
	return CDN.NewCDNClient(conn), func() {
		conn.Close()
	}, nil
}

func uploadHandler(w http.ResponseWriter, r *http.Request, pathParams map[string]string) {

	//parse the multipart form in the request
	err := r.ParseMultipartForm(100000)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	//get a ref to the parsed multipart form
	multipartForm := r.MultipartForm

	files := multipartForm.File["file"]
	// Allow exactly one fileupload at a time, for now
	if len(files) != 1 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	client, closeFn, err := getCDNClient()
	if err != nil {
		log.Println("Unable to get CDN client RPC")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer closeFn()

	const file_index = 0
	//for each fileheader, get a handle to the actual file
	splits := strings.Split(files[file_index].Filename, ".")
	file_extension := splits[len(splits)-1]
	size := files[file_index].Size
	file, err := files[file_index].Open()

	var metadata = CDN.Metadata{}
	metadata.Size = int32(size)
	metadata.Created = time.Now().Unix()

	if strings.EqualFold(file_extension, "png") {
		metadata.Ext = CDN.ImageExt_PNG
	} else if strings.EqualFold(file_extension, "jpg") || strings.EqualFold(file_extension, "jpeg") {
		metadata.Ext = CDN.ImageExt_JPEG
	} else {
		metadata.Ext = CDN.ImageExt_RESERVED
	}

	upCli, err := client.Upload(context.Background())
	if err != nil {
		log.Printf("Unable to upload image. Err:%s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
	}

	err = upCli.Send(&CDN.UploadReq{Content: &CDN.UploadReq_Mtdt{Mtdt: &metadata}})
	if err != nil {
		log.Printf("Failed sending metadata Err:%s Mtdt:%v", err, metadata)
		w.WriteHeader(http.StatusInternalServerError)
	}
	var off int32
	buf := make([]byte, DEFAULT_BLOCK_SIZE)
	failed := true
	for {
		n, err := file.ReadAt(buf, int64(off))
		if err != nil && err != io.EOF {
			//Something went wrong. We got some error apart from EOF
			log.Printf("Failed reading file at offset %d", off)
			break
		}

		done := (err == io.EOF)

		data := &CDN.UploadReq{Content: &CDN.UploadReq_Data{Data: &CDN.DataBlk{Offset: off, Data: buf[:n]}}}
		err = upCli.Send(data)
		if err != nil {
			log.Printf("Failed sending file blk %v", data)
			break
		}

		if done {
			failed = false
			break
		}
		off += int32(n)
	}

	if !failed {
		resp, err := upCli.CloseAndRecv()
		if err != nil {
			log.Printf("Failed getting response for upload. Err:%s", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		metadata.Id = resp.PhotoId
		metadata.FullPath = resp.PhotoId + "." + file_extension
	} else {
		_ = upCli.CloseSend()
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	jsonBuffer, err := json.Marshal(metadata)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonBuffer)
}

func customFileServerHandler(w http.ResponseWriter, r *http.Request, pathParams map[string]string) {
	http.ServeFile(w, r, IMAGE_FOLDER_PATH+pathParams["uuid"])
}

func Init(ctx context.Context, mux *runtime.ServeMux, conf *objects.ProxyConf, opts []grpc.DialOption) error {
	c := conf.GetProxyEp(go_common.CDN_SVC)
	if c == nil {
		return errors.New("CDN endpoint absent.")
	}

	global_opts = opts
	global_endpoint = c.Hostname

	// pattern to accept a normal get request - v1/multipart_image_upload
	pattern_uploadImage, _ := runtime.NewPattern(1,
		[]int{
			int(utilities.OpLitPush), 0,
			int(utilities.OpLitPush), 1,
		},
		[]string{
			"v1",
			"multipart_image_upload",
		},
		"")

	// accepts UUID as an argument via URL - v1/images/{uuid}
	pattern_getImage := runtime.MustPattern(
		runtime.NewPattern(
			1,
			[]int{2, 0, 2, 1, 1, 0, 4, 1, 5, 2},
			[]string{
				"v1",
				"images",
				"uuid",
			},
			"",
		),
	)

	mux.Handle("POST", pattern_uploadImage, uploadHandler)
	mux.Handle("GET", pattern_getImage, customFileServerHandler)

	return CDN.RegisterCDNHandlerFromEndpoint(ctx, mux, c.Hostname, opts)
}
