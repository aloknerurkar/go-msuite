package pb

import (
	"net/http"

	"context"
	"errors"
	"github.com/golang/protobuf/proto"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"io"
	"log"
	"reflect"
	"strconv"
)

func displayCheckoutResp(ctx context.Context, mux *runtime.ServeMux, marshaler runtime.Marshaler, w http.ResponseWriter,
	req *http.Request, recv func() (proto.Message, error),
	opts ...func(context.Context, http.ResponseWriter, proto.Message) error) {
	var cont_type string
	vals := req.URL.Query()

	switch vals.Get("ext") {
	case ImageExt_JPEG.String():
		cont_type = "image/jpeg"
	case ImageExt_PNG.String():
		cont_type = "image/png"
	default:
		http.Error(w, "Invalid image type.", http.StatusNotImplemented)
		return
	}

	wd, err := strconv.Atoi(vals.Get("get.width"))
	if err != nil {
		http.Error(w, "Invalid image width.", http.StatusBadRequest)
		return
	}
	ht, err := strconv.Atoi(vals.Get("get.height"))
	if err != nil {
		http.Error(w, "Invalid image height.", http.StatusBadRequest)
		return
	}

	log.Printf("Width %d Height %d Tx %s\n", wd, ht, vals.Get("get.tx"))
	// Creating ImgMarshaller
	img_marshlr := &ImgMarshal{content_type: cont_type}

	runtime.ForwardResponseStream(ctx, mux, img_marshlr, w, req, recv, opts...)
}

func init() {
	forward_CDN_Display_0 = displayCheckoutResp
}

type ImgMarshal struct {
	content_type string
}

func (i *ImgMarshal) ContentType() string {
	return i.content_type
}

var ErrInvalidArgument = errors.New("ImgMarshal: Invalid argument provided.")
var ErrNotImplemented = errors.New("ImgMarshal: Not implemented.")

func (i *ImgMarshal) Marshal(v interface{}) ([]byte, error) {
	msg_map, ok := v.(map[string]proto.Message)
	if !ok {
		log.Println("Invalid msg type provided. Type:" + reflect.TypeOf(v).String())
		return nil, ErrInvalidArgument
	}

	msg, ok := msg_map["result"].(*PhotoData)
	if !ok {
		log.Println("Invalid proto msg type provided. Type:" + reflect.TypeOf(v).String())
		return nil, ErrInvalidArgument
	}

	return msg.Data, nil
}

func (i *ImgMarshal) Unmarshal(data []byte, v interface{}) error {
	return ErrNotImplemented
}

func (i *ImgMarshal) NewDecoder(r io.Reader) runtime.Decoder {
	return runtime.DecoderFunc(func(v interface{}) error { return ErrNotImplemented })
}

func (i *ImgMarshal) NewEncoder(w io.Writer) runtime.Encoder {
	return runtime.EncoderFunc(func(v interface{}) error { return ErrNotImplemented })
}
