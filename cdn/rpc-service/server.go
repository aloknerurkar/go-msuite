package rpc_service

import (
	"bytes"
	"fmt"
	lg "github.com/aloknerurkar/log-post/client"
	"github.com/nfnt/resize"
	"github.com/pkg/errors"
	"gitlab.com/go-msuite/app-errors"
	CDN "gitlab.com/go-msuite/cdn/pb"
	"gitlab.com/go-msuite/common/go-common"
	"gitlab.com/go-msuite/common/objects"
	Messages "gitlab.com/go-msuite/common/pb"
	"gitlab.com/go-msuite/configurator/config_service/grpc_server"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	"gitlab.com/go-msuite/locker"
	"gitlab.com/go-msuite/store"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"image"
	"image/jpeg"
	"image/png"
	"io"
	"os"
	"sync/atomic"
)

const (
	VERSION         = "v1.0.0"
	DEFAULT_BLK_LEN = 4096
)

var (
	req_id int64 = 1
)

type cdnServer struct {
	conf *objects.GrpcServerConf
	mdbP store.Store
	dbP  store.Store
	logr *lg.LogPostClient
	lckr locker.Locker
}

type metadataObj struct {
	*CDN.Metadata
}

func (m *metadataObj) GetNamespace() string { return go_common.CDN_SVC }

func (m *metadataObj) SetCreated(i int64) { m.Created = i }

func (m *metadataObj) SetUpdated(i int64) { m.Updated = i }

type fileObj struct {
	*os.File
	name string
}

func (f *fileObj) GetId() string { return f.name }

func (f *fileObj) GetNamespace() string { return go_common.CDN_SVC }

func (f *fileObj) SetId(i string) { f.name = i }

func (f *fileObj) SetFp(fp *os.File) { f.File = fp }

var InitFn grpc_server.RPCInitFn = Init

func Init(srv_conf *objects.GrpcServerConf, srv *grpc.Server) error {

	lgr := lg.InitLogger(srv_conf.SvcName, VERSION, srv_conf.LogLevel, false)

	lockerConf := srv_conf.GetLockerConfig("zookeeper")
	if lockerConf == nil {
		return lgr.Error(errors.New("Locker Config absent"), "Locker config not provided")
	}

	lockr, err := locker.New(lockerConf)
	if err != nil {
		return lgr.Error(err, "Failed initializing wallet locker")
	}

	db1_conf := srv_conf.GetDBConfig("os_filestore")
	if db1_conf == nil {
		return lgr.Error(errors.New("CDN DB Config absent"), "CDN DB config not provided")
	}

	dbp, err := store.NewStore(db1_conf)
	if err != nil {
		return lgr.Error(err, "Failed initializing CDN DB")
	}

	db2_conf := srv_conf.GetDBConfig("redis")
	if db2_conf == nil {
		return lgr.Error(errors.New("CDN Mtdt DB Config absent"), "CDN Mtdt DB config not provided")
	}

	mdbp, err := store.NewStore(db2_conf)
	if err != nil {
		return lgr.Error(err, "Failed initializing CDN Mtdt DB")
	}

	CDN.RegisterCDNServer(srv, &cdnServer{
		conf: srv_conf,
		dbP:  dbp,
		mdbP: mdbp,
		logr: lgr,
		lckr: lockr,
	})
	return nil
}

func (s *cdnServer) GetImageUrl(c context.Context, gt *CDN.GetReq) (ret_photo *CDN.PhotoUrl, ret_err error) {

	_req_id := atomic.AddInt64(&req_id, 1)
	s.logr.TraceStart("ReqID: %d", _req_id)
	defer s.logr.TraceEnd("ReqID: %d", _req_id)

	mtdt := &metadataObj{
		&CDN.Metadata{
			Id: gt.PhotoId,
		},
	}

	err := s.mdbP.Read(mtdt)
	if err != nil {
		ret_err = s.logr.Error(app_errors.ErrInvalidArg("Image not found."),
			"Failed querying image ID %s SecErr:%s", gt.PhotoId, err.Error())
		return
	}

	ret_photo = new(CDN.PhotoUrl)

	host := s.conf.GetCDNConfig()
	if host == nil {
		host = &Configurator.CDNHostConfig{
			Hostname: "api.pocketbarter.com",
			Port:     "80",
			BaseUrl:  "v1/display",
			Protocol: "http",
		}
	}

	baseUrl := fmt.Sprintf("%s//%s:%s/%s", host.Protocol, host.Hostname, host.Port, host.BaseUrl)
	ret_photo.PhotoUrl = fmt.Sprintf("%s/%s?get.tx=%s&get.width=%d&get.height=%d&ext=%s", baseUrl, mtdt.Id,
		gt.Tx.String(), gt.Width, gt.Height, mtdt.Ext.String())
	return
}

func (s *cdnServer) Upload(srv CDN.CDN_UploadServer) (ret_err error) {

	_req_id := atomic.AddInt64(&req_id, 1)
	s.logr.TraceStart("ReqID: %d", _req_id)
	defer s.logr.TraceEnd("ReqID: %d", _req_id)

	rcvd_mtdt := false
	buf := make([]*CDN.DataBlk, 0)
	var mtdt *metadataObj
	var ret_resp *CDN.UploadResp

	file := &fileObj{}

	err := s.dbP.Create(file)
	if err != nil {
		ret_err = s.logr.Error(app_errors.ErrInternal("Failed creating file."),
			"Something went wrong while opening File. SecErr:%s.", err.Error())
		return
	}
	defer file.Close()

	writeToFile := func(fp *os.File, bytes []byte, off int32) error {
		size, err := fp.WriteAt(bytes, int64(off))
		if err != nil {
			return err
		} else if size != len(bytes) {
			return errors.New("Failed writing byte length")
		}
		return nil
	}

	s.logr.Info("CREATED FILE: %s", file.File.Name())

	for {
		msg, err := srv.Recv()
		if err == io.EOF {
			break
		} else if err != nil {
			ret_err = s.logr.Error(app_errors.ErrInvalidArg("Error while receiving file."),
				"Got error while receiving file from client SecErr:%s", err.Error())
			goto done
		}

		s.logr.Info("Received stream message: %v", msg)

		switch msg.GetContent().(type) {
		case *CDN.UploadReq_Mtdt:
			if rcvd_mtdt {
				ret_err = s.logr.Error(app_errors.ErrInvalidArg("Got more than 1 Metadata block."),
					"Something went wrong on the client. Got multiple mtdt.")
				break
			}
			mtdt = &metadataObj{
				Metadata: msg.GetMtdt(),
			}

			mtdt.Id = file.GetId()
			err = s.mdbP.Create(mtdt)
			if err != nil {
				ret_err = s.logr.Error(app_errors.ErrInternal("Failed storing metadata."),
					"Failed storing metadata entry. SecErr:%s", err.Error())
				break
			}
			rcvd_mtdt = true
		case *CDN.UploadReq_Data:
			data := msg.GetData()
			if !rcvd_mtdt {
				buf = append(buf, data)
			} else {
				for i := range buf {
					err := writeToFile(file.File, buf[i].Data, buf[i].Offset)
					if err != nil {
						ret_err = s.logr.Error(app_errors.ErrInternal("Failed writing to file"),
							"Failed writing from buffer. SecErr:%s", err.Error())
						break
					}
				}
				err := writeToFile(file.File, data.Data, data.Offset)
				if err != nil {
					ret_err = s.logr.Error(app_errors.ErrInternal("Failed writing to file"),
						"Failed writing block. SecErr:%s", err.Error())
					break
				}
			}
		}
	}
done:
	if ret_err == nil {
		ret_resp = &CDN.UploadResp{PhotoId: mtdt.GetId()}
		_ = srv.SendAndClose(ret_resp)
	} else {
		_ = s.dbP.Delete(file)
		if rcvd_mtdt {
			_ = s.mdbP.Delete(mtdt)
		}
	}
	return
}

func (s *cdnServer) Remove(c context.Context, remove_req *Messages.ReqUUID) (ret_emp *Messages.EmptyMessage, ret_err error) {

	_req_id := atomic.AddInt64(&req_id, 1)
	s.logr.TraceStart("ReqID: %d", _req_id)
	defer s.logr.TraceEnd("ReqID: %d", _req_id)

	item := &metadataObj{
		&CDN.Metadata{
			Id: remove_req.Uuid,
		},
	}

	file := &fileObj{
		name: remove_req.Uuid,
	}

	err := s.mdbP.Delete(item)
	if err != nil {
		ret_err = s.logr.Error(app_errors.ErrInternal("Failed to remove metadata item."),
			"Failed deleting image mtdt for ID %s SecErr:%s",
			remove_req.Uuid, err.Error())
		return
	}

	err = s.dbP.Delete(file)
	if err != nil {
		ret_err = s.logr.Error(app_errors.ErrInternal("Failed to remove image."),
			"Failed deleting image ID %s SecErr:%s",
			remove_req.Uuid, err.Error())
		return
	}

	ret_emp = &Messages.EmptyMessage{}

	return
}

func imageTx(img image.Image, width, height uint, tx string) image.Image {
	switch tx {
	case "TX_NONE":
		return img
	case "RESIZE":
		return resize.Resize(width, height, img, resize.Lanczos3)
	case "CROP":
		return resize.Thumbnail(width, height, img, resize.Lanczos3)
	}
	panic("Invalid image transformation type.")
}

func (s *cdnServer) Display(ph *CDN.PhotoReq, srv CDN.CDN_DisplayServer) (ret_err error) {

	_req_id := atomic.AddInt64(&req_id, 1)
	s.logr.TraceStart("ReqID: %d", _req_id)
	defer s.logr.TraceEnd("ReqID: %d", _req_id)

	file := &fileObj{
		name: ph.Get.PhotoId,
	}

	err := s.dbP.Read(file)
	if err != nil {
		ret_err = s.logr.Error(app_errors.ErrInternal("Failed opening file."), "Failed to open file.")
		return
	}

	img, _, err := image.Decode(file)
	if err != nil {
		ret_err = s.logr.Error(app_errors.ErrInternal("Failed decoding image file."), "Failed to decode file.")
		return
	}

	tx_img := imageTx(img, uint(ph.Get.Width), uint(ph.Get.Width), ph.Get.Tx.String())

	buf := new(bytes.Buffer)

	switch ph.Ext {
	case CDN.ImageExt_JPEG.String():
		err = jpeg.Encode(buf, tx_img, nil)
	case CDN.ImageExt_PNG.String():
		err = png.Encode(buf, tx_img)
	default:
		ret_err = s.logr.Error(app_errors.ErrInvalidArg("Invalid image extension."),
			"Image extension %s not supported.", ph.Ext)
		return
	}

	if err != nil {
		ret_err = s.logr.Error(app_errors.ErrInternal("Failed encoding image file."),
			"Failed to encode %s file. SecErr:%s", ph.Ext, err.Error())
		return
	}

	var buf_to_send []byte

	for buf.Len() > 0 {
		if buf.Len() > DEFAULT_BLK_LEN {
			buf_to_send = buf.Next(DEFAULT_BLK_LEN)
		} else {
			buf_to_send = buf.Next(buf.Len())
		}
		err = srv.Send(&CDN.PhotoData{Data: buf_to_send})
		if err != nil {
			ret_err = s.logr.Error(app_errors.ErrInternal("Failed to send file."),
				"Failed to send. SecErr:%s.", err.Error())
			return
		}
	}

	s.logr.Info("Done sending file %s.", ph.Get.PhotoId)

	return
}
