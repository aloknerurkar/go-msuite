package rpc_service

import (
	"errors"
	lg "github.com/aloknerurkar/log-post/client"
	"gitlab.com/go-msuite/app-errors"
	"gitlab.com/go-msuite/common/go-common"
	"gitlab.com/go-msuite/common/objects"
	"gitlab.com/go-msuite/common/objects/utils"
	Messages "gitlab.com/go-msuite/common/pb"
	"gitlab.com/go-msuite/configurator/config_service/grpc_server"
	"gitlab.com/go-msuite/locker"
	Payments "gitlab.com/go-msuite/payments/pb"
	"gitlab.com/go-msuite/payments/providers"
	"gitlab.com/go-msuite/store"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"sync"
	"sync/atomic"
)

const (
	PKG_NAME = "kuber-payments"
	VERSION  = "v1.0.0"
)

var (
	req_id int64 = 1
)

type chargeObj struct {
	*Payments.Charge
}

func (l *chargeObj) GetNamespace() string { return go_common.PAYMENTS_SVC + "_" + "charge" }

func (l *chargeObj) GetId() string { return l.ChargeId }

func (l *chargeObj) SetId(id string) { l.ChargeId = id }

func (l *chargeObj) SetCreated(i int64) { l.Created = i }

func (l *chargeObj) SetUpdated(i int64) { l.Updated = i }

type payments struct {
	conf    *objects.GrpcServerConf
	logr    *lg.LogPostClient
	dbP     store.Store
	lckr    locker.Locker
	provdrs *providers.Providers
}

var InitFn grpc_server.RPCInitFn = Init

func Init(srv_conf *objects.GrpcServerConf, srv *grpc.Server) error {

	lgr := lg.InitLogger(srv_conf.SvcName, VERSION, srv_conf.LogLevel, false)

	dbConf := srv_conf.GetDBConfig("redis")
	if dbConf == nil {
		return lgr.Error(errors.New("DB Config absent"), "Database config not provided")
	}

	dbp, err := store.NewStore(dbConf)
	if err != nil {
		return lgr.Error(err, "Failed initializing wallet db")
	}

	lockerConf := srv_conf.GetLockerConfig("zookeeper")
	if lockerConf == nil {
		return lgr.Error(errors.New("Locker Config absent"), "Locker config not provided")
	}

	lockr, err := locker.New(lockerConf)
	if err != nil {
		return lgr.Error(err, "Failed initializing wallet locker")
	}

	payment_providers := make([]interface{}, 0)

	wallet_conf := srv_conf.GetClientConfig(go_common.WALLET_SVC)
	pr := srv_conf.GetPaymentProviders()

	if wallet_conf != nil {
		payment_providers = append(payment_providers, wallet_conf)
	}

	if len(pr) > 0 {
		for _, v := range pr {
			payment_providers = append(payment_providers, v)
		}
	}

	if len(payment_providers) == 0 {
		lgr.Error(errors.New("Payment providers absent"), "No providers configured")
	}

	p, err := providers.NewProviders(payment_providers)
	if err != nil {
		return err
	}

	Payments.RegisterPaymentsServer(srv, &payments{
		conf:    srv_conf,
		logr:    lgr,
		provdrs: p,
		dbP:     dbp,
		lckr:    lockr,
	})
	return nil
}

func (p *payments) NewCharge(c context.Context, pay_req *Payments.ChargeReq) (ret_charge *Payments.Charge,
	ret_err error) {
	_req_id := atomic.AddInt64(&req_id, 1)
	p.logr.TraceStart("ReqID: %d", _req_id)
	defer p.logr.TraceEnd("ReqID: %d", _req_id)

	pr := p.provdrs.GetProvider(pay_req.Provider)

	if pr.ProviderId() != Payments.ProviderId_WALLET {
		supported := false
		for _, v := range pr.SupportedCards() {
			if v == pay_req.Source.(*Payments.ChargeReq_Card).Card.Type {
				supported = true
				break
			}
		}
		if !supported {
			ret_err = p.logr.Error(app_errors.ErrInvalidArg("Card type not supported."),
				"ReqID:%d Req:%v", _req_id, pay_req)
			return
		}
	}

	ret_charge, ret_err = pr.Charge(pay_req)
	if ret_err != nil {
		ret_err = p.logr.Error(app_errors.ErrInternal("Failed to charge."),
			"ReqID:%d SecErr:%s Req:%v", _req_id, ret_err.Error(), pay_req)
		return
	}

	ret_charge.Status = Payments.ChargeStatus_PAID

	ch := &chargeObj{
		Charge: ret_charge,
	}

	ret_err = utils.WithRetry(func() error {
		return p.dbP.Create(ch)
	}, 2)
	if ret_err != nil {
		if _, err := pr.Refund(&Payments.RefundReq{
			ProviderRef: ret_charge.ProviderRef,
			Type:        Payments.Refund_SERVER_ERROR,
			Amount:      ret_charge.ChargeAmount,
			Currency:    ret_charge.Currency}); err != nil {
			ret_err = p.logr.Error(app_errors.ErrInternal("Failed to store charge and then failed to refund."),
				"ReqID:%d SecErr:%s Req:%v", _req_id, err.Error(), pay_req)
			return
		}
		ret_err = p.logr.Error(app_errors.ErrInternal("Failed to store charge, amount refunded."),
			"ReqID:%d SecErr:%s Req:%v", _req_id, ret_err.Error(), pay_req)
	}

	return
}

func (p *payments) RefundCharge(c context.Context, refund_req *Payments.RefundReq) (ret_refund *Payments.Refund,
	ret_err error) {
	_req_id := atomic.AddInt64(&req_id, 1)
	p.logr.TraceStart("ReqID: %d", _req_id)
	defer p.logr.TraceEnd("ReqID: %d", _req_id)

	charge := &chargeObj{
		Charge: &Payments.Charge{
			ChargeId: refund_req.ChargeId,
		},
	}

	unlock, err := p.lckr.TryLock(charge, locker.DefaultTimeout)
	if err != nil {
		ret_err = p.logr.Error(app_errors.ErrResourceExhausted("Unable to lock charge."),
			"ReqID:%d SecErr:%s Req:%v", _req_id, err.Error(), refund_req)
		return
	}
	defer unlock()

	err = p.dbP.Read(charge)
	if err != nil {
		ret_err = p.logr.Error(app_errors.ErrInvalidArg("Unable to find charge."),
			"ReqID:%d SecErr:%s Req:%v", _req_id, err.Error(), refund_req)
		return
	}

	if charge.Status != Payments.ChargeStatus_PAID || charge.ChargeAmount <= 0 ||
		charge.ChargeAmount < refund_req.Amount || (charge.RefundAmount+refund_req.Amount) > charge.ChargeAmount {
		ret_err = p.logr.Error(app_errors.ErrPermissionDenied("Refund not available for this charge."),
			"ReqID:%d Req:%v", _req_id, refund_req)
		return
	}
	refund_req.ProviderRef = charge.ProviderRef

	pr := p.provdrs.GetProvider(charge.Provider)

	ret_refund, ret_err = pr.Refund(refund_req)
	if ret_err != nil {
		ret_err = p.logr.Error(app_errors.ErrInternal("Unable to process refund."),
			"ReqID:%d SecErr:%s Req:%v", _req_id, ret_err.Error(), refund_req)
		return
	}

	charge.RefundAmount += ret_refund.RefundAmount
	charge.Status = Payments.ChargeStatus_REFUNDED
	charge.RefundInfo = append(charge.RefundInfo, ret_refund)

	ret_err = utils.WithRetry(func() error {
		return p.dbP.Update(charge)
	}, 2)
	if ret_err != nil {
		ret_err = p.logr.Error(app_errors.ErrInternal("Unable to update charge."),
			"ReqID:%d SecErr:%s Req:%v", _req_id, ret_err.Error(), charge)
		return
	}

	return
}

func (p *payments) GetCharge(c context.Context, id *Messages.ReqUUID) (ret_charge *Payments.Charge, ret_err error) {

	_req_id := atomic.AddInt64(&req_id, 1)
	p.logr.TraceStart("ReqID: %d", _req_id)
	defer p.logr.TraceEnd("ReqID: %d", _req_id)

	charge := &chargeObj{
		Charge: &Payments.Charge{
			ChargeId: id.Uuid,
		},
	}

	unlock, err := p.lckr.TryLock(charge, locker.DefaultTimeout)
	if err != nil {
		ret_err = p.logr.Error(app_errors.ErrResourceExhausted("Unable to lock charge."),
			"ReqID:%d SecErr:%s Req:%v", _req_id, err.Error(), id)
		return
	}
	defer unlock()

	err = p.dbP.Read(charge)
	if err != nil {
		ret_err = p.logr.Error(app_errors.ErrInvalidArg("Unable to find charge."),
			"ReqID:%d SecErr:%s Req:%v", _req_id, err.Error(), id)
		return
	}

	ret_charge = charge.Charge

	return
}

func (p *payments) GetCharges(c context.Context, ids *Messages.ReqUUIDList) (ret_charges *Payments.Charges, ret_err error) {

	_req_id := atomic.AddInt64(&req_id, 1)
	p.logr.TraceStart("ReqID: %d", _req_id)
	defer p.logr.TraceEnd("ReqID: %d", _req_id)

	wg := sync.WaitGroup{}
	mtx := sync.Mutex{}
	var errs []error

	ret_charges = &Payments.Charges{}
	ret_charges.Charges = make([]*Payments.Charge, 0)

	for idx := range ids.Uuids {
		wg.Add(1)
		go func(wg *sync.WaitGroup, id_str *Messages.ReqUUID) {
			defer wg.Done()
			chrg := &chargeObj{
				Charge: &Payments.Charge{
					ChargeId: id_str.Uuid,
				},
			}
			err := p.dbP.Read(chrg)
			if err != nil {
				mtx.Lock()
				errs = append(errs, err)
				mtx.Unlock()
				return
			}
			mtx.Lock()
			ret_charges.Charges = append(ret_charges.Charges, chrg.Charge)
			mtx.Unlock()
		}(&wg, ids.Uuids[idx])
	}
	wg.Wait()
	if errs != nil {
		ret_err = p.logr.Error(app_errors.ErrInternal("Failed getting charge list"),
			"ReqID: %d SecErr:%v", _req_id, errs[0])
		ret_charges = nil
	}

	return
}
