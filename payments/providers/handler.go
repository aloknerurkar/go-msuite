package providers

import (
	"errors"
	"gitlab.com/go-msuite/common/objects"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	Payments "gitlab.com/go-msuite/payments/pb"
	"gitlab.com/go-msuite/payments/providers/stripe"
	"gitlab.com/go-msuite/payments/providers/wallet"
	"log"
	"reflect"
	"sync"
)

type Provider interface {
	ProviderId() Payments.ProviderId
	SupportedCards() []Payments.CardType
	Charge(*Payments.ChargeReq) (*Payments.Charge, error)
	Refund(*Payments.RefundReq) (*Payments.Refund, error)
}

type Providers struct {
	providers map[Payments.ProviderId]Provider
	mtx       sync.Mutex
}

func (p *Providers) GetProvider(id Payments.ProviderId) Provider {
	p.mtx.Lock()
	defer p.mtx.Unlock()
	if v, ok := p.providers[id]; ok {
		return v
	}
	panic("Provider not found")
	return nil
}

func NewProviders(conf []interface{}) (*Providers, error) {
	p := new(Providers)
	p.providers = make(map[Payments.ProviderId]Provider)
	log.Printf("NO OF PROVIDERS:%d", len(conf))
	for _, v := range conf {
		var provider Provider
		var err error

		switch v.(type) {
		case *objects.GrpcClientConf:
			provider, err = wallet.NewWalletClient(v.(*objects.GrpcClientConf))
			if err != nil {
				return nil, err
			}
		case *Configurator.PaymentProviderConfig:
			switch v.(*Configurator.PaymentProviderConfig).Provider {
			case "stripe":
				provider, err = stripe.NewStripeClient(v.(*Configurator.PaymentProviderConfig))
				if err != nil {
					return nil, err
				}
			default:
				return nil, errors.New("Invalid payment provider.")
			}
		default:
			return nil, errors.New("Invalid payment provider conf. Type:" + reflect.TypeOf(v).String())
		}

		p.mtx.Lock()
		p.providers[provider.ProviderId()] = provider
		p.mtx.Unlock()
	}
	return p, nil
}
