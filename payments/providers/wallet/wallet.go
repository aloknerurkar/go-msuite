package wallet

import (
	"github.com/pkg/errors"
	"gitlab.com/go-msuite/common/objects"
	"gitlab.com/go-msuite/common/objects/utils"
	Messages "gitlab.com/go-msuite/common/pb"
	Payments "gitlab.com/go-msuite/payments/pb"
	Wallet "gitlab.com/go-msuite/wallet/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"time"
)

type provider struct {
	conf *objects.GrpcClientConf
}

func NewWalletClient(payClient *objects.GrpcClientConf) (*provider, error) {
	provider := new(provider)
	provider.conf = payClient
	return provider, nil
}

func (p *provider) SupportedCards() []Payments.CardType {
	return []Payments.CardType{
		Payments.CardType_CARD_RESERVED,
	}
}

func (p *provider) ProviderId() Payments.ProviderId {
	return Payments.ProviderId(Payments.ProviderId_WALLET)
}

func (p *provider) getWalletConn() (*grpc.ClientConn, utils.ConnDone, error) {
	conn, err := p.conf.NewRPCConn(p.conf.ServerAddr[0])
	return conn, func() {
		if conn != nil {
			_ = conn.Close()
			return
		}
	}, err
}

func (p *provider) Charge(req *Payments.ChargeReq) (*Payments.Charge, error) {

	user_id := req.GetUserId()
	if len(user_id) == 0 {
		return nil, errors.New("Invalid source.")
	}

	conn, done, err := p.getWalletConn()
	if err != nil {
		return nil, err
	}
	defer done()

	// perform new charge
	ret_charge, err := Wallet.NewWalletClient(conn).Charge(context.Background(), &Wallet.WChargeReq{
		UserId: user_id,
		Type:   Wallet.WalletCharge_DEBIT,
		Req:    req,
	})
	if err != nil {
		return nil, err
	}

	// return charge
	return &Payments.Charge{
		Provider:     p.ProviderId(),
		ProviderRef:  ret_charge.RefId,
		Status:       Payments.ChargeStatus_PAID,
		Email:        req.GetEmail(),
		Currency:     req.GetCurrency(),
		ChargeAmount: req.GetAmount(),
		Statement:    req.GetStatement(),
	}, nil

}

func (p *provider) Refund(req *Payments.RefundReq) (*Payments.Refund, error) {

	conn, done, err := p.getWalletConn()
	if err != nil {
		return nil, err
	}
	defer done()

	ret_charge, err := Wallet.NewWalletClient(conn).GetCharge(context.Background(), &Messages.ReqUUID{
		Uuid: req.ProviderRef})
	if err != nil {
		return nil, err
	}

	if ret_charge.Currency != req.Currency || ret_charge.Amount < req.Amount ||
		ret_charge.Type != Wallet.WalletCharge_DEBIT {
		return nil, errors.New("Charge not refundable.")
	}

	// perform refund
	ret_charge, err = Wallet.NewWalletClient(conn).Charge(context.Background(), &Wallet.WChargeReq{
		UserId: ret_charge.UserId,
		Type:   Wallet.WalletCharge_CREDIT,
		Req: &Payments.ChargeReq{
			Amount:    ret_charge.Amount,
			Currency:  ret_charge.Currency,
			Statement: "Refund for charge " + ret_charge.RefId,
			Source: &Payments.ChargeReq_VoucherId{
				VoucherId: req.ChargeId,
			},
		},
	})
	if err != nil {
		return nil, err
	}

	return &Payments.Refund{
		ProviderRef:  ret_charge.RefId,
		RefundAmount: ret_charge.Amount,
		Created:      time.Now().Unix(),
		Type:         req.Type,
	}, nil
}
