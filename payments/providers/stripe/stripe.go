package stripe

import (
	"github.com/pkg/errors"
	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/charge"
	"github.com/stripe/stripe-go/refund"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	Payments "gitlab.com/go-msuite/payments/pb"
	"log"
	"strings"
	"time"
)

var (
	// IncorrectNum incorrect cc num
	IncorrectNum error = errors.New("incorrect_number")
	// InvalidNum invalid cc num
	InvalidNum error = errors.New("invalid_number")
	// InvalidExpM invalid exp month
	InvalidExpM error = errors.New("invalid_expiry_month")
	// InvalidExpY invalid exp year
	InvalidExpY error = errors.New("invalid_expiry_year")
	// InvalidCvc invalid cvc number
	InvalidCvc error = errors.New("invalid_cvc")
	// ExpiredCard card is expired
	ExpiredCard error = errors.New("expired_card")
	// IncorrectCvc incorrect cvc
	IncorrectCvc error = errors.New("incorrect_cvc")
	// IncorrectZip incorrect zip code
	IncorrectZip error = errors.New("incorrect_zip")
	// CardDeclined card declined
	CardDeclined error = errors.New("card_declined")
	// Missing missing information
	Missing error = errors.New("missing")
	// ProcessingErr processing error
	ProcessingErr error = errors.New("processing_error")
	// RateLimit reached call rate limit
	RateLimit error = errors.New("rate_limit")
)

type provider struct {
}

type stripeLogger struct {
}

func (s *stripeLogger) Printf(format string, v ...interface{}) {
	log.Printf(format, v...)
}

// NewProvider creates and prepare the stripe provider
func NewStripeClient(paymentConfig *Configurator.PaymentProviderConfig) (*provider, error) {
	stripe.Key = paymentConfig.Secret
	stripe.Logger = &stripeLogger{}
	return &provider{}, nil
}

func (p *provider) SupportedCards() []Payments.CardType {
	return []Payments.CardType{
		Payments.CardType_MASTERCARD,
		Payments.CardType_VISA,
		Payments.CardType_AMEX,
		Payments.CardType_JCB,
		Payments.CardType_DISCOVER,
		Payments.CardType_DINERS_CLUB,
	}
}

func (p *provider) ProviderId() Payments.ProviderId {
	return Payments.ProviderId(Payments.ProviderId_STRIPE)
}

func (p *provider) Charge(req *Payments.ChargeReq) (*Payments.Charge, error) {
	// perform new charge
	ch, err := charge.New(&stripe.ChargeParams{
		Amount:   uint64(req.GetAmount()),
		Currency: stripe.Currency(strings.ToLower(req.GetCurrency().String())),
		Desc:     req.GetStatement(),
		Email:    req.GetEmail(),
		Source: &stripe.SourceParams{
			Card: &stripe.CardParams{
				Number: req.GetCard().GetCardNumber(),
				Month:  req.GetCard().GetExpireMonth(),
				Year:   req.GetCard().GetExpireYear(),
				CVC:    req.GetCard().GetCvc(),
				Name:   req.GetCard().GetLastName() + " " + req.GetCard().GetFirstName(),
			},
		},
	})

	// convert err
	if err != nil {
		switch x := err.(type) {
		case *stripe.Error:
			return nil, convertStripeError(x.Code)
		default:
			return nil, err
		}
	}

	if !ch.Paid {

	}

	// return charge
	return &Payments.Charge{
		Provider:     p.ProviderId(),
		ProviderRef:  ch.ID,
		Status:       Payments.ChargeStatus_PAID,
		Email:        req.GetEmail(),
		Currency:     req.GetCurrency(),
		ChargeAmount: req.GetAmount(),
		Statement:    req.GetStatement(),
	}, nil

}

func (p *provider) Refund(req *Payments.RefundReq) (*Payments.Refund, error) {
	var stripeReason stripe.RefundReason
	// convert reason to stripe reason
	switch req.Type {
	case Payments.Refund_FRAUD:
		stripeReason = "fraudulent"
	case Payments.Refund_DUPLICATE:
		stripeReason = "duplicate"
	case Payments.Refund_USER_REQUESTED:
		stripeReason = "requested_by_customer"
	}
	// perform refund
	rf, err := refund.New(&stripe.RefundParams{
		Amount: uint64(req.Amount),
		Charge: req.ProviderRef,
		Reason: stripeReason,
	})
	// convert err
	if err != nil {
		return nil, err
	}
	return &Payments.Refund{
		ProviderRef:  rf.ID,
		RefundAmount: int64(rf.Amount),
		Created:      time.Now().Unix(),
		Type:         req.Type,
	}, nil
}

func convertStripeError(errorCode stripe.ErrorCode) error {
	switch errorCode {
	case stripe.IncorrectNum:
		return IncorrectNum
	case stripe.InvalidNum:
		return InvalidNum
	case stripe.InvalidExpM:
		return InvalidExpM
	case stripe.InvalidExpY:
		return InvalidExpY
	case stripe.InvalidCvc:
		return InvalidCvc
	case stripe.ExpiredCard:
		return ExpiredCard
	case stripe.IncorrectCvc:
		return IncorrectCvc
	case stripe.IncorrectZip:
		return IncorrectZip
	case stripe.CardDeclined:
		return CardDeclined
	case stripe.Missing:
		return Missing
	case stripe.ProcessingErr:
		return ProcessingErr
	case stripe.RateLimit:
		return RateLimit
	default:
		break
	}
	return nil
}
