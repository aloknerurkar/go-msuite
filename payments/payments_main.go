package main

import (
	"flag"
	"gitlab.com/go-msuite/common/go-common"
	"gitlab.com/go-msuite/configurator/client"
	"gitlab.com/go-msuite/configurator/config_service"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	"gitlab.com/go-msuite/payments/rpc-service"
	"log"
	"os"
	"os/signal"
	"syscall"
)

const CONFIGURATOR_DEFAULT = "none"

var configurator = flag.String("configurator", CONFIGURATOR_DEFAULT, "Configurator service endpoint")

func main() {
	flag.Parse()

	ch := make(chan os.Signal)
	signal.Notify(ch, os.Interrupt, syscall.SIGINT)

	payments_svc := config_service.NewService(&Configurator.Request{
		SvcName: go_common.PAYMENTS_SVC,
		Version: "1.0.0",
		Configs: []*Configurator.ConfigReq{
			{
				ConfType: Configurator.ConfigType_GRPC_SERVER_CONFIG,
				Handler:  go_common.PAYMENTS_SVC,
				SubConfs: []*Configurator.ConfigReq{
					{
						ConfType: Configurator.ConfigType_DB_CONFIG,
						Handler:  "redis",
					},
					{
						ConfType: Configurator.ConfigType_GRPC_CLIENT_CONFIG,
						Handler:  go_common.WALLET_SVC,
					},
					{
						ConfType: Configurator.ConfigType_LOCKER_CONFIG,
						Handler:  "zookeeper",
					},
				},
			},
		},
	}, rpc_service.Init)
	c := client.NewConfigClient(*configurator, payments_svc)
	c.Start()
	sig := <-ch
	log.Printf("Received %v. Stopping service...", sig.String())
	_ = c.Stop()
}
