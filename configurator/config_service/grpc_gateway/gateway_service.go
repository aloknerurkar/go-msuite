package grpc_gateway

import (
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"gitlab.com/go-msuite/common/objects"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type HTTPInitFn func(context.Context, *runtime.ServeMux, *objects.ProxyConf, []grpc.DialOption) error

type GatewayServer interface {
	Start(context.Context, HTTPInitFn) error
}

type gatewayServer struct {
	*objects.ProxyConf
}

func NewGatewayService(conf *objects.ProxyConf) GatewayServer { return &gatewayServer{ProxyConf: conf} }

var LOG *log.Logger = log.New(os.Stdout, "gatewayServer Main:", log.Ldate|log.Ltime|log.Lshortfile)

func (gw *gatewayServer) Start(ctx context.Context, gwInit HTTPInitFn) error {
	// Emit default values through GRPC gateway
	// Refer : https://github.com/grpc-ecosystem/grpc-gateway/issues/233
	mux := runtime.NewServeMux(runtime.WithMarshalerOption(runtime.MIMEWildcard,
		&runtime.JSONPb{OrigName: true, EmitDefaults: true}))

	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := gwInit(ctx, mux, gw.ProxyConf, opts)
	if err != nil {
		LOG.Println("Error registering grpc-gateway:" + err.Error())
		return err
	}

	LOG.Printf("Registered grpc_gateway with %v. Running on port %d", gw.SvcName, gw.Port)

	httpServer := &http.Server{Addr: ":" + strconv.Itoa(int(gw.Port)), Handler: mux}
	started := false
	for {
		select {
		case <-ctx.Done():
			LOG.Println("Shutting down HTTP server.")
			return httpServer.Shutdown(nil)
		default:
			if !started {
				go func() {
					err = httpServer.ListenAndServe()
				}()
				started = true
			}
		}
	}
	return err
}
