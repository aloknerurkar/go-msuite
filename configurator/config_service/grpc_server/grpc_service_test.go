package grpc_server

import (
	"errors"
	"gitlab.com/go-msuite/common/objects"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"testing"
	"time"
)

func TestGrpcServer_Start(t *testing.T) {
	var errInitFn RPCInitFn = func(*objects.GrpcServerConf, *grpc.Server) error {
		return errors.New("Dummy Error")
	}

	var initFn RPCInitFn = func(*objects.GrpcServerConf, *grpc.Server) error {
		return nil
	}

	// The new server currently passes a pointer. So its not straightforward to mock this.
	// Instead we will simulate errors using the server config values. To know when it returns error
	// checkout server_obj.go
	testConf := &objects.GrpcServerConf{
		GrpcServerConfig: &Configurator.GrpcServerConfig{
			UseJwt:       true, // This will return error in GetServerOpts
			UseValidator: false,
			UseRecovery:  false,
		},
	}

	grpcSvc := &grpcServer{GrpcServerConf: testConf}

	err := grpcSvc.Start(context.Background(), initFn)
	if err == nil {
		t.Fatal("Expected error on start.")
	}

	testConf.UseJwt = false
	err = grpcSvc.Start(context.Background(), errInitFn)
	if err == nil {
		t.Fatal("Expected error on start.")
	}

	done := make(chan bool, 1)
	ctx, cancel := context.WithCancel(context.Background())
	go func(_ch chan bool) {
		err := grpcSvc.Start(ctx, initFn)
		if err != nil {
			_ch <- false
		} else {
			_ch <- true
		}
	}(done)

	// Issue stop
	cancel()

	start := time.Now()
	for {
		stop := false
		select {
		case done_sig := <-done:
			if !done_sig {
				t.Fatal("Service should not have returned error on external stop.")
			}
			stop = true
			break
		default:
			if time.Since(start) > time.Minute {
				t.Fatal("Service took too long to stop.")
			}
		}
		if stop {
			break
		}
	}
}
