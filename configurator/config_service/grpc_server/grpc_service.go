package grpc_server

import (
	"fmt"
	hb "gitlab.com/go-msuite/common/heartbeat-service"
	"gitlab.com/go-msuite/common/objects"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

type RPCInitFn func(*objects.GrpcServerConf, *grpc.Server) error

type GrpcServer interface {
	Start(context.Context, RPCInitFn) error
}

type grpcServer struct {
	*objects.GrpcServerConf
}

func NewGrpcService(conf *objects.GrpcServerConf) GrpcServer { return &grpcServer{GrpcServerConf: conf} }

var LOG *log.Logger = log.New(os.Stdout, "grpcServer Main:", log.Ldate|log.Ltime|log.Lshortfile)

func (s *grpcServer) Start(ctx context.Context, rpcInit RPCInitFn) error {

	if s.GetUseJwt() {
		LOG.Println("Using default auth function for JWT validation.")
	} else {
		LOG.Println("Disabling JWT validation.")
	}

	if s.GetUseRecovery() {
		LOG.Println("Using default recovery handler for grpc recovery.")
	} else {
		LOG.Println("Disabling grpc recovery.")
	}

	if s.GetUseValidator() {
		LOG.Println("Enabling grpc validator.")
	} else {
		LOG.Println("Disabling grpc validator.")
	}

	opts, err := s.GetServerOpts()
	if err != nil {
		LOG.Println("Error getting grpc server options:" + err.Error())
		return err
	}

	LOG.Printf("Using server options %v", opts)

	port := fmt.Sprintf(":%d", s.GetPort())
	lis, err := net.Listen("tcp", port)
	if err != nil {
		LOG.Println("Error starting TCP listener:" + err.Error())
		return err
	}
	LOG.Printf("Started TCP listener on [%s]\n", port)
	defer lis.Close()

	grpcServer := grpc.NewServer(opts...)
	err = rpcInit(s.GrpcServerConf, grpcServer)
	if err != nil {
		LOG.Println("Error getting grpc server options:" + err.Error())
		return err
	}
	LOG.Println("Initialized RPC service. Starting.")

	LOG.Println("Registering heartbeat service for " + s.GetSvcName())
	hb.Init(s.GetSvcName(), grpcServer)

	started := false
	for {
		select {
		case <-ctx.Done():
			LOG.Println("Shutting down RPC server.")
			grpcServer.Stop()
			return nil
		default:
			if !started {
				go func() {
					err = grpcServer.Serve(lis)
				}()
				started = true
			}
		}
	}
	return err
}
