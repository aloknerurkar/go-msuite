package config_service

import (
	"errors"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"gitlab.com/go-msuite/common/objects"
	"gitlab.com/go-msuite/configurator/config_service/grpc_gateway"
	"gitlab.com/go-msuite/configurator/config_service/grpc_server"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"testing"
	"time"
)

var testConf = &Configurator.Configurations{
	SvcName: "TestService",
	Version: "1.0.0",
	ConfigValues: []*Configurator.Config{
		{
			ConfType: Configurator.ConfigType_GRPC_SERVER_CONFIG,
			Type: &Configurator.Config_Server{
				Server: &Configurator.GrpcServerConfig{
					SvcName: "TestRpc",
					Port:    1000,
				},
			},
		},
		{
			ConfType: Configurator.ConfigType_PROXY_ENDPOINT_CONFIG,
			Type: &Configurator.Config_Ep{
				Ep: &Configurator.ProxyEndpointConfig{
					SvcName: "TestRpc",
					Port:    1001,
				},
			},
		},
	},
}

var rpcOnlyConf = &Configurator.Configurations{
	SvcName: "TestService",
	Version: "1.0.0",
	ConfigValues: []*Configurator.Config{
		{
			ConfType: Configurator.ConfigType_GRPC_SERVER_CONFIG,
			Type: &Configurator.Config_Server{
				Server: &Configurator.GrpcServerConfig{
					SvcName: "TestRpc",
					Port:    1000,
				},
			},
		},
	},
}

var rpcErrInitFn grpc_server.RPCInitFn = func(*objects.GrpcServerConf, *grpc.Server) error {
	return errors.New("Dummy Error")
}

var rpcInitFn grpc_server.RPCInitFn = func(*objects.GrpcServerConf, *grpc.Server) error {
	return nil
}

var httpErrInitFn grpc_gateway.HTTPInitFn = func(context.Context, *runtime.ServeMux, *objects.ProxyConf, []grpc.DialOption) error {
	return errors.New("Dummy Error")
}

var httpInitFn grpc_gateway.HTTPInitFn = func(context.Context, *runtime.ServeMux, *objects.ProxyConf, []grpc.DialOption) error {
	return nil
}

func TestService_Start(t *testing.T) {
	svc := &service{
		conf:     rpcOnlyConf,
		init_fns: []interface{}{rpcErrInitFn},
	}
	err := svc.Start(context.Background())
	if err == nil {
		t.Fatal("Expected error on start")
	}

	svc = &service{
		conf:     testConf,
		init_fns: []interface{}{rpcInitFn},
	}
	err = svc.Start(context.Background())
	if err == nil {
		t.Fatal("Expected error on start")
	}

	svc = &service{
		conf:     testConf,
		init_fns: []interface{}{rpcInitFn, httpErrInitFn},
	}
	err = svc.Start(context.Background())
	if err == nil {
		t.Fatal("Expected error on start")
	}

	testConf.ConfigValues[1].GetEp().Port = 1000
	svc.init_fns = []interface{}{rpcInitFn, httpInitFn}
	err = svc.Start(context.Background())
	if err == nil {
		t.Fatal("Expected error on start")
	}

	testConf.ConfigValues[1].GetEp().Port = 1001
	err = nil
	start := time.Now()
	ctx, cancel := context.WithCancel(context.Background())
	stopped := false
	go func(st *bool) {
		err = svc.Start(ctx)
		if err != nil {
			t.Fatal("Start returned unexpected error")
		}
		*st = true
	}(&stopped)

	for {
		if time.Since(start) < time.Second*10 {
			if stopped {
				t.Fatal("Service returned unexpected error.")
			}
		} else {
			cancel()
			break
		}
	}

	time.Sleep(time.Second)
	if !stopped {
		t.Fatal("Service should have been stopped.")
	}
	if err != nil {
		t.Fatal("Service should have not returned error.")
	}
}
