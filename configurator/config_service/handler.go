package config_service

import (
	"errors"
	"gitlab.com/go-msuite/common/go-common"
	"gitlab.com/go-msuite/common/objects"
	"gitlab.com/go-msuite/configurator/config_service/grpc_gateway"
	"gitlab.com/go-msuite/configurator/config_service/grpc_server"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	"golang.org/x/net/context"
	"log"
	"os"
	"sync"
	"time"
)

var LOG *log.Logger = log.New(os.Stdout, "Service Main:", log.Ldate|log.Ltime|log.Lshortfile)

type Service interface {
	GetArgs() *Configurator.Request
	GetConfigLastUpdated() int64
	UpdateConfig(c *Configurator.Configurations)
	Start(ctx context.Context) error
}

type service struct {
	args     *Configurator.Request
	conf_mtx sync.Mutex
	conf     *Configurator.Configurations
	init_fns []interface{}
}

func NewService(conf *Configurator.Request, inits ...interface{}) Service {
	return &service{
		args:     conf,
		init_fns: inits,
	}
}

func (s *service) GetArgs() *Configurator.Request {
	return s.args
}

func (s *service) GetConfigLastUpdated() int64 {
	s.conf_mtx.Lock()
	defer s.conf_mtx.Unlock()
	return s.conf.Updated
}

func (s *service) UpdateConfig(c *Configurator.Configurations) {
	s.conf_mtx.Lock()
	defer s.conf_mtx.Unlock()
	s.conf = c
}

func (s *service) Start(ctx context.Context) (ret_err error) {

	wg := &sync.WaitGroup{}

	errc := make(chan error, 2)
	defer close(errc)

	cctx, cancel := context.WithCancel(ctx)
	go func() {
		<-ctx.Done()
		LOG.Printf("Parent requested stop.")
		cancel()
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case ret_err = <-errc:
				LOG.Printf("One of the services returned error %v. Quitting.", ret_err)
				cancel()
				return
			case <-cctx.Done():
				LOG.Println("Stop requested. Returning.")
				return
			default:
				time.Sleep(time.Millisecond * 5000)
			}
		}
	}()

	// Map to make sure same port is not used. Currently multiple services
	// are allowed to run on independent ports.
	started := make(map[int32]bool)
	for idx := range s.conf.ConfigValues {
		switch s.conf.ConfigValues[idx].ConfType {
		case Configurator.ConfigType_GRPC_SERVER_CONFIG:
			server_conf := &objects.GrpcServerConf{GrpcServerConfig: s.conf.ConfigValues[idx].GetServer()}

			if server_conf.SvcName == go_common.CONFIGURATOR_SVC {
				if val, ok := ctx.Value("docker").(bool); ok {
					LOG.Println("Docker mode enabled on Configurator.")
					server_conf.SetDockerMode(val)
				}
			}

			if already_started, ok := started[server_conf.GetPort()]; ok {
				if already_started {
					LOG.Println("Not allowed to start service on same port")
					errc <- errors.New("Invalid port no.")
				}
			} else {
				started[server_conf.GetPort()] = true
			}

			var initFn grpc_server.RPCInitFn

			for i := range s.init_fns {
				if fn, ok := s.init_fns[i].(grpc_server.RPCInitFn); ok {
					LOG.Println("Found INIT fn")
					initFn = fn
				}
			}

			if initFn != nil {
				wg.Add(1)
				go func(conf *objects.GrpcServerConf) {
					defer wg.Done()
					rpc_srv := grpc_server.NewGrpcService(conf)
					err := rpc_srv.Start(cctx, initFn)
					if err != nil {
						LOG.Println("RPC service returned ERR:" + err.Error())
						errc <- err
					}
				}(server_conf)
			} else {
				errc <- errors.New("GRPC Init function not provided.")
			}

		case Configurator.ConfigType_PROXY_ENDPOINT_CONFIG:
			proxy_conf := &objects.ProxyConf{ProxyEndpointConfig: s.conf.ConfigValues[idx].GetEp()}

			if already_started, ok := started[proxy_conf.GetPort()]; ok {
				if already_started {
					LOG.Println("Not allowed to start service on same port")
					errc <- errors.New("Invalid port no.")
				}
			} else {
				started[proxy_conf.GetPort()] = true
			}

			var initFn grpc_gateway.HTTPInitFn

			for i := range s.init_fns {
				if fn, ok := s.init_fns[i].(grpc_gateway.HTTPInitFn); ok {
					initFn = fn
				}
			}

			if initFn != nil {
				wg.Add(1)
				go func(conf *objects.ProxyConf) {
					defer wg.Done()
					gwSvc := grpc_gateway.NewGatewayService(conf)
					err := gwSvc.Start(cctx, initFn)
					if err != nil {
						LOG.Println("HTTP service returned ERR:" + err.Error())
						errc <- err
					}
				}(proxy_conf)
			} else {
				errc <- errors.New("GRPC Gateway Init function not provided.")
			}
		}
	}

	wg.Wait()
	LOG.Println("Services stopped. Returning.")
	return
}
