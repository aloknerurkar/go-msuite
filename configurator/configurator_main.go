package main

import (
	"flag"
	"gitlab.com/go-msuite/common/objects"
	"gitlab.com/go-msuite/configurator/config_service"
	"gitlab.com/go-msuite/configurator/rpc-service"
	"golang.org/x/net/context"
	"log"
	"time"
)

var docker = flag.Bool("docker", false, "Docker mode")

func main() {
	flag.Parse()

	svc := config_service.NewService(nil, rpc_service.Init)

	if *docker {
		srv := &objects.GrpcServerConf{GrpcServerConfig: rpc_service.ConfiguratorDefaultConf.ConfigValues[0].GetServer()}
		srv.Port = 10000
		srv.GetDBConfig("redis").Hostname = "redis"
		srv.GetLockerConfig("zookeeper").Hostname = "zookeeper"
	}
	svc.UpdateConfig(rpc_service.ConfiguratorDefaultConf)
retry:
	ctx, cancel := context.WithCancel(context.Background())
	if *docker {
		ctx = context.WithValue(ctx, "docker", true)
	}
	err := svc.Start(ctx)
	if err != nil {
		log.Printf("Configurator service stopped. Result:%s\n", err.Error())
		cancel()
	}
	time.Sleep(time.Second * 15)
	log.Println("Trying to restart Configurator Service.")
	goto retry
}
