package client

import (
	"errors"
	"github.com/stretchr/testify/require"
	"gitlab.com/go-msuite/common/objects/utils"
	Messages "gitlab.com/go-msuite/common/pb"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"os"
	"os/signal"
	"testing"
	"time"
)

func assertPanic(t *testing.T, f func()) {
	defer func() {
		if r := recover(); r == nil {
			t.Fatal("The code did not panic")
		} else {
			t.Logf("Got panic %v", r)
		}
	}()
	f()
}

func TestMain(m *testing.M) {
	os.Setenv("CLIENT_TEST", "true")
	os.Exit(m.Run())
}

func TestStateMachine_TransitionState(t *testing.T) {

	s := &StateMachine{}

	success_transitions := []State{RUNNING, STOPPED, RUNNING, UPDATING, RUNNING, STOPPED}
	for _, st := range success_transitions {
		s.TransitionState(st)
		if s.GetState() != st {
			t.Fatalf("Expected state %v", st)
		}
	}

	s.TransitionState(RUNNING)
	// From RUNNING
	assertPanic(t, func() { s.TransitionState(INITIALIZED) })

	s.TransitionState(UPDATING)
	// From UPDATING
	assertPanic(t, func() { s.TransitionState(INITIALIZED) })

	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc)

	s.TransitionState(FATAL)
	start := time.Now()
	for {
		stop := false
		select {
		case s := <-sigc:
			t.Logf("Got signal: %v", s)
			stop = true
		}
		if stop {
			break
		}
		if time.Since(start) > time.Second*5 {
			t.Fatal("Waited 5 secs for signal.")
		}
	}

	// From FATAL
	assertPanic(t, func() { s.TransitionState(RUNNING) })
	assertPanic(t, func() { s.TransitionState(STOPPED) })
	assertPanic(t, func() { s.TransitionState(INITIALIZED) })
	assertPanic(t, func() { s.TransitionState(UPDATING) })
}

type testSvc struct {
	last_updated int64
	ret_err      error
	testSvcChan  chan error
}

func (t *testSvc) GetArgs() *Configurator.Request { return &Configurator.Request{} }

func (t *testSvc) GetConfigLastUpdated() int64 { return t.last_updated }

func (t *testSvc) UpdateConfig(c *Configurator.Configurations) {
	t.last_updated = c.Updated
}

func (t *testSvc) Start(ctx context.Context) error {
	for {
		stop := false
		select {
		case e := <-t.testSvcChan:
			log.Printf("Got error %s", e.Error())
			t.ret_err = e
			stop = true
		case sig := <-ctx.Done():
			log.Printf("Got signal %v", sig)
			stop = true
		}
		if stop {
			break
		}
		time.Sleep(time.Second)
	}
	return t.ret_err
}

type testClient struct {
	updated int64
	ret_err error
}

func (c *testClient) GetConfigurations(ctx context.Context, in *Configurator.Request, opts ...grpc.CallOption) (*Configurator.Configurations, error) {
	return &Configurator.Configurations{Updated: c.updated}, c.ret_err
}

func (c *testClient) GetServices(ctx context.Context, in *Messages.EmptyMessage, opts ...grpc.CallOption) (*Configurator.ConfigurationsList, error) {
	return &Configurator.ConfigurationsList{}, c.ret_err
}

func (c *testClient) UpdateConfig(ctx context.Context, in *Configurator.Config, opts ...grpc.CallOption) (*Configurator.Config, error) {
	return &Configurator.Config{}, c.ret_err
}

type testConfSvcGetter struct {
	*testClient
	ret_err error
}

func (t *testConfSvcGetter) InitDone() bool {
	return true
}

func (t *testConfSvcGetter) GetConfigurator() (Configurator.ConfiguratorClient, utils.ConnDone, error) {
	if t.testClient != nil {
		return t.testClient, func() {}, nil
	}
	return &testClient{}, func() {}, t.ret_err
}

func TestConfigClient_StartStopBasic(t *testing.T) {

	client := &ConfigClient{
		sm:              &StateMachine{},
		svc:             &testSvc{last_updated: time.Now().Unix()},
		static_mode:     false,
		restart_on_fail: false,
		confSvc:         &testConfSvcGetter{},
	}

	client.Start()
	require.Condition(t, func() bool {
		if client.started != true {
			return false
		}
		time.Sleep(time.Second * 2)
		if client.sm.state != RUNNING {
			return false
		}
		return true
	}, "Client state incorrect after start %v", client)

	err := client.Stop()
	time.Sleep(time.Second * 3)
	require.Condition(t, func() bool {
		if err != nil {
			t.Error("Error returned")
			return false
		}
		time.Sleep(time.Second * 15)
		if client.started != false {
			t.Error("Client still 'started'")
			return false
		}
		if client.sm.state != STOPPED {
			t.Error("State is not stopped")
			return false
		}
		return true
	}, "Client state incorrect after stop %v", client)

	client.Start()
	require.Condition(t, func() bool {
		if client.started != true {
			return false
		}
		time.Sleep(time.Second * 2)
		if client.sm.state != RUNNING {
			return false
		}
		return true
	}, "Client state incorrect after start %v", client)
}

func TestConfigClient_StartKeepAlive(t *testing.T) {

	client := &ConfigClient{
		sm:              &StateMachine{},
		svc:             &testSvc{last_updated: time.Now().Unix()},
		static_mode:     false,
		restart_on_fail: false,
		confSvc:         &testConfSvcGetter{ret_err: errors.New("Dummy error")},
	}

	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc)

	client.Start()
	require.Condition(t, func() bool {
		if client.started != true {
			return false
		}
		time.Sleep(time.Second * 5)
		if client.sm.state != INITIALIZED {
			return false
		}
		time.Sleep(time.Second * 5)
		if client.sm.state != INITIALIZED {
			return false
		}
		return true
	}, "Client state incorrect after start %v", client)

	start := time.Now()
	for {
		stop := false
		select {
		case s := <-sigc:
			t.Logf("Got signal: %v", s)
			stop = true
		}
		if stop {
			break
		}
		if time.Since(start) > time.Second*10 {
			t.Fatal("Waited 10 secs for signal.")
		}
	}

	require.Condition(t, func() bool {
		time.Sleep(time.Second * 3)
		if client.started != false {
			return false
		}
		if client.sm.state != FATAL {
			return false
		}
		return true
	}, "Client state incorrect after fatal %v", client)

	client = &ConfigClient{
		sm:              &StateMachine{},
		svc:             &testSvc{last_updated: time.Now().Unix()},
		static_mode:     false,
		restart_on_fail: false,
		confSvc:         &testConfSvcGetter{testClient: &testClient{ret_err: errors.New("Dummy error")}},
	}

	client.Start()
	require.Condition(t, func() bool {
		if client.started != true {
			return false
		}
		time.Sleep(time.Second * 5)
		if client.sm.state != INITIALIZED {
			return false
		}
		time.Sleep(time.Second * 5)
		if client.sm.state != INITIALIZED {
			return false
		}
		return true
	}, "Client state incorrect after start %v", client)

	start = time.Now()
	for {
		stop := false
		select {
		case s := <-sigc:
			t.Logf("Got signal: %v", s)
			stop = true
		}
		if stop {
			break
		}
		if time.Since(start) > time.Second*10 {
			t.Fatal("Waited 10 secs for signal.")
		}
	}

	require.Condition(t, func() bool {
		if client.started != false {
			return false
		}
		if client.sm.state != FATAL {
			return false
		}
		return true
	}, "Client state incorrect after fatal %v", client)

}

func TestConfigClient_StartUpdate(t *testing.T) {

	timestamp := time.Now().Unix()
	tc := &testClient{updated: timestamp}

	client := &ConfigClient{
		sm:              &StateMachine{},
		svc:             &testSvc{last_updated: timestamp},
		static_mode:     false,
		restart_on_fail: false,
		confSvc:         &testConfSvcGetter{testClient: tc},
	}

	client.Start()
	require.Condition(t, func() bool {
		if client.started != true {
			return false
		}
		time.Sleep(time.Second * 2)
		if client.sm.state != RUNNING {
			return false
		}
		return true
	}, "Client state incorrect after start %v", client)

	start := time.Now()
	for {
		if client.sm.state != RUNNING {
			t.Fatal("Client expected to be running...")
		}
		time.Sleep(time.Millisecond * 500)
		if time.Since(start) > time.Second*5 {
			break
		}
	}

	// Update config
	tc.updated = time.Now().Unix()
	service_stopped := false
	rcvr_stopped := false
	start = time.Now()
	for {
		if client.sm.state == STOPPED {
			service_stopped = true
		}
		if client.started == false {
			rcvr_stopped = true
		}
		if client.sm.state == UPDATING {
			if !service_stopped || !rcvr_stopped {
				t.Fatal("Service was not stopped before updating")
			}
			break
		}
		time.Sleep(time.Millisecond * 100)
		if time.Since(start) > time.Second*10 {
			t.Fatal("Took too long to update.")
		}
	}

	time.Sleep(time.Second * 2)
	start = time.Now()
	for {
		if client.sm.state != RUNNING {
			t.Fatal("Client expected to be running...")
		}
		time.Sleep(time.Millisecond * 500)
		if time.Since(start) > time.Second*5 {
			break
		}
	}
}

func TestConfigClient_StartErr(t *testing.T) {

	ts := &testSvc{
		testSvcChan: make(chan error, 1),
	}

	client := &ConfigClient{
		sm:              &StateMachine{},
		svc:             ts,
		static_mode:     false,
		restart_on_fail: false,
		confSvc:         &testConfSvcGetter{},
	}

	client.Start()
	require.Condition(t, func() bool {
		if client.started != true {
			return false
		}
		time.Sleep(time.Second * 2)
		if client.sm.state != RUNNING {
			return false
		}
		time.Sleep(time.Second * 2)
		if client.sm.state != RUNNING {
			return false
		}
		time.Sleep(time.Second * 2)
		if client.sm.state != RUNNING {
			return false
		}
		return true
	}, "Client state incorrect after start %v", client)

	ts.testSvcChan <- errors.New("Dummy error")

	start := time.Now()
	service_stopped := false
	for {
		if client.sm.state == STOPPED {
			service_stopped = true
		}
		if client.started == false {
			if !service_stopped {
				t.Fatal("Service was not stopped before stopping receiver")
			}
			break
		}
		if time.Since(start) > time.Second*20 {
			t.Fatal("Took too long to update.")
		}
	}

	client.Start()
	require.Condition(t, func() bool {
		if client.started != true {
			return false
		}
		time.Sleep(time.Second * 2)
		if client.sm.state != RUNNING {
			return false
		}
		time.Sleep(time.Second * 2)
		if client.sm.state != RUNNING {
			return false
		}
		time.Sleep(time.Second * 2)
		if client.sm.state != RUNNING {
			return false
		}
		return true
	}, "Client state incorrect after start %v", client)
}

func TestConfigClient_StartStaticRestartOnFail(t *testing.T) {

	tc := &testClient{}
	ts := &testSvc{
		testSvcChan: make(chan error, 1),
	}

	client := &ConfigClient{
		sm:              &StateMachine{},
		svc:             ts,
		static_mode:     true,
		restart_on_fail: false,
		confSvc:         &testConfSvcGetter{testClient: tc},
	}

	client.Start()
	require.Condition(t, func() bool {
		if client.started != true {
			return false
		}
		time.Sleep(time.Second * 2)
		if client.sm.state != RUNNING {
			return false
		}
		time.Sleep(time.Second * 2)
		if client.sm.state != RUNNING {
			return false
		}
		return true
	}, "Client state incorrect after start %v", client)

	tc.updated = time.Now().Unix()
	service_stopped := false
	rcvr_stopped := false
	updated := false
	start := time.Now()
	for {
		if client.sm.state == STOPPED {
			service_stopped = true
		}
		if client.started == false {
			rcvr_stopped = true
		}
		if client.sm.state == UPDATING {
			updated = true
		}
		time.Sleep(time.Millisecond * 100)
		if time.Since(start) > time.Second*10 {
			break
		}
	}

	if rcvr_stopped || service_stopped || updated {
		t.Fatal("Service should not have updated in static mode.")
	}

	// Turn off static mode
	client.static_mode = false

	// Make sure the service updates first as we turned static mode off
	service_stopped = false
	rcvr_stopped = false
	start = time.Now()
	for {
		if client.sm.state == STOPPED {
			service_stopped = true
		}
		if client.started == false {
			rcvr_stopped = true
		}
		if client.sm.state == UPDATING {
			if !service_stopped || !rcvr_stopped {
				t.Fatal("Service was not stopped before updating")
			}
			break
		}
		time.Sleep(time.Millisecond * 100)
		if time.Since(start) > time.Second*10 {
			t.Fatal("Took too long to update.")
		}
	}

	client.restart_on_fail = true

	ts.testSvcChan <- errors.New("Dummy error")

	start = time.Now()
	service_stopped = false
	for {
		if client.sm.state == STOPPED {
			service_stopped = true
		}
		time.Sleep(time.Millisecond * 20)
		if client.sm.state == RUNNING {
			if !service_stopped {
				t.Fatal("Service was not stopped before starting again.")
			}
			break
		}
		if time.Since(start) > time.Second*15 {
			t.Fatal("Took too long to restart.")
		}
	}
}
