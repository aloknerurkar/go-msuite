package client

import (
	"errors"
	"gitlab.com/go-msuite/common/objects/utils"
	Messages "gitlab.com/go-msuite/common/pb"
	"gitlab.com/go-msuite/configurator/config_service"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"os"
	"sync"
	"syscall"
	"time"
)

type State int32

const (
	INITIALIZED State = 0
	RUNNING     State = 1
	STOPPED     State = 2
	UPDATING    State = 3
	FATAL       State = 4
)

type StateMachine struct {
	mtx   sync.Mutex
	state State
}

func (s *StateMachine) TransitionState(new_st State) {
	s.mtx.Lock()
	defer s.mtx.Unlock()

	if s.state == FATAL {
		panic("Transition called after client stopped fatally.")
	}

	if (s.state != INITIALIZED && s.state != STOPPED) && new_st == INITIALIZED {
		panic("Illegal state transition.")
	}

	s.state = new_st
	if new_st == FATAL {
		log.Printf("Sending Interrupt Curr PID:%d Parent PID:%d", syscall.Getpid(), syscall.Getppid())
		err := syscall.Kill(syscall.Getpid(), syscall.SIGINT)
		if err != nil {
			log.Printf("Hit error while sending interrupt: %s", err.Error())
		}
	}
}

func (s *StateMachine) GetState() State {
	s.mtx.Lock()
	defer s.mtx.Unlock()
	return s.state
}

type Interval string

const (
	CONFIG_LAG  Interval = "CONFIG_LAG"
	STOP_WAIT_1 Interval = "STOP_WAIT_1"
	STOP_WAIT_2 Interval = "STOP_WAIT_2"
	RCVR_WAIT   Interval = "RCVR_WAIT"
	KEEP_ALIVE  Interval = "KEEP_ALIVE"
	CANCEL_WAIT Interval = "CANCEL_WAIT"
)

func waitInterval(waitType Interval) time.Duration {
	testIntervals := false
	if os.Getenv("CLIENT_TEST") == "true" {
		testIntervals = true
	}
	switch waitType {
	case CONFIG_LAG:
		if testIntervals {
			return time.Second * 1
		} else {
			return time.Second * 10
		}
	case STOP_WAIT_1:
		if testIntervals {
			return time.Second * 3
		} else {
			return time.Second * 15
		}
	case STOP_WAIT_2:
		if testIntervals {
			return time.Second * 1
		} else {
			return time.Second * 3
		}
	case RCVR_WAIT:
		if testIntervals {
			return time.Second * 3
		} else {
			return time.Minute * 15
		}
	case KEEP_ALIVE:
		if testIntervals {
			return time.Second * 15
		} else {
			return time.Hour * 1
		}
	case CANCEL_WAIT:
		if testIntervals {
			return time.Second * 1
		} else {
			return time.Second * 15
		}
	}
	panic("Unknown Interval type.")
}

func NewConfigClient(addr string, s config_service.Service) *ConfigClient {
	return &ConfigClient{
		server_addr: addr,
		sm:          &StateMachine{},
		svc:         s,
		static_mode: true,
		confSvc:     &confSvcGetter{},
	}
}

type ConfigClient struct {
	server_addr     string
	static_mode     bool
	restart_on_fail bool
	svc             config_service.Service
	ctx             context.Context
	mtx             sync.Mutex
	cancel          context.CancelFunc
	sm              *StateMachine
	confSvc         ConfiguratorSvc
	err             error
	started         bool
}

type ConfiguratorSvc interface {
	InitDone() bool
	GetConfigurator() (Configurator.ConfiguratorClient, utils.ConnDone, error)
}

type confSvcGetter struct {
	*utils.Pool
}

func (c *confSvcGetter) InitDone() bool {
	if c.Pool != nil {
		return true
	}
	return false
}

func (c *confSvcGetter) GetConfigurator() (Configurator.ConfiguratorClient, utils.ConnDone, error) {
	conn, done, err := c.GetConn()
	return Configurator.NewConfiguratorClient(conn), done, err
}

func startReceiver(cli *ConfigClient) {
	log.Println("Starting receiver thread.")
	startFn := func(cli_ *ConfigClient) {
		// Reset the error
		cli_.err = nil
		cli_.sm.TransitionState(RUNNING)
		log.Println("Starting service.")
		err := cli_.svc.Start(cli_.ctx)
		if err != nil {
			cli_.err = err
		}
		cli_.sm.TransitionState(STOPPED)
		log.Println("Service start routine stopped.")
	}
	go func(c *ConfigClient) {
		// This will be used to keep track of last successful configurator call. If the configurator is down
		// for more than 1 hour we will issue FATAL stop. This will try to kill the caller process.
		configurator_epoch := time.Now()
		for {
			switch c.sm.GetState() {
			case INITIALIZED:
				// Create connection pool if it doesnt already exist.
				if !c.confSvc.InitDone() {
					pool := &utils.Pool{
						DialFn: func(addr string) (*grpc.ClientConn, error) {
							return grpc.Dial(addr, grpc.WithInsecure())
						},
						HeartBeatFn: func(conn *grpc.ClientConn) error {
							_, err := Messages.NewHeartBeatClient(conn).GetHeartBeat(
								context.Background(), &Messages.EmptyMessage{})
							return err
						},
						NoOfConnections: 5,
						ServerAddr:      c.server_addr,
					}
					c.confSvc = &confSvcGetter{Pool: pool}
				}
				fallthrough
			case RUNNING:
				if c.static_mode && c.sm.GetState() == RUNNING {
					log.Println("Static mode. Skipping check for updated config")
					break
				}
				// Query for configs
				configurator, done, err := c.confSvc.GetConfigurator()
				if err != nil {
					c.err = err
					log.Printf("Failed getting connection Err:%s Args:%v", err.Error(),
						c.svc.GetArgs())
					// If we are unable to get config for more than an hour, we will issue FATAL stop.
					if time.Since(configurator_epoch) > waitInterval(KEEP_ALIVE) {
						_ = utils.WithRetry(func() error {
							return c.Stop()
						}, 2)
						c.sm.TransitionState(FATAL)
						return
					}
					// Wait for 15 mins and try again.
					break
				}

				res, err := configurator.GetConfigurations(context.Background(), c.svc.GetArgs())
				// Done with conn
				done()
				if err != nil {
					c.err = err
					log.Printf("Failed getting configurations Err:%s Args:%v", err.Error(),
						c.svc.GetArgs())
					// A heartbeat will have been sent at the time connection was obtained. If
					// we fail here, it means probably some resource contention at the server.
					// So we will just retry later and introduce a lag to avoid similar timed
					// requests.
					time.Sleep(waitInterval(CONFIG_LAG))
					// If we are unable to get config for more than an hour, we will issue FATAL stop.
					if time.Since(configurator_epoch) > waitInterval(KEEP_ALIVE) {
						_ = utils.WithRetry(func() error {
							return c.Stop()
						}, 2)
						c.sm.TransitionState(FATAL)
						return
					}
					// Dont sleep for 15 mins. Go back and retry getting Configs. Theory is server
					// might fail if lot of services are requesting for same configuration due to
					// locking. So the lag introduced will eventually space out the requests
					// enough.
					continue
				}

				configurator_epoch = time.Now()

				if c.sm.GetState() == INITIALIZED {
					c.svc.UpdateConfig(res)
					go startFn(c)
				} else {
					if c.svc.GetConfigLastUpdated() < res.Updated {
						log.Printf("Config updated: OldTS:%d NewTS:%d", c.svc.GetConfigLastUpdated(),
							res.Updated)
						err := utils.WithRetry(func() error {
							return c.Stop()
						}, 2)
						if err != nil {
							log.Println("Could not stop service while updating.")
							c.err = err
							c.sm.TransitionState(FATAL)
							return
						}
						c.sm.TransitionState(UPDATING)
						// Make sure client startup routine is stopped.
						time.Sleep(waitInterval(CANCEL_WAIT))
						c.svc.UpdateConfig(res)
						go c.Start()
						log.Println("Closing receiver thread. New one will be started.")
						return
					}
				}
			case UPDATING:
				log.Println("Updated service configuration.")
				go startFn(c)
			case STOPPED:
				log.Println("Service stopped...")
				if c.err != nil {
					log.Printf("Client hit error: %s", c.err.Error())
					if c.restart_on_fail {
						go startFn(c)
						log.Println("Restarting service...")
						break
					}
					err := utils.WithRetry(func() error {
						return c.Stop()
					}, 2)
					if err != nil {
						log.Println("Could not stop service after error.")
						c.err = err
						c.sm.TransitionState(FATAL)
						return
					}
				}
				return
			case FATAL:
				panic("Receiver should have been stopped. Came here after FATAL error.")
			}
			time.Sleep(waitInterval(RCVR_WAIT))
		}
	}(cli)
}

func (c *ConfigClient) Start() {

	wg := sync.WaitGroup{}

	c.mtx.Lock()
	defer c.mtx.Unlock()

	if c.started {
		return
	}

	// Restart
	if c.sm.GetState() == STOPPED {
		c.sm.TransitionState(INITIALIZED)
	}

	c.ctx, c.cancel = context.WithCancel(context.Background())
	wg.Add(1)
	go func(c *ConfigClient, _wg *sync.WaitGroup) {
		for {
			select {
			case <-c.ctx.Done():
				log.Printf("Received stop signal %v", c.svc.GetArgs())
				// cancel() is protected by lock. So this can only be called
				// from locked state.
				c.started = false
				return
			default:
				if !c.started {
					log.Println("Starting receiver.")
					startReceiver(c)
					c.started = true
					// Here we dont stop the current routine. This
					// is just to signal the caller routine to return.
					// We needed the lock upto this point.
					_wg.Done()
				}
				time.Sleep(waitInterval(CANCEL_WAIT))
			}
		}
	}(c, &wg)
	wg.Wait()
	log.Println("Receiver started")
	return
}

func (c *ConfigClient) Stop() error {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	if !c.started {
		log.Println("Receiver is not running.")
		return nil
	}

	c.cancel()
	start := time.Now()
	for {
		if c.sm.GetState() == RUNNING {
			if time.Since(start) > waitInterval(STOP_WAIT_1) {
				log.Println("Timeout hit for stopping service.")
				break
			}
			log.Println("Waiting for service to be stopped...")
			time.Sleep(waitInterval(STOP_WAIT_2))
		} else {
			log.Println("Stop service complete.")
			c.started = false
			return nil
		}
	}
	return errors.New("Failed to stop client routine.")
}

func (c *ConfigClient) Status() State {
	return c.sm.GetState()
}
