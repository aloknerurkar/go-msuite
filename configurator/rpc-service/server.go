package rpc_service

import (
	lg "github.com/aloknerurkar/log-post/client"
	"github.com/pkg/errors"
	"gitlab.com/go-msuite/app-errors"
	"gitlab.com/go-msuite/common/go-common"
	"gitlab.com/go-msuite/common/objects"
	Messages "gitlab.com/go-msuite/common/pb"
	"gitlab.com/go-msuite/configurator/config_service/grpc_server"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	"gitlab.com/go-msuite/locker"
	Notifications "gitlab.com/go-msuite/notifications/pb"
	"gitlab.com/go-msuite/store"
	"gitlab.com/go-msuite/store/item"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"sync/atomic"
)

var (
	req_id int64 = 1
)

const (
	CONF_KEY_PFX  = "config"
	CONFS_KEY_PFX = "configs"
)

type configurator struct {
	db   store.Store
	lckr locker.Locker
	logr *lg.LogPostClient
	conf *objects.GrpcServerConf
}

type configObj struct {
	*Configurator.Config
}

func (u *configObj) GetNamespace() string {
	return go_common.CONFIGURATOR_SVC + "_" + CONF_KEY_PFX
}

func getConfigObj(u Configurator.ConfigType, v string) *configObj {
	switch u {
	case Configurator.ConfigType_GRPC_SERVER_CONFIG:
		return &configObj{
			Config: &Configurator.Config{
				ConfType: u, Type: &Configurator.Config_Server{
					Server: &Configurator.GrpcServerConfig{
						SvcName: v,
					},
				},
			},
		}
	case Configurator.ConfigType_GRPC_CLIENT_CONFIG:
		return &configObj{
			Config: &Configurator.Config{
				ConfType: u, Type: &Configurator.Config_RpcClient{
					RpcClient: &Configurator.GrpcClientConfig{
						SvcName: v,
					},
				},
			},
		}
	case Configurator.ConfigType_PROXY_ENDPOINT_CONFIG:
		return &configObj{
			Config: &Configurator.Config{
				ConfType: u, Type: &Configurator.Config_Ep{
					Ep: &Configurator.ProxyEndpointConfig{
						SvcName: v,
					},
				},
			},
		}
	case Configurator.ConfigType_LOCKER_CONFIG:
		return &configObj{
			Config: &Configurator.Config{
				ConfType: u, Type: &Configurator.Config_Locker{
					Locker: &Configurator.LockerConfig{
						Handler: v,
					},
				},
			},
		}
	case Configurator.ConfigType_DB_CONFIG:
		return &configObj{
			Config: &Configurator.Config{
				ConfType: u, Type: &Configurator.Config_Db{
					Db: &Configurator.DBConfig{
						Handler: v,
					},
				},
			},
		}
	case Configurator.ConfigType_PAYMENT_PROVIDER_CONFIG:
		return &configObj{
			Config: &Configurator.Config{
				ConfType: u, Type: &Configurator.Config_PaymentClient{
					PaymentClient: &Configurator.PaymentProviderConfig{
						Provider: v,
					},
				},
			},
		}
	case Configurator.ConfigType_CDN_HOST_CONFIG:
		return &configObj{
			Config: &Configurator.Config{
				ConfType: u,
			},
		}
	case Configurator.ConfigType_NOTIF_PROVIDER_CONFIG:
		return &configObj{
			Config: &Configurator.Config{
				ConfType: u, Type: &Configurator.Config_Notifications{
					Notifications: &Configurator.NotifProviderConfig{
						Type: Notifications.NotificationType(
							Notifications.NotificationType_value[v]),
					},
				},
			},
		}
	default:
		panic("Config type unidentified")
	}
}

func (u *configObj) GetId() string {
	var a, b string
	a = u.ConfType.String()
	switch u.ConfType {
	case Configurator.ConfigType_GRPC_SERVER_CONFIG:
		b = u.GetServer().GetSvcName()
	case Configurator.ConfigType_GRPC_CLIENT_CONFIG:
		b = u.GetRpcClient().GetSvcName()
	case Configurator.ConfigType_PROXY_ENDPOINT_CONFIG:
		b = u.GetEp().GetSvcName()
	case Configurator.ConfigType_LOCKER_CONFIG:
		b = u.GetLocker().GetHandler()
	case Configurator.ConfigType_DB_CONFIG:
		b = u.GetDb().GetHandler()
	case Configurator.ConfigType_PAYMENT_PROVIDER_CONFIG:
		b = u.GetPaymentClient().GetProvider()
	case Configurator.ConfigType_CDN_HOST_CONFIG:
		b = "cdn_host"
	case Configurator.ConfigType_NOTIF_PROVIDER_CONFIG:
		b = u.GetNotifications().GetType().String()
	default:
		panic("Config type unidentified")
	}
	return a + "_" + b
}

func (u *configObj) GetUpdated() int64 {
	switch u.ConfType {
	case Configurator.ConfigType_GRPC_SERVER_CONFIG:
		return u.GetServer().GetUpdated()
	case Configurator.ConfigType_GRPC_CLIENT_CONFIG:
		return u.GetRpcClient().GetUpdated()
	case Configurator.ConfigType_PROXY_ENDPOINT_CONFIG:
		return u.GetEp().GetUpdated()
	case Configurator.ConfigType_LOCKER_CONFIG:
		return u.GetLocker().GetUpdated()
	case Configurator.ConfigType_DB_CONFIG:
		return u.GetDb().GetUpdated()
	case Configurator.ConfigType_EMAILR_CLIENT_CONFIG:
		return u.GetMailClient().GetUpdated()
	case Configurator.ConfigType_PAYMENT_PROVIDER_CONFIG:
		return u.GetPaymentClient().GetUpdated()
	case Configurator.ConfigType_CDN_HOST_CONFIG:
		return u.GetCdnHost().GetUpdated()
	case Configurator.ConfigType_NOTIF_PROVIDER_CONFIG:
		return u.GetNotifications().GetUpdated()
	}
	panic("Config type unidentified")
	return 0
}

func (u *configObj) GetCreated() int64 {
	switch u.ConfType {
	case Configurator.ConfigType_GRPC_SERVER_CONFIG:
		return u.GetServer().GetCreated()
	case Configurator.ConfigType_GRPC_CLIENT_CONFIG:
		return u.GetRpcClient().GetCreated()
	case Configurator.ConfigType_PROXY_ENDPOINT_CONFIG:
		return u.GetEp().GetCreated()
	case Configurator.ConfigType_LOCKER_CONFIG:
		return u.GetLocker().GetCreated()
	case Configurator.ConfigType_DB_CONFIG:
		return u.GetDb().GetCreated()
	case Configurator.ConfigType_EMAILR_CLIENT_CONFIG:
		return u.GetMailClient().GetCreated()
	case Configurator.ConfigType_PAYMENT_PROVIDER_CONFIG:
		return u.GetPaymentClient().GetCreated()
	case Configurator.ConfigType_CDN_HOST_CONFIG:
		return u.GetCdnHost().GetCreated()
	case Configurator.ConfigType_NOTIF_PROVIDER_CONFIG:
		return u.GetNotifications().GetCreated()
	}
	panic("Config type unidentified")
	return 0
}

func (u *configObj) SetUpdated(ts int64) {
	switch u.ConfType {
	case Configurator.ConfigType_GRPC_SERVER_CONFIG:
		u.GetServer().Updated = ts
		return
	case Configurator.ConfigType_GRPC_CLIENT_CONFIG:
		u.GetRpcClient().Updated = ts
		return
	case Configurator.ConfigType_PROXY_ENDPOINT_CONFIG:
		u.GetEp().Updated = ts
		return
	case Configurator.ConfigType_LOCKER_CONFIG:
		u.GetLocker().Updated = ts
		return
	case Configurator.ConfigType_DB_CONFIG:
		u.GetDb().Updated = ts
		return
	case Configurator.ConfigType_EMAILR_CLIENT_CONFIG:
		u.GetMailClient().Updated = ts
		return
	case Configurator.ConfigType_PAYMENT_PROVIDER_CONFIG:
		u.GetPaymentClient().Updated = ts
		return
	case Configurator.ConfigType_CDN_HOST_CONFIG:
		u.GetCdnHost().Updated = ts
		return
	case Configurator.ConfigType_NOTIF_PROVIDER_CONFIG:
		u.GetNotifications().Updated = ts
		return
	}
	panic("Config type unidentified")
}

func (u *configObj) SetCreated(ts int64) {
	switch u.ConfType {
	case Configurator.ConfigType_GRPC_SERVER_CONFIG:
		u.GetServer().Created = ts
		return
	case Configurator.ConfigType_GRPC_CLIENT_CONFIG:
		u.GetRpcClient().Created = ts
		return
	case Configurator.ConfigType_PROXY_ENDPOINT_CONFIG:
		u.GetEp().Created = ts
		return
	case Configurator.ConfigType_LOCKER_CONFIG:
		u.GetLocker().Created = ts
		return
	case Configurator.ConfigType_DB_CONFIG:
		u.GetDb().Created = ts
		return
	case Configurator.ConfigType_EMAILR_CLIENT_CONFIG:
		u.GetMailClient().Created = ts
		return
	case Configurator.ConfigType_PAYMENT_PROVIDER_CONFIG:
		u.GetPaymentClient().Created = ts
		return
	case Configurator.ConfigType_CDN_HOST_CONFIG:
		u.GetCdnHost().Created = ts
		return
	case Configurator.ConfigType_NOTIF_PROVIDER_CONFIG:
		u.GetNotifications().Created = ts
		return
	}
	panic("Config type unidentified")
}

type configsObj struct {
	*Configurator.Configurations
}

func (u *configsObj) GetId() string { return u.SvcName + "_" + u.Version }

func (u *configsObj) GetNamespace() string { return go_common.CONFIGURATOR_SVC + "_" + CONFS_KEY_PFX }

func (l *configsObj) SetCreated(i int64) { l.Created = i }

func (l *configsObj) SetUpdated(i int64) { l.Updated = i }

type configsList []*configsObj

var InitFn grpc_server.RPCInitFn = Init

func Init(config *objects.GrpcServerConf, srv *grpc.Server) error {

	lgr := lg.InitLogger(config.SvcName, "", config.LogLevel, false)

	dbConf := config.GetDBConfig("redis")
	if dbConf == nil {
		return lgr.Error(errors.New("DB Config absent"), "Database config not provided")
	}

	dbp, err := store.NewStore(dbConf)
	if err != nil {
		return lgr.Error(err, "Failed initializing configurator db")
	}

	lockerConf := config.GetLockerConfig("zookeeper")
	if lockerConf == nil {
		return lgr.Error(errors.New("Locker Config absent"), "Locker config not provided")
	}

	lockr, err := locker.New(lockerConf)
	if err != nil {
		return lgr.Error(err, "Failed initializing configurator locker")
	}

	Configurator.RegisterConfiguratorServer(srv, &configurator{
		db:   dbp,
		lckr: lockr,
		logr: lgr,
		conf: config,
	})
	return nil
}

func (s *configurator) getConfigWithLock(v *Configurator.ConfigReq) (*configObj, error) {

	conf := getConfigObj(v.ConfType, v.Handler)

	unlock, err := s.lckr.TryLock(conf, locker.DefaultTimeout*2)
	if err != nil {
		err = s.logr.Error(app_errors.ErrInternal("Failed to get lock on config."),
			"Failed to lock configurations obj Err:%s LockObj:%v", err, conf)
		return nil, err
	}
	defer unlock()

	err = s.db.Read(conf)
	if err != nil {
		conf.Config = default_conf(v.ConfType, v.Handler, s.conf.GetDockerMode())
		if conf.Config == nil {
			err = s.logr.Error(app_errors.ErrInternal("Failed to get default config."),
				"Default config not present Err:%s Req:%v", err, v)
			return nil, err
		}
		err = s.db.Create(conf)
		if err != nil {
			err = s.logr.Error(app_errors.ErrInternal("Failed to store created config."),
				"Failed storing config in DB. Err:%s Conf:%v", err, conf)
			return nil, err
		}
		s.logr.Info("Stored new config %v", conf)
	} else {
		s.logr.Info("Got stored config %v", conf)
	}
	return conf, nil
}

// Currently we will just grab all the individual configurations along with their timestamps.
// The client will decide based on the latest timestamp whether he should update or not.
func (s *configurator) GetConfigurations(c context.Context,
	req *Configurator.Request) (ret_configs *Configurator.Configurations,
	ret_err error) {
	_req_id := atomic.AddInt64(&req_id, 1)
	s.logr.TraceStart("ReqID: %d", _req_id)
	defer s.logr.TraceEnd("ReqID: %d", _req_id)

	confs := &configsObj{
		Configurations: &Configurator.Configurations{
			SvcName: req.SvcName,
			Version: req.Version,
		},
	}

	unlock, err := s.lckr.TryLock(confs, locker.DefaultTimeout*2)
	if err != nil {
		ret_err = s.logr.Error(app_errors.ErrInternal("Failed to get lock on configurations."),
			"Failed to lock configurations obj Err:%s LockObj:%v", err, confs)
		return
	}
	defer unlock()

	new_entry := false
	err = s.db.Read(confs)
	if err != nil {
		new_entry = true
	}

	var last_update int64

	new_confs := new(Configurator.Configurations)
	new_confs.ConfigValues = make([]*Configurator.Config, len(req.Configs))
	for idx, v := range req.Configs {

		u_conf, err := s.getConfigWithLock(v)
		if err != nil {
			ret_err = err
			return
		}

		if u_conf.GetUpdated() > last_update {
			s.logr.Info("CONF TS:%d", u_conf.GetUpdated())
			last_update = u_conf.GetUpdated()
		}

		new_confs.ConfigValues[idx] = u_conf.Config
		if u_conf.ConfType == Configurator.ConfigType_GRPC_SERVER_CONFIG {
			srv_conf := u_conf.GetServer()
			srv_conf.SubConfs = make([]*Configurator.Config, len(v.SubConfs))
			for idx2, v2 := range v.SubConfs {

				sub_conf, err := s.getConfigWithLock(v2)
				if err != nil {
					ret_err = err
					return
				}
				srv_conf.SubConfs[idx2] = sub_conf.Config

				if sub_conf.GetUpdated() > last_update {
					last_update = sub_conf.GetUpdated()
					s.logr.Info("SUB CONF TS:%d", sub_conf.GetUpdated())
				}
			}
		}
	}

	if confs.Updated < last_update {
		confs.Configurations = new_confs
	}

	if new_entry {
		err = s.db.Create(confs)
	} else {
		err = s.db.Update(confs)
	}

	if err != nil {
		ret_err = s.logr.Error(app_errors.ErrInternal("Failed to store configurations."),
			"Failed storing config in DB. Err:%s Conf:%v", err, confs)
		return
	}

	ret_configs = confs.Configurations

	return
}

func (s *configurator) GetServices(c context.Context,
	emp *Messages.EmptyMessage) (ret_confs *Configurator.ConfigurationsList,
	ret_err error) {
	_req_id := atomic.AddInt64(&req_id, 1)
	s.logr.TraceStart("ReqID: %d", _req_id)
	defer s.logr.TraceEnd("ReqID: %d", _req_id)

	no_of_services := 20

	opt := item.ListOpt{
		Page:  0,
		Limit: int64(no_of_services),
	}

	confs := make([]item.Item, no_of_services)
	for i := range confs {
		confs[i] = new(configsObj)
	}

	results, err := s.db.List(confs, opt)
	if err != nil {
		ret_err = s.logr.Error(app_errors.ErrInternal("Failed to list configurations."),
			"Failed listing configs in DB. Err:%s", err)
		return
	}

	ret_confs = &Configurator.ConfigurationsList{Conf: make([]*Configurator.Configurations, results)}
	for i := 0; i < results; i++ {
		if c, ok := confs[i].(*configsObj); ok {
			ret_confs.Conf[i] = c.Configurations
		}
	}

	return
}

func (s *configurator) UpdateConfig(c context.Context,
	upd *Configurator.Config) (ret_conf *Configurator.Config, ret_err error) {
	_req_id := atomic.AddInt64(&req_id, 1)
	s.logr.TraceStart("ReqID: %d", _req_id)
	defer s.logr.TraceEnd("ReqID: %d", _req_id)

	conf := &configObj{Config: upd}

	unlock, err := s.lckr.TryLock(conf, locker.DefaultTimeout)
	if err != nil {
		err = s.logr.Error(app_errors.ErrInternal("Failed to get lock on config."),
			"Failed to lock configurations obj Err:%s LockObj:%v", err, conf)
		return nil, err
	}
	defer unlock()

	err = s.db.Update(conf)
	if err != nil {
		err = s.logr.Error(app_errors.ErrInternal("Failed to store config."),
			"Failed storing config in DB. Err:%s Conf:%v", err, upd)
		return nil, err
	}
	return
}
