package rpc_service

import (
	"gitlab.com/go-msuite/common/go-common"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	Notifications "gitlab.com/go-msuite/notifications/pb"
	"os"
	"strconv"
	"time"
)

var ConfiguratorDefaultConf = &Configurator.Configurations{
	SvcName: go_common.CONFIGURATOR_SVC,
	Version: "1.0.0",
	Updated: time.Now().Unix(),
	ConfigValues: []*Configurator.Config{
		{
			ConfType: Configurator.ConfigType_GRPC_SERVER_CONFIG,
			Type: &Configurator.Config_Server{
				Server: &Configurator.GrpcServerConfig{
					SvcName:  go_common.CONFIGURATOR_SVC,
					Port:     10010,
					LogLevel: 12,
					Updated:  time.Now().Unix(),
					SubConfs: []*Configurator.Config{
						{
							ConfType: Configurator.ConfigType_DB_CONFIG,
							Type: &Configurator.Config_Db{
								Db: &Configurator.DBConfig{
									Handler:  "redis",
									Hostname: "localhost",
									Port:     6379,
									Updated:  time.Now().Unix(),
								},
							},
						},
						{
							ConfType: Configurator.ConfigType_LOCKER_CONFIG,
							Type: &Configurator.Config_Locker{
								Locker: &Configurator.LockerConfig{
									Handler:  "zookeeper",
									Hostname: "localhost",
									Port:     2181,
									Updated:  time.Now().Unix(),
								},
							},
						},
					},
				},
			},
		},
	},
}

var (
	postgres_hostname = func(docker bool) string {
		if docker {
			return "pg"
		}
		if len(os.Getenv("POSTGRES")) > 0 {
			return os.Getenv("POSTGRES")
		}
		return "localhost"
	}

	redis_hostname = func(docker bool) string {
		if docker {
			return "redis"
		}
		if len(os.Getenv("REDIS")) > 0 {
			return os.Getenv("REDIS")
		}
		return "localhost"
	}

	zookeeper_hostname = func(docker bool) string {
		if docker {
			return "zookeeper"
		}
		if len(os.Getenv("ZOOKEEPER")) > 0 {
			return os.Getenv("ZOOKEEPER")
		}
		return "localhost"
	}
)

var (
	postgres_conf = func(docker bool) *Configurator.DBConfig {
		return &Configurator.DBConfig{
			Handler:  "postgres",
			Hostname: postgres_hostname(docker),
			Port:     5432,
			Username: "postgres",
			Password: "postgres",
			Updated:  time.Now().Unix(),
		}
	}

	redis_conf = func(docker bool) *Configurator.DBConfig {
		return &Configurator.DBConfig{
			Handler:  "redis",
			Hostname: redis_hostname(docker),
			Port:     6379,
			Updated:  time.Now().Unix(),
		}
	}

	default_databases = func(handler string, docker bool) *Configurator.DBConfig {
		switch handler {
		case "os_filestore":
			return &Configurator.DBConfig{
				Handler: "os_filestore",
				DbPath:  ".",
				Updated: time.Now().Unix(),
			}
		case "os_filestore_test":
			return &Configurator.DBConfig{
				Handler: "os_filestore",
				DbPath:  "resources/output",
				Updated: time.Now().Unix(),
			}
		case "redis":
			return redis_conf(docker)
		case "postgres":
			return postgres_conf(docker)
		}
		panic("Unrecognized handler " + handler)
		return nil
	}
)

var default_locker = func(handler string, docker bool) *Configurator.LockerConfig {
	switch handler {
	case "zookeeper":
		return &Configurator.LockerConfig{
			Handler:  handler,
			Hostname: zookeeper_hostname(docker),
			Port:     2181,
			Updated:  time.Now().Unix(),
		}
	case "redis":
		return &Configurator.LockerConfig{
			Handler:  handler,
			Hostname: redis_hostname(docker),
			Port:     6379,
			Updated:  time.Now().Unix(),
		}
	}
	// this will enable mem locker
	return &Configurator.LockerConfig{
		Handler: "none",
		Updated: time.Now().Unix(),
	}
}

var (
	default_client = func(svc, hostname string, port int32) *Configurator.GrpcClientConfig {
		return &Configurator.GrpcClientConfig{
			SvcName:    svc,
			ServerAddr: []string{hostname + ":" + strconv.Itoa(int(port))},
			Updated:    time.Now().Unix(),
		}
	}

	default_clients = func(svc string, docker bool) *Configurator.GrpcClientConfig {
		if docker {
			return default_client(svc, svc, 10000)
		}
		localHost := "localhost"
		switch svc {
		case go_common.AUTH_SVC:
			return default_client(svc, localHost, 10001)
		case go_common.CDN_SVC:
			return default_client(svc, localHost, 10002)
		case go_common.PAYMENTS_SVC:
			return default_client(svc, localHost, 10006)
		case go_common.WALLET_SVC:
			return default_client(svc, localHost, 10009)
		case go_common.NOTIFICATIONS_SVC:
			return default_client(svc, localHost, 10012)
		}
		panic("Unrecognized service " + svc)
		return nil
	}
)

var (
	default_server = func(svc string, port int32) *Configurator.GrpcServerConfig {
		return &Configurator.GrpcServerConfig{
			SvcName:  svc,
			Port:     port,
			LogLevel: 15,
			Updated:  time.Now().Unix(),
		}
	}

	default_servers = func(svc string, docker bool) *Configurator.GrpcServerConfig {
		if docker {
			switch svc {
			case go_common.AUTH_SVC:
				srv := default_server(svc, 10000)
				srv.PubKeyFile = "app.rsa.pub"
				srv.PrivKeyFile = "app.rsa"
				return srv
			}
			return default_server(svc, 10000)
		}
		switch svc {
		case go_common.AUTH_SVC:
			srv := default_server(svc, 10001)
			srv.PubKeyFile = "app.rsa.pub"
			srv.PrivKeyFile = "app.rsa"
			return srv
		case go_common.AUTH_TEST_SVC:
			srv := default_server(svc, 10001)
			srv.PubKeyFile = "../auth/app.rsa.pub"
			srv.PrivKeyFile = "../auth/app.rsa"
			return srv
		case go_common.CDN_SVC:
			return default_server(svc, 10002)
		case go_common.PAYMENTS_SVC:
			return default_server(svc, 10006)
		case go_common.WALLET_SVC:
			return default_server(svc, 10009)
		case go_common.NOTIFICATIONS_SVC:
			return default_server(svc, 10012)
		}
		panic("Unrecognized service " + svc)
		return nil
	}
)

var default_notif_provider = func(handler string, docker bool) *Configurator.NotifProviderConfig {
	switch Notifications.NotificationType(Notifications.NotificationType_value[handler]) {
	case Notifications.NotificationType_EMAIL:
		return &Configurator.NotifProviderConfig{
			Type: Notifications.NotificationType_EMAIL,
			Value: &Configurator.NotifProviderConfig_Email{
				Email: &Configurator.EmailProvider{
					Src: Configurator.EmailProvider_NATIVE,
					Config: &Configurator.EmailProvider_Emailer{
						Emailer: &Configurator.BaseEmailerConfig{
							SmtpAddr: "smtp.gmail.com",
							SmtpPort: "587",
							Username: "kuber.platform@gmail.com",
							Password: "KuberPlatform12345!@#$%",
						},
					},
				},
			},
		}
	}
	panic("Unrecognized notif provider " + handler)
	return nil
}

var default_conf = func(conf_type Configurator.ConfigType, svc string, docker bool) *Configurator.Config {

	switch conf_type {
	case Configurator.ConfigType_GRPC_SERVER_CONFIG:
		return &Configurator.Config{
			ConfType: Configurator.ConfigType_GRPC_SERVER_CONFIG,
			Type: &Configurator.Config_Server{
				Server: default_servers(svc, docker),
			},
		}
	case Configurator.ConfigType_PROXY_ENDPOINT_CONFIG:
		authHost := "localhost:10001"
		cdnHost := "localhost:10002"
		if docker {
			authHost = go_common.AUTH_SVC + ":10000"
			cdnHost = go_common.CDN_SVC + ":10000"
		}
		return &Configurator.Config{
			ConfType: Configurator.ConfigType_PROXY_ENDPOINT_CONFIG,
			Type: &Configurator.Config_Ep{
				Ep: &Configurator.ProxyEndpointConfig{
					SvcName: svc,
					Port:    80,
					Updated: time.Now().Unix(),
					Endpoints: []*Configurator.ProxyEndpointConfig_Endpoint{
						{
							Hostname: authHost,
							SvcName:  go_common.AUTH_SVC,
						},
						{
							Hostname: cdnHost,
							SvcName:  go_common.CDN_SVC,
						},
					},
				},
			},
		}
	case Configurator.ConfigType_DB_CONFIG:
		return &Configurator.Config{
			ConfType: Configurator.ConfigType_DB_CONFIG,
			Type: &Configurator.Config_Db{
				Db: default_databases(svc, docker),
			},
		}
	case Configurator.ConfigType_GRPC_CLIENT_CONFIG:
		return &Configurator.Config{
			ConfType: Configurator.ConfigType_GRPC_CLIENT_CONFIG,
			Type: &Configurator.Config_RpcClient{
				RpcClient: default_clients(svc, docker),
			},
		}
	case Configurator.ConfigType_LOCKER_CONFIG:
		return &Configurator.Config{
			ConfType: Configurator.ConfigType_LOCKER_CONFIG,
			Type: &Configurator.Config_Locker{
				Locker: default_locker(svc, docker),
			},
		}
	case Configurator.ConfigType_EMAILR_CLIENT_CONFIG:
		return &Configurator.Config{
			ConfType: Configurator.ConfigType_EMAILR_CLIENT_CONFIG,
			Type: &Configurator.Config_MailClient{
				MailClient: &Configurator.EmailrClientConfig{
					SmtpAddr: "smtp.gmail.com",
					SmtpPort: 587,
					Username: "kuber.platform@gmail.com",
					Password: "KuberPlatform12345!@#$%",
				},
			},
		}
	case Configurator.ConfigType_CDN_HOST_CONFIG:
		return &Configurator.Config{
			ConfType: Configurator.ConfigType_CDN_HOST_CONFIG,
			Type: &Configurator.Config_CdnHost{
				CdnHost: &Configurator.CDNHostConfig{
					Protocol: "http",
					Hostname: "api.pocketbarter.com",
					Port:     "80",
					BaseUrl:  "/v1/images/",
				},
			},
		}
	case Configurator.ConfigType_NOTIF_PROVIDER_CONFIG:
		return &Configurator.Config{
			ConfType: Configurator.ConfigType_NOTIF_PROVIDER_CONFIG,
			Type: &Configurator.Config_Notifications{
				Notifications: default_notif_provider(svc, docker),
			},
		}
	}
	return nil
}
