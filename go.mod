module gitlab.com/go-msuite

go 1.12

require (
	github.com/aloknerurkar/backend_utils v0.0.0-20180515124716-4158e05ae1c5
	github.com/aloknerurkar/log-post v0.0.0-20170917150139-1949622c67f4
	github.com/aloknerurkar/task-runner v0.0.0-20170722073601-3626fdad9f93 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/garyburd/redigo v1.6.0
	github.com/gogo/protobuf v1.2.1
	github.com/goinggo/tracelog v0.0.0-20180821171753-c71e233f94bd // indirect
	github.com/golang/protobuf v1.3.1
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.0
	github.com/grpc-ecosystem/grpc-gateway v1.8.5
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/pkg/errors v0.8.1
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.3.0
	github.com/stripe/stripe-go v30.8.0+incompatible
	github.com/yaronsumel/go-zookeeper v0.0.0-20170729191140-bb6d1f799eb4
	golang.org/x/crypto v0.0.0-20190426145343-a29dc8fdc734
	golang.org/x/net v0.0.0-20190503192946-f4e77d36d62c
	google.golang.org/genproto v0.0.0-20190502173448-54afdca5d873
	google.golang.org/grpc v1.20.1
)
