include Makefile.docker

BUILD_DIR  := $(PWD)
PROTO_DIRS := auth cdn common payments configurator notifications wallet
SVC_DIRS   := auth cdn payments configurator notifications wallet

PROTOC := $(shell which protoc)

GOOGLE_APIS := $(GOPATH)/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis

INCLUDES := -I$(GOOGLE_APIS)

GRPC_PLUGIN := --go_out=plugins,paths=source_relative:.
GATEWAY_PLUGIN := --grpc-gateway_out=logtostderr=true:.
SWAGGER_PLUGIN := --swagger_out=logtostderr=true:.

PROTO_PATH := --proto_path=$(BUILD_DIR):.
PROTO_SFX := .proto

proto:
	@echo ""; echo "";
	@echo "===== Make target proto =====";
	@for dir in $(PROTO_DIRS); do \
		cd $(BUILD_DIR)/$$dir/pb; \
		echo "Building gRPC service $$dir"; \
      $(PROTOC) $(INCLUDES) $(GRPC_PLUGIN) $(PROTO_PATH) $$dir$(PROTO_SFX); \
	done
	@echo "===== Done target proto =====";

gw:
	@echo ""; echo "";
	@echo "===== Make target gw =====";
	@for dir in $(PROTO_DIRS); do \
		cd $(BUILD_DIR)/$$dir/pb; \
		echo "Building gRPC gateway for service $$dir"; \
      $(PROTOC) $(INCLUDES) $(GATEWAY_PLUGIN) $(PROTO_PATH) $$dir$(PROTO_SFX); \
	done
	@echo "===== Done target gw =====";

swagger:
	@echo ""; echo "";
	@echo "===== Make target swagger =====";
	@for dir in $(PROTO_DIRS); do \
		cd $(BUILD_DIR)/$$dir/pb; \
		echo "Building swagger definitions for service $$dir"; \
		$(PROTOC) $(INCLUDES) $(SWAGGER_PLUGIN) $(PROTO_PATH) $$dir$(PROTO_SFX); \
	done
	@echo "===== Done target swagger =====";

services:
	@echo ""; echo "";
	@echo "===== Make target services =====";
	@for dir in $(SVC_DIRS); do \
    		cd $(BUILD_DIR)/$$dir; \
    		echo "Building service $$dir"; \
    		go build -i; \
    done
	@echo "===== Done target services =====";

clean:
	@echo ""; echo "";
	@echo "===== Make target clean =====";
	@for dir in $(SVC_DIRS); do \
      echo "Removing executable $$dir"; \
      rm -if $(BUILD_DIR)/$$dir/$$dir; \
    done
	@for dir in $(PROTO_DIRS); do \
	   echo "Removing generated proto files in $$dir"; \
    	rm -if $(BUILD_DIR)/$$dir/pb/*.pb.go; \
    	rm -if $(BUILD_DIR)/$$dir/pb/*.gw.go; \
    	rm -if $(BUILD_DIR)/$$dir/pb/*.swagger.json; \
    done
	@echo "===== Done target clean =====";

all: clean proto gw swagger services

tests:
	@cd $(BUILD_DIR)/_test; \
	(./setup.sh); \
	(go test -run /phase=1 -v); \

default: all
