package go_common

import (
	"context"
	msgs "gitlab.com/go-msuite/common/pb"
	locker "gitlab.com/go-msuite/locker"
	store "gitlab.com/go-msuite/store"
	storeItem "gitlab.com/go-msuite/store/item"
	"log"
	"os"
	"os/exec"
	"sync"
)

// Services
const (
	AUTH_SVC          = "auth"
	CDN_SVC           = "cdn"
	PAYMENTS_SVC      = "payments"
	WALLET_SVC        = "wallet"
	CONFIGURATOR_SVC  = "configurator"
	NOTIFICATIONS_SVC = "notifications"
)

// Test related configs
const (
	AUTH_TEST_SVC = "auth-test"
)

const WAIT_FOR_SCRIPT = "./wait-for-it.sh"

func WaitForService(svc string) error {
	if _, err := os.Stat(WAIT_FOR_SCRIPT); err != nil {
		log.Println("Did not find wait-for script. Returning.")
		return nil
	}
	log.Println("Waiting for " + svc)

	cmd := exec.Command(WAIT_FOR_SCRIPT, svc)
	err := cmd.Start()
	if err != nil {
		log.Printf("Hit error starting %v Err:%s", cmd, err.Error())
		return err
	}

	err = cmd.Wait()
	if err != nil {
		log.Printf("Hit error waiting for %v Err:%s", cmd, err.Error())
		return err
	}

	log.Printf("Done waiting for %s.", svc)
	return nil
}

func ParallelGetHelper(c context.Context, ids *msgs.UUIDs,
	getItem func(id string) storeItem.Item, retItems storeItem.Items,
	lckr locker.Locker, db store.Store) (retErr error) {

	wg := &sync.WaitGroup{}
	itemChan := make(chan storeItem.Item, len(ids.Vals))
	errChan := make(chan error, 1)
	ctx, cancel := context.WithCancel(c)

	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case it := <-itemChan:
				retItems = append(retItems, it)
				log.Printf("Added item to result %s_%s", it.GetNamespace(), it.GetId())
				if len(ids.Vals) == len(retItems) {
					log.Println("Read all the IDs. Returning")
					return
				}
			case retErr = <-errChan:
				log.Printf("Read returned error for one of the routines Err:%s", retErr.Error())
				cancel()
				return
			}
		}
	}()

	for i := range ids.Vals {
		wg.Add(1)
		go func(id string) {
			defer wg.Done()
			// If one of the previous routine returned error.
			select {
			case <-ctx.Done():
				return
			}
			item := getItem(id)

			// Get the distributed lock
			unlock, err := lckr.TryLock(item, locker.DefaultTimeout)
			if err != nil {
				errChan <- err
				return
			}
			defer unlock()

			err = db.Read(item)
			if err != nil {
				errChan <- err
				return
			}
			itemChan <- item
		}(ids.Vals[i])
	}
	wg.Wait()
	return
}
