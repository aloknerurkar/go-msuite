package heartbeat_service

import (
	Msgs "gitlab.com/go-msuite/common/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
)

type heartBeat struct {
	svc string
}

func Init(svc string, srv *grpc.Server) error {
	Msgs.RegisterHeartBeatServer(srv, &heartBeat{svc: svc})
	return nil
}

func (s *heartBeat) GetHeartBeat(c context.Context, msg *Msgs.EmptyMessage) (*Msgs.EmptyMessage, error) {
	log.Println("Received heartbeat for " + s.svc)
	return &Msgs.EmptyMessage{}, nil
}
