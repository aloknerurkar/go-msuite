package objects

import (
	"crypto/rsa"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"github.com/grpc-ecosystem/go-grpc-middleware/validator"
	"github.com/pkg/errors"
	"gitlab.com/go-msuite/app-errors"
	"gitlab.com/go-msuite/common/objects/utils"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
	"log"
)

type GrpcServerConf struct {
	*Configurator.GrpcServerConfig

	PubKey        *rsa.PublicKey
	PrivKey       *rsa.PrivateKey
	auth_func_set bool
	auth_func     grpc_auth.AuthFunc
	recv_func_set bool
	recv_func     grpc_recovery.RecoveryHandlerFunc
	docker_mode   bool
}

func (c *GrpcServerConf) GetServerOpts() ([]grpc.ServerOption, error) {

	var opts []grpc.ServerOption

	if c.UseTls {
		creds, err := credentials.NewServerTLSFromFile(c.CertFile, c.KeyFile)
		if err != nil {
			log.Printf("Failed creating TLS credentials.ERR:%s\n", err)
			return opts, err
		}

		opts = append(opts, grpc.Creds(creds))
	}

	var u_interceptors []grpc.UnaryServerInterceptor
	var s_interceptors []grpc.StreamServerInterceptor

	if c.UseJwt {
		if !c.auth_func_set {
			err := c.withDefaultAuthFunc()
			if err != nil {
				log.Printf("Failed creating default JWT auth. ERR:%s\n", err)
				return nil, err
			}
		}
		u_interceptors = append(u_interceptors, grpc_auth.UnaryServerInterceptor(c.auth_func))
		s_interceptors = append(s_interceptors, grpc_auth.StreamServerInterceptor(c.auth_func))

	}

	if c.UseValidator {
		u_interceptors = append(u_interceptors, grpc_validator.UnaryServerInterceptor())
		s_interceptors = append(s_interceptors, grpc_validator.StreamServerInterceptor())
	}

	if c.UseRecovery {
		if !c.recv_func_set {
			c.withDefaultRecvFunc()
		}
		u_interceptors = append(u_interceptors, grpc_recovery.UnaryServerInterceptor(
			grpc_recovery.WithRecoveryHandler(c.recv_func)))
		s_interceptors = append(s_interceptors, grpc_recovery.StreamServerInterceptor(
			grpc_recovery.WithRecoveryHandler(c.recv_func)))
	}

	opts = append(opts, grpc_middleware.WithUnaryServerChain(u_interceptors...))
	opts = append(opts, grpc_middleware.WithStreamServerChain(s_interceptors...))

	return opts, nil
}

func (c *GrpcServerConf) DefaultAuthFunction(ctx context.Context) (context.Context, error) {

	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, app_errors.ErrUnauthenticated("Metadata corrupted")
	}

	jwtToken, ok := md["authorization"]
	if !ok {
		return nil, app_errors.ErrUnauthenticated("Authorization header not present")
	}

	token, err := utils.ValidateToken(jwtToken[0], c.PubKey)
	if err != nil {
		return nil, app_errors.ErrUnauthenticated("Invalid token")
	}

	newCtx := context.WithValue(ctx, "jwt_token", token)
	claims, ok := token.Claims.(*utils.AuthClaims)
	if ok {
		newCtx = context.WithValue(newCtx, "user_id", claims.UserUuid)
	}
	return newCtx, nil
}

func (c *GrpcServerConf) WithAuthFunc(auth grpc_auth.AuthFunc) {

	if !c.UseJwt {
		log.Fatal("Public key file not specified in config.")
	}

	var err error
	c.PubKey, err = utils.ParseJWTpubKeyFile(c.PubKeyFile)
	if err != nil {
		log.Fatalf("Failed parsing public key.ERR:%s\n", err)
	}

	c.auth_func = auth
	c.auth_func_set = true
}

func (c *GrpcServerConf) withDefaultAuthFunc() error {

	var err error
	c.PubKey, err = utils.ParseJWTpubKeyFile(c.PubKeyFile)
	if err != nil {
		log.Printf("Failed parsing public key.ERR:%s\n", err)
		return err
	}

	c.auth_func = c.DefaultAuthFunction
	c.auth_func_set = true
	return nil
}

func (c *GrpcServerConf) WithRecvFunc(recv grpc_recovery.RecoveryHandlerFunc) {

	if !c.UseRecovery {
		log.Fatal("Use of recovery not specified in config.")
	}

	c.recv_func = recv
	c.recv_func_set = true
}

func (c *GrpcServerConf) withDefaultRecvFunc() {
	c.recv_func = utils.DefaultRecovery
	c.recv_func_set = true
}

func matchDB(hdlr1, hdlr2 string) bool {
	switch hdlr1 {
	case "os_filestore_test":
		return hdlr2 == "os_filestore"
	}
	return hdlr1 == hdlr2
}

func (c *GrpcServerConf) GetDBConfig(handler string) *Configurator.DBConfig {
	for i := range c.SubConfs {
		if c.SubConfs[i].ConfType == Configurator.ConfigType_DB_CONFIG {
			if matchDB(c.SubConfs[i].GetDb().Handler, handler) {
				return c.SubConfs[i].GetDb()
			}
		}
	}
	return nil
}

func (c *GrpcServerConf) GetSubServerConfig(svc string) *GrpcServerConf {
	for i := range c.SubConfs {
		if c.SubConfs[i].ConfType == Configurator.ConfigType_GRPC_SERVER_CONFIG {
			if c.SubConfs[i].GetServer().SvcName == svc {
				return &GrpcServerConf{GrpcServerConfig: c.SubConfs[i].GetServer()}
			}
		}
	}
	return nil
}

func (c *GrpcServerConf) GetClientConfig(svc string) *GrpcClientConf {
	for i := range c.SubConfs {
		if c.SubConfs[i].ConfType == Configurator.ConfigType_GRPC_CLIENT_CONFIG {
			if c.SubConfs[i].GetRpcClient().SvcName == svc {
				return &GrpcClientConf{GrpcClientConfig: c.SubConfs[i].GetRpcClient()}
			}
		}
	}
	return nil
}

func (c *GrpcServerConf) GetLockerConfig(handler string) *Configurator.LockerConfig {
	for i := range c.SubConfs {
		if c.SubConfs[i].ConfType == Configurator.ConfigType_LOCKER_CONFIG {
			if c.SubConfs[i].GetLocker().Handler == handler {
				return c.SubConfs[i].GetLocker()
			}
		}
	}
	return nil
}

func (c *GrpcServerConf) GetEmailrConfig() *Configurator.EmailrClientConfig {
	for i := range c.SubConfs {
		if c.SubConfs[i].ConfType == Configurator.ConfigType_EMAILR_CLIENT_CONFIG {
			return c.SubConfs[i].GetMailClient()
		}
	}
	return nil
}

func (c *GrpcServerConf) GetPaymentProviders() []*Configurator.PaymentProviderConfig {
	providers := make([]*Configurator.PaymentProviderConfig, 0)
	for i := range c.SubConfs {
		if c.SubConfs[i].ConfType == Configurator.ConfigType_PAYMENT_PROVIDER_CONFIG {
			p := c.SubConfs[i].GetPaymentClient()
			providers = append(providers, p)
		}
	}
	return providers
}

func (c *GrpcServerConf) GetCDNConfig() *Configurator.CDNHostConfig {
	for i := range c.SubConfs {
		if c.SubConfs[i].ConfType == Configurator.ConfigType_CDN_HOST_CONFIG {
			return c.SubConfs[i].GetCdnHost()
		}
	}
	return nil
}

func (c *GrpcServerConf) ParseJWTKeys() (err error) {

	if len(c.GetPrivKeyFile()) == 0 {
		err = errors.New("Priv key missing")
		return
	}
	if len(c.GetPrivKeyFile()) == 0 {
		err = errors.New("Priv key missing")
		return
	}
	c.PrivKey, err = utils.ParseJWTprivKeyFile(c.GetPrivKeyFile())
	if err != nil {
		return
	}

	c.PubKey, err = utils.ParseJWTpubKeyFile(c.GetPubKeyFile())
	if err != nil {
		return
	}
	return
}

func (c *GrpcServerConf) GetDockerMode() bool {
	if c.SvcName == "kuber_configurator" {
		return c.docker_mode
	}
	return false
}

func (c *GrpcServerConf) SetDockerMode(dkr_md bool) {
	if c.SvcName == "kuber_configurator" {
		c.docker_mode = dkr_md
	}
	return
}

func (c *GrpcServerConf) GetNotifProviders() []*Configurator.NotifProviderConfig {
	providers := make([]*Configurator.NotifProviderConfig, 0)
	for i := range c.SubConfs {
		if c.SubConfs[i].ConfType == Configurator.ConfigType_NOTIF_PROVIDER_CONFIG {
			providers = append(providers, c.SubConfs[i].GetNotifications())
		}
	}
	return providers
}
