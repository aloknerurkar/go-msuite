package objects

import (
	"database/sql"
	"fmt"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	"log"
)

type PostgresConf struct {
	*Configurator.DBConfig
}

func (dbConf *PostgresConf) OpenDB() (*sql.DB, error) {

	open_str := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		dbConf.Hostname, dbConf.Port, dbConf.Username, dbConf.Password, dbConf.DbName)

	dbP, err := sql.Open("postgres", open_str)
	if err == nil {
		// Open doesn't really do anything. Ping is where we will know.
		err = dbP.Ping()
	}

	if err != nil {
		log.Printf("Failed opening DB Err:%s", err.Error())
		return nil, err
	}

	log.Printf("Successfully connected to DB %s", dbConf.DbName)

	return dbP, nil
}

func (dbConf *PostgresConf) CreatePQDB() (*sql.DB, error) {

	// If DB is already created, Use the same.
	dbP, err := dbConf.OpenDB()
	if err == nil {
		log.Println("DB has already been created.")
		return dbP, nil
	}

	// Connect to pq and create database.
	open_str := fmt.Sprintf("host=%s port=%d user=%s password=%s sslmode=disable",
		dbConf.Hostname, dbConf.Port, dbConf.Username, dbConf.Password)

	dbP, err = sql.Open("postgres", open_str)
	if err != nil {
		log.Printf("Failed to open postgres. Open String:%s", open_str)
		return nil, err
	}

	err = dbP.Ping()
	if err != nil {
		log.Printf("Failed to ping postgres. Open String:%s", open_str)
		return nil, err
	}

	_, err = dbP.Exec("CREATE DATABASE " + dbConf.DbName)
	if err != nil {
		log.Printf("Failed to create postgres database %s", dbConf.DbName)
		return nil, err
	}

	// We need to make a new connection using the newly created database name.
	err = dbP.Close()
	if err != nil {
		log.Println("Failed to close postgres db after creation.")
		return nil, err
	}

	return dbConf.OpenDB()
}

func (dbConf *PostgresConf) RemovePQDB() error {
	open_str := fmt.Sprintf("host=%s port=%d user=%s password=%s sslmode=disable",
		dbConf.Hostname, dbConf.Port, dbConf.Username, dbConf.Password)

	dbP, err := sql.Open("postgres", open_str)
	if err != nil {
		log.Printf("Failed to open postgres. Open String:%s", open_str)
		return err
	}

	err = dbP.Ping()
	if err != nil {
		log.Printf("Failed to ping postgres. Open String:%s", open_str)
		return err
	}

	_, err = dbP.Exec("REVOKE CONNECT ON DATABASE " + dbConf.DbName + " FROM public")
	if err != nil {
		log.Printf("Failed to revoke database connections %s", dbConf.DbName)
		return err
	}

	_, err = dbP.Exec("SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity " +
		"WHERE pg_stat_activity.datname = '" + dbConf.DbName + "'")
	if err != nil {
		log.Printf("Failed to terminate database connections %s", dbConf.DbName)
		return err
	}

	_, err = dbP.Exec("DROP DATABASE " + dbConf.DbName)
	if err != nil {
		log.Printf("Failed to drop postgres database %s", dbConf.DbName)
		return err
	}

	err = dbP.Close()
	if err != nil {
		log.Println("Failed to close postgres db after creation.")
		return err
	}
	return nil
}
