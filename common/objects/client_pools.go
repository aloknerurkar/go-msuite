package objects

import (
	"errors"
	Auth "gitlab.com/go-msuite/auth/pb"
	CDN "gitlab.com/go-msuite/cdn/pb"
	"gitlab.com/go-msuite/common/go-common"
	"gitlab.com/go-msuite/common/objects/utils"
	Messages "gitlab.com/go-msuite/common/pb"
	configurator "gitlab.com/go-msuite/configurator/pb"
	Notifications "gitlab.com/go-msuite/notifications/pb"
	Payments "gitlab.com/go-msuite/payments/pb"
	Wallet "gitlab.com/go-msuite/wallet/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type ClientPools struct {
	cpools map[string]*utils.Pool
}

func NewClientPools(srv *GrpcServerConf) *ClientPools {
	c_pools := new(ClientPools)
	c_pools.cpools = make(map[string]*utils.Pool, len(srv.SubConfs))
	for i := range srv.SubConfs {
		if srv.SubConfs[i].ConfType == configurator.ConfigType_GRPC_CLIENT_CONFIG {
			cliConf := &GrpcClientConf{GrpcClientConfig: srv.SubConfs[i].GetRpcClient()}
			c_pools.cpools[cliConf.SvcName] = &utils.Pool{
				ServerAddr:      cliConf.ServerAddr[0],
				NoOfConnections: 5,
				DialFn: func(addr string) (*grpc.ClientConn, error) {
					return cliConf.NewRPCConn(addr)
				},
				HeartBeatFn: func(c *grpc.ClientConn) error {
					cli := Messages.NewHeartBeatClient(c)
					_, err := cli.GetHeartBeat(context.Background(), &Messages.EmptyMessage{})
					return err
				},
			}
		}
	}
	return c_pools
}

func (c *ClientPools) Notifications() (Notifications.NotificationsClient, utils.ConnDone, error) {
	conn, done, err := c.cpools[go_common.NOTIFICATIONS_SVC].GetConn()
	return Notifications.NewNotificationsClient(conn), done, err
}

func (c *ClientPools) CDN() (CDN.CDNClient, utils.ConnDone, error) {
	conn, done, err := c.cpools[go_common.CDN_SVC].GetConn()
	return CDN.NewCDNClient(conn), done, err
}

func (c *ClientPools) Auth() (Auth.AuthClient, utils.ConnDone, error) {
	conn, done, err := c.cpools[go_common.AUTH_SVC].GetConn()
	return Auth.NewAuthClient(conn), done, err
}

func (c *ClientPools) Payments() (Payments.PaymentsClient, utils.ConnDone, error) {
	conn, done, err := c.cpools[go_common.PAYMENTS_SVC].GetConn()
	return Payments.NewPaymentsClient(conn), done, err
}

func (c *ClientPools) Wallet() (Wallet.WalletClient, utils.ConnDone, error) {
	conn, done, err := c.cpools[go_common.WALLET_SVC].GetConn()
	return Wallet.NewWalletClient(conn), done, err
}

func (c *ClientPools) Service(name string) (*grpc.ClientConn, utils.ConnDone, error) {
	if pool, ok := c.cpools[name]; ok {
		return pool.GetConn()
	}
	return nil, func() {}, errors.New("Pool to service doesnt exist")
}
