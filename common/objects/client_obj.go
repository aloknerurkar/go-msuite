package objects

import (
	"errors"
	"gitlab.com/go-msuite/common/objects/utils"
	configurator "gitlab.com/go-msuite/configurator/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"log"
	"sync"
)

type RoundRobin struct {
	MaxIdx   int
	curr_idx int
	mtx      sync.Mutex
}

func (r *RoundRobin) GetNextIdx() int {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	r.curr_idx = (r.curr_idx + 1) % r.MaxIdx
	return r.curr_idx
}

type GrpcClientConf struct {
	*configurator.GrpcClientConfig
	credentials.PerRPCCredentials
	JwtToken    string
	next_idx    int
	rr_selector *RoundRobin
	pools       []*utils.Pool
}

// GetRequestMetadata gets the current request metadata
func (j *GrpcClientConf) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {

	return map[string]string{
		"authorization": j.JwtToken,
	}, nil
}

// Jwt does not RequireTransportSecurity
func (j *GrpcClientConf) RequireTransportSecurity() bool { return false }

func (c *GrpcClientConf) WithJWTToken(token string) *GrpcClientConf {
	c.JwtToken = token
	return c
}

func (c *GrpcClientConf) NewRPCConn(addr string) (*grpc.ClientConn, error) {

	opts, err := c.GetClientOpts()
	if err != nil {
		log.Printf("Failed to get client options. ERR:%s\n", err.Error())
		return nil, err
	}

	conn, err := grpc.Dial(addr, opts...)
	if err != nil {
		log.Printf("Failed to dial. ERR:%s\n", err.Error())
		return nil, err
	}

	return conn, nil
}

func (c *GrpcClientConf) GetClientOpts() ([]grpc.DialOption, error) {

	var opts []grpc.DialOption
	if c.UseTls {
		var sn string
		if c.ServerHostOverride != "" {
			sn = c.ServerHostOverride
		}

		var creds credentials.TransportCredentials
		if c.CertFile != "" {
			var err error
			creds, err = credentials.NewClientTLSFromFile(c.CertFile, sn)
			if err != nil {
				log.Printf("Failed to create TLS credentials. ERR:%s\n", err.Error())
				return nil, err
			}
		} else {
			creds = credentials.NewClientTLSFromCert(nil, sn)
		}
		opts = append(opts, grpc.WithTransportCredentials(creds))
	} else {
		opts = append(opts, grpc.WithInsecure())
	}

	if c.UseJwt {
		if len(c.JwtToken) == 0 {
			log.Println("Token not specified for JWT.")
			return nil, errors.New("Token not specified for use of JWT.")
		}
		opts = append(opts, grpc.WithPerRPCCredentials(c))
	}

	return opts, nil
}

// Single client conn pool needs to be synchronized externally.
func (c *GrpcClientConf) CreatePool(no_of_conn int, do_heartbeat func(*grpc.ClientConn) error) error {

	c.pools = make([]*utils.Pool, len(c.ServerAddr))
	for i, v := range c.ServerAddr {
		c.pools[i] = &utils.Pool{
			DialFn:          c.NewRPCConn,
			ServerAddr:      v,
			HeartBeatFn:     do_heartbeat,
			NoOfConnections: no_of_conn,
		}
	}
	c.rr_selector = &RoundRobin{
		MaxIdx: len(c.ServerAddr),
	}
	return nil
}

func (c *GrpcClientConf) GetPooledConn() (*grpc.ClientConn, utils.ConnDone, error) {
	idx := c.rr_selector.GetNextIdx()
	return c.pools[idx].GetConn()
}
