package objects

type UserRecord struct {
	UserId string
	Email  string
	Mobile string
}
