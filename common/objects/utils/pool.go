package utils

import (
	"time"
	"sync"
	"log"
	"google.golang.org/grpc"
	"errors"
)

const HeartBeatDuration time.Duration = time.Minute * 5
const GetConnTimeout time.Duration = time.Second * 15

type Pool struct {
	DialFn 		func(addr string) (*grpc.ClientConn, error)
	HeartBeatFn 	func(*grpc.ClientConn) error
	NoOfConnections int
	ServerAddr	string
	init_done	bool
	mtx		sync.Mutex
	heartbeat_map	map[*grpc.ClientConn] time.Time
	conn_chan	chan *grpc.ClientConn
}

type ConnDone func()

func (p *Pool) GetConn() (*grpc.ClientConn, ConnDone, error) {

	/* Critical section. Only one routine should initialize at a time.
	 * Getting connection from the channel is a blocking call. So we should
	 * not hold the lock before doing that.
	 */
	p.mtx.Lock()
	if !p.init_done {
		p.conn_chan = make(chan *grpc.ClientConn, p.NoOfConnections)
		p.heartbeat_map = make(map[*grpc.ClientConn] time.Time, p.NoOfConnections)
		for i := 0; i < p.NoOfConnections; i++ {
			conn, err := p.DialFn(p.ServerAddr)
			if err != nil {
				p.mtx.Unlock()
				return nil, nil, err
			}
			p.conn_chan <- conn
			p.heartbeat_map[conn] = time.Now()
		}
		p.init_done = true
	}
	p.mtx.Unlock()

	// Blocking call. Timeout if takes too long. This should indicate we need more connections
	// in the config.
	var new_conn *grpc.ClientConn
	select {
	case new_conn = <-p.conn_chan:
		break
	case <-time.After(GetConnTimeout):
		log.Println("Timed out getting new conn.")
		return nil, nil, errors.New("Timeout while getting new connection.")
	}

	if time.Since(p.heartbeat_map[new_conn]) > HeartBeatDuration {
		err := p.HeartBeatFn(new_conn)
		if err != nil {
			// Get the lock before updating the map.
			p.mtx.Lock()
			delete(p.heartbeat_map, new_conn)
			p.mtx.Unlock()
			log.Printf("Heartbeat failed Err:%s", err.Error())
			new_conn, err = p.DialFn(p.ServerAddr)
			if err != nil {
				log.Printf("Failed to create new conn Err:%s", err.Error())
				return nil, nil, err
			}
		}
		// Get the lock before updating the map.
		p.mtx.Lock()
		p.heartbeat_map[new_conn] = time.Now()
		p.mtx.Unlock()
	}
	return new_conn, func() {p.connDone(new_conn)}, nil
}

func (p *Pool) connDone(conn *grpc.ClientConn) {
	p.conn_chan <- conn
}
