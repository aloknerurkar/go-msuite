package utils

import (
	"bytes"
	"crypto/rsa"
	"fmt"
	"github.com/aloknerurkar/backend_utils"
	"github.com/dgrijalva/jwt-go"
	"github.com/gogo/protobuf/jsonpb"
	Configurator "gitlab.com/go-msuite/configurator/pb"
	"io/ioutil"
	"log"
	r "math/rand"
	"os"
	"runtime/debug"
	"time"
)

type AuthClaims struct {
	*jwt.StandardClaims
	TokenType string
	UserUuid  string
	Username  string
	LoginType string
}

func ParseJWTpubKeyFile(file_path string) (*rsa.PublicKey, error) {
	key, err := ioutil.ReadFile(file_path)
	if err != nil {
		log.Printf("Failed reading JWT public key file.ERR:%s\n", err)
		return nil, err
	}
	pub_key, err := jwt.ParseRSAPublicKeyFromPEM(key)
	if err != nil {
		log.Printf("Failed parsing public key.ERR:%s\n", err)
		return nil, err
	}
	return pub_key, nil
}

func ParseJWTprivKeyFile(file_path string) (*rsa.PrivateKey, error) {
	key, err := ioutil.ReadFile(file_path)
	if err != nil {
		log.Printf("Failed reading JWT public key file.ERR:%s\n", err)
		return nil, err
	}
	priv_key, err := jwt.ParseRSAPrivateKeyFromPEM(key)
	if err != nil {
		log.Printf("Failed parsing public key.ERR:%s\n", err)
		return nil, err
	}
	return priv_key, nil
}

func ValidateToken(token string, publicKey *rsa.PublicKey) (*jwt.Token, error) {
	jwtToken, err := jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodRSA); !ok {
			log.Printf("Unexpected signing method: %v", t.Header["alg"])
			return nil, fmt.Errorf("Invalid token %s", token)
		}
		return publicKey, nil
	})
	if err == nil && jwtToken.Valid {
		return jwtToken, nil
	}
	return nil, err
}

func DefaultRecovery(arg interface{}) (ret_err error) {
	debug.PrintStack()

	switch arg.(type) {
	case string:
		ret_err = backend_utils.ErrInternal(arg.(string))
	default:
		ret_err = backend_utils.ErrUnknown("Server encountered unknown error.")
	}
	log.Println("Service Recovery handler. Returning error:" + ret_err.Error())
	return
}

func ReadConfFile(file_path string) (*Configurator.Configurations, error) {

	file, err := os.Open(file_path)
	if err != nil {
		return nil, err
	}

	conf := new(Configurator.Configurations)

	//decoder := json.NewDecoder(file)
	err = jsonpb.Unmarshal(file, conf)
	if err != nil {
		return nil, err
	}

	log.Printf("Read Configurations:%v\n", conf)

	return conf, nil
}

func WithRetry(fn func() error, no_of_tries int) error {
	var err error
	for i := 0; i < no_of_tries; i++ {
		err = fn()
		if err == nil {
			break
		}
		log.Printf("Failed on attempt no:%d Err:%s Tries Remaining:%d", i+1, err.Error(), (no_of_tries - i))
	}
	return err
}

// For generating random strings
const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandStringBytes(n int) string {
	r.Seed(time.Now().Unix() + time.Now().UnixNano())
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[r.Int63()%int64(len(letterBytes))]
	}
	return string(b)
}

func BufferStrings(args ...string) string {
	var buffer bytes.Buffer
	for _, str := range args {
		buffer.WriteString(str)
	}
	return buffer.String()
}
