package rpc_service

import (
	lg "github.com/aloknerurkar/log-post/client"
	"github.com/pkg/errors"
	"gitlab.com/go-msuite/app-errors"
	"gitlab.com/go-msuite/common/go-common"
	"gitlab.com/go-msuite/common/objects"
	"gitlab.com/go-msuite/common/objects/utils"
	msgs "gitlab.com/go-msuite/common/pb"
	"gitlab.com/go-msuite/configurator/config_service/grpc_server"
	"gitlab.com/go-msuite/locker"
	Payments "gitlab.com/go-msuite/payments/pb"
	"gitlab.com/go-msuite/store"
	Wallet "gitlab.com/go-msuite/wallet/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"sync/atomic"
	"time"
)

const (
	PKG_NAME = "kuber-wallet"
	VERSION  = "v1.0.0"
)

var (
	req_id int64 = 1
)

type balanceObj struct {
	*Wallet.Balance
}

func (l *balanceObj) GetNamespace() string { return go_common.WALLET_SVC + "_" + "balance" }

func (l *balanceObj) GetId() string { return l.UserId }

func (l *balanceObj) SetCreated(i int64) { l.Created = i }

func (l *balanceObj) SetUpdated(i int64) { l.Updated = i }

type chargeObj struct {
	*Wallet.WalletCharge
}

func (l *chargeObj) GetNamespace() string { return go_common.WALLET_SVC + "_" + "wcharge" }

func (l *chargeObj) GetId() string { return l.RefId }

func (l *chargeObj) SetId(id string) { l.RefId = id }

func (l *chargeObj) SetCreated(i int64) { l.Created = i }

func (l *chargeObj) SetUpdated(i int64) { l.Updated = i }

type chargesObj struct {
	*Wallet.WalletCharges
	UserId string
}

func (l *chargesObj) GetNamespace() string {
	return go_common.WALLET_SVC + "_" + "wcharges"
}

func (l *chargesObj) GetId() string {
	return l.UserId
}

type wallet struct {
	conf *objects.GrpcServerConf
	logr *lg.LogPostClient
	dbP  store.Store
	lckr locker.Locker
}

var InitFn grpc_server.RPCInitFn = Init

func Init(srv_conf *objects.GrpcServerConf, srv *grpc.Server) error {

	lgr := lg.InitLogger(srv_conf.SvcName, "", srv_conf.LogLevel, false)

	dbConf := srv_conf.GetDBConfig("redis")
	if dbConf == nil {
		return lgr.Error(errors.New("DB Config absent"), "Database config not provided")
	}

	dbp, err := store.NewStore(dbConf)
	if err != nil {
		return lgr.Error(err, "Failed initializing wallet db")
	}

	lockerConf := srv_conf.GetLockerConfig("zookeeper")
	if lockerConf == nil {
		return lgr.Error(errors.New("Locker Config absent"), "Locker config not provided")
	}

	lockr, err := locker.New(lockerConf)
	if err != nil {
		return lgr.Error(err, "Failed initializing wallet locker")
	}

	Wallet.RegisterWalletServer(srv, &wallet{
		dbP:  dbp,
		lckr: lockr,
		logr: lgr,
		conf: srv_conf,
	})
	return nil
}

func (r *wallet) getPaymentsClient() (Payments.PaymentsClient, utils.ConnDone, error) {
	c_conf := r.conf.GetClientConfig(go_common.PAYMENTS_SVC)
	conn, err := c_conf.NewRPCConn(c_conf.ServerAddr[0])
	return Payments.NewPaymentsClient(conn), func() {
		if conn != nil {
			conn.Close()
		}
	}, err
}

func (r *wallet) Charge(c context.Context, req *Wallet.WChargeReq) (ret_charge *Wallet.WalletCharge, ret_err error) {

	_req_id := atomic.AddInt64(&req_id, 1)
	r.logr.TraceStart("ReqID: %d", _req_id)
	defer r.logr.TraceEnd("ReqID: %d", _req_id)

	bal := &balanceObj{
		Balance: &Wallet.Balance{
			UserId: req.UserId,
		},
	}

	unlock, err := r.lckr.TryLock(bal, locker.DefaultTimeout)
	if err != nil {
		ret_err = r.logr.Error(app_errors.ErrInternal("Failed to get lock on user."),
			"ReqID:%d Arg:%v SecErr:%s", _req_id, req.UserId, err.Error())
		return
	}
	defer unlock()

	new_user := false
	err = r.dbP.Read(bal)
	if err != nil {
		// Check for ENOENT
		r.logr.Info("Failed to get balance for user %s. Creating new entry.", req.UserId)
		bal.Currency = req.Req.Currency
		bal.BalanceAmount = 1000
		bal.TotalCredit = 1000
		bal.TotalDebit = 0
		bal.Created = time.Now().Unix()
		new_user = true
	}

	switch req.Type {
	case Wallet.WalletCharge_CREDIT:
		cli, closeFn, err := r.getPaymentsClient()
		if err != nil {
			ret_err = r.logr.Error(app_errors.ErrInternal("Failed to connect to Payments service."),
				"ReqID:%d SecErr:%s", _req_id, err.Error())
			return
		}
		defer closeFn()

		// VoucherID is currently a charge ID for which refund is being issued.
		// This can be extended to redeem points/rewards etc.
		if charge_id := req.Req.GetVoucherId(); len(charge_id) == 0 {
			ret_err = r.logr.Error(app_errors.ErrInvalidArg("Voucher ID not specified."),
				"ReqID:%d Arg:%v", _req_id, req)
			return

		} else {
			charge, err := cli.GetCharge(context.Background(), &msgs.ReqUUID{Uuid: charge_id})
			if err != nil {
				ret_err = r.logr.Error(app_errors.ErrInternal("Failed to get charge information."),
					"ReqID:%d SecErr:%s", _req_id, err.Error())
				return
			}
			if charge.ChargeAmount != req.Req.Amount || charge.Currency != req.Req.Currency ||
				req.Req.Currency != bal.Currency {
				ret_err = r.logr.Error(app_errors.ErrPermissionDenied("Credit not allowed for this charge."),
					"ReqID:%d Charge:%v Req %v", _req_id, charge, req)
				return
			}
			bal.BalanceAmount += req.Req.Amount
			bal.TotalCredit += req.Req.Amount
			err = r.dbP.Update(bal)
			if err != nil {
				ret_err = r.logr.Error(app_errors.ErrInternal("Failed to update user balance."),
					"ReqID:%d Req %v", _req_id, req)
				return
			}
			ch := &chargeObj{
				WalletCharge: &Wallet.WalletCharge{
					UserId:      bal.UserId,
					Amount:      req.Req.Amount,
					Currency:    req.Req.Currency,
					Description: req.Req.Statement,
					Type:        Wallet.WalletCharge_CREDIT,
					VoucherId:   charge_id,
				},
			}
			err = r.dbP.Create(ch)
			if err != nil {
				ret_err = r.logr.Error(app_errors.ErrInternal("Failed to store charge."),
					"ReqID:%d Balance:%v Req %v", _req_id, bal, req)
				return
			}
			ret_charge = ch.WalletCharge
		}
	case Wallet.WalletCharge_DEBIT:
		if req.Req.Provider != Payments.ProviderId_WALLET || req.Req.Currency != bal.Currency ||
			req.Req.Amount > bal.BalanceAmount {
			ret_err = r.logr.Error(app_errors.ErrPermissionDenied("Debit not allowed for this charge."),
				"ReqID:%d Balance:%v Req %v", _req_id, bal, req)
			return
		}
		bal.BalanceAmount -= req.Req.Amount
		bal.TotalDebit += req.Req.Amount
		err = r.dbP.Update(bal)
		if err != nil {
			ret_err = r.logr.Error(app_errors.ErrInternal("Failed to update user balance."),
				"ReqID:%d Req %v", _req_id, req)
			return
		}
		ch := &chargeObj{
			WalletCharge: &Wallet.WalletCharge{
				UserId:      bal.UserId,
				Amount:      req.Req.Amount,
				Currency:    req.Req.Currency,
				Description: req.Req.Statement,
				Type:        Wallet.WalletCharge_DEBIT,
			},
		}
		err = r.dbP.Create(ch)
		if err != nil {
			ret_err = r.logr.Error(app_errors.ErrInternal("Failed to store charge."),
				"ReqID:%d Balance:%v Req %v", _req_id, bal, req)
			return
		}
		ret_charge = ch.WalletCharge
	}

	charges := &chargesObj{
		UserId: bal.UserId,
	}

	if new_user {
		charges.WalletCharges = &Wallet.WalletCharges{
			Charges: []*Wallet.WalletCharge{
				ret_charge,
			},
		}
		err = r.dbP.Create(charges)
		if err != nil {
			ret_err = r.logr.Error(app_errors.ErrInternal("Failed to create charges."),
				"ReqID:%d Charges:%v Req %v", _req_id, charges, req)
			return
		}
	} else {
		charges.WalletCharges = new(Wallet.WalletCharges)
		err = r.dbP.Read(charges)
		if err != nil {
			ret_err = r.logr.Error(app_errors.ErrInternal("Failed to read charges."),
				"ReqID:%d Charges:%v Req %v", _req_id, charges, req)
			return
		}
		charges.Charges = append(charges.Charges, ret_charge)
		err = r.dbP.Update(charges)
		if err != nil {
			ret_err = r.logr.Error(app_errors.ErrInternal("Failed to update charges."),
				"ReqID:%d Charges:%v Req %v", _req_id, charges, req)
			return
		}
	}

	return
}

func (r *wallet) GetCharge(c context.Context, charge_id *msgs.ReqUUID) (ret_charge *Wallet.WalletCharge, ret_err error) {

	_req_id := atomic.AddInt64(&req_id, 1)
	r.logr.TraceStart("ReqID: %d", _req_id)
	defer r.logr.TraceEnd("ReqID: %d", _req_id)

	ch := &chargeObj{
		WalletCharge: &Wallet.WalletCharge{
			RefId: charge_id.Uuid,
		},
	}

	unlock, err := r.lckr.TryLock(ch, locker.DefaultTimeout)
	if err != nil {
		ret_err = r.logr.Error(app_errors.ErrInternal("Failed to get lock on user."),
			"ReqID:%d Arg:%v SecErr:%s", _req_id, charge_id, err.Error())
		return
	}
	defer unlock()

	ret_err = r.dbP.Read(ch)
	if ret_err != nil {
		ret_err = r.logr.Error(app_errors.ErrInvalidArg("Charge ID invalid."),
			"ReqID:%d ID:%s SecErr:%s", _req_id, charge_id.Uuid, ret_err)
		return
	}

	ret_charge = ch.WalletCharge

	return
}

func (r *wallet) GetBalance(c context.Context, user_id *msgs.ReqUUID) (ret_balance *Wallet.Balance, ret_err error) {

	_req_id := atomic.AddInt64(&req_id, 1)
	r.logr.TraceStart("ReqID: %d", _req_id)
	defer r.logr.TraceEnd("ReqID: %d", _req_id)

	bal := &balanceObj{
		Balance: &Wallet.Balance{
			UserId: user_id.Uuid,
		},
	}

	unlock, err := r.lckr.TryLock(bal, locker.DefaultTimeout)
	if err != nil {
		ret_err = r.logr.Error(app_errors.ErrInternal("Failed to get lock on user."),
			"ReqID:%d Arg:%v SecErr:%s", _req_id, user_id, err.Error())
		return
	}
	defer unlock()

	ret_err = r.dbP.Read(bal)
	if ret_err != nil {
		ret_err = r.logr.Error(app_errors.ErrInvalidArg("User ID invalid."),
			"ReqID:%d ID:%s SecErr:%s", _req_id, user_id.Uuid, ret_err)
		return
	}

	ret_balance = bal.Balance

	return
}

func (r *wallet) GetStatement(c context.Context, user_id *msgs.ReqUUID) (ret_charges *Wallet.WalletCharges, ret_err error) {

	_req_id := atomic.AddInt64(&req_id, 1)
	r.logr.TraceStart("ReqID: %d", _req_id)
	defer r.logr.TraceEnd("ReqID: %d", _req_id)

	charges := &chargesObj{
		UserId: user_id.Uuid,
	}

	unlock, err := r.lckr.TryLock(charges, locker.DefaultTimeout)
	if err != nil {
		ret_err = r.logr.Error(app_errors.ErrInternal("Failed to get lock on user."),
			"ReqID:%d Arg:%v SecErr:%s", _req_id, user_id, err.Error())
		return
	}
	defer unlock()

	ret_err = r.dbP.Read(charges)
	if ret_err != nil {
		ret_err = r.logr.Error(app_errors.ErrInvalidArg("User ID invalid."),
			"ReqID:%d ID:%s SecErr:%s", _req_id, user_id.Uuid, ret_err)
		return
	}

	return
}
